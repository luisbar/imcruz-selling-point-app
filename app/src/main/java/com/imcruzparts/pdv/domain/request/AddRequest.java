package com.imcruzparts.pdv.domain.request;


import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.request.RequestDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

import java.util.List;

public class AddRequest extends UseCase<AddRequest.Params, Object, Object> {

    private DatabaseRepository mRequestDatabaseRepository;

    public AddRequest() {
        this.mRequestDatabaseRepository = new RequestDatabaseRepository();
    }

    @Override
    public void execute(Params param1) {
        mRequestDatabaseRepository.add(RequestDatabaseRepository.Params.paramsForAdd(
                param1.idSellingPoint,
                param1.idActivity,
                param1.getSubTotal(),
                param1.discount,
                param1.totalAmount,
                param1.productsId,
                param1.quantityByProducts
        ));
    }

    public static class Params {

        private int idSellingPoint;
        private int idActivity;
        private double subTotal;
        private double discount;
        private double totalAmount;
        private List<Integer> productsId;
        private List<Integer> quantityByProducts;

        public Params(int idSellingPoint, int idActivity, double subTotal, double discount, double totalAmount, List<Integer> productsId, List<Integer> quantityByProducts) {
            this.idSellingPoint = idSellingPoint;
            this.idActivity = idActivity;
            this.subTotal = subTotal;
            this.discount = discount;
            this.totalAmount = totalAmount;
            this.productsId = productsId;
            this.quantityByProducts = quantityByProducts;
        }

        public int getIdSellingPoint() {
            return idSellingPoint;
        }

        public void setIdSellingPoint(int idSellingPoint) {
            this.idSellingPoint = idSellingPoint;
        }

        public int getIdActivity() {
            return idActivity;
        }

        public void setIdActivity(int idActivity) {
            this.idActivity = idActivity;
        }

        public double getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(double subTotal) {
            this.subTotal = subTotal;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public List<Integer> getProductsId() {
            return productsId;
        }

        public void setProductsId(List<Integer> productsId) {
            this.productsId = productsId;
        }

        public static Params paramsForAddRequest(int idSellingPoint, int idActivity, double subTotal, double discount, double totalAmount, List<Integer> productsId, List<Integer> quantityByProducts) {
            return new Params(idSellingPoint, idActivity, subTotal, discount, totalAmount,
                    productsId, quantityByProducts);
        }
    }
}
