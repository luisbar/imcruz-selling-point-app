package com.imcruzparts.pdv.domain.route;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.route.RouteApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class AddUnassignedRoute extends UseCase {

    private ApiRepository mRouteApiRepository;

    public AddUnassignedRoute() {
        mRouteApiRepository = new RouteApiRepository();
    }

    @Override
    public void execute(Object param1) {
        mRouteApiRepository.post(param1);
    }
}
