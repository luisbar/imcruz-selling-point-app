package com.imcruzparts.pdv.domain.activity;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.activity.ActivityDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class AddNote extends UseCase {

    private DatabaseRepository mActivityDatabaseRepository;

    public AddNote() {
        this.mActivityDatabaseRepository = new ActivityDatabaseRepository();
    }

    @Override
    public void execute(Object param1, Object param2) {
        mActivityDatabaseRepository.update(ActivityDatabaseRepository.Params.paramsForUpdate(
                Integer.valueOf(param1.toString()),
                "note",
                param2.toString()
        ));
    }
}
