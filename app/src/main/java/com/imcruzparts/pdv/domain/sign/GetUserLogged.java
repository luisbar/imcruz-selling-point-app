package com.imcruzparts.pdv.domain.sign;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.sign.SignDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class GetUserLogged extends UseCase {

    @Override
    public void execute() {
        DatabaseRepository mSignDatabaseRepository = new SignDatabaseRepository();
        mSignDatabaseRepository.queryOne(null);
    }
}
