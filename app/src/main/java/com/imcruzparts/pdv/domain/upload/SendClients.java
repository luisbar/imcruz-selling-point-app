package com.imcruzparts.pdv.domain.upload;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.home.HomeApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class SendClients extends UseCase {

    private ApiRepository mHomeApiRepository;

    public SendClients() {
        this.mHomeApiRepository = new HomeApiRepository();
    }

    @Override
    public void execute() {
        mHomeApiRepository.post(null);
    }
}
