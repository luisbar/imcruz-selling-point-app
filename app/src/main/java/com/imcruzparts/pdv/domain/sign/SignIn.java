package com.imcruzparts.pdv.domain.sign;


import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.sign.SignApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class SignIn extends UseCase<SignIn.Params, Object, Object> {

    private ApiRepository mSignApiRepository;

    public SignIn() {
        this.mSignApiRepository = new SignApiRepository();
    }

    @Override
    public void execute(Params param1) {
        // Aqui se aplica la logica a los Params, por ejemplo cifrar la clave

        mSignApiRepository.post(
                SignApiRepository.Params.forPost(param1.username, param1.password));
    }

    public static final class Params {

        private final String username;
        private final String password;

        public Params(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public static Params forExecute(String username, String password) {
            return new Params(username, password);
        }
    }
}
