package com.imcruzparts.pdv.domain.route;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.seller.SellerApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class GetSellersAndRoutesProgrammed extends UseCase {

    private ApiRepository mSellerApiRepository;

    @Override
    public void execute(Object param1) {
        mSellerApiRepository = new SellerApiRepository((Integer) param1);
        mSellerApiRepository.getMany();
    }
}
