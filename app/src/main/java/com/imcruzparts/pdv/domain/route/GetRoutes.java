package com.imcruzparts.pdv.domain.route;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.route.RouteApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class GetRoutes extends UseCase {

    private ApiRepository mRouteApiRepository;

    public GetRoutes() {
        mRouteApiRepository = new RouteApiRepository();
    }

    @Override
    public void execute() {
        mRouteApiRepository.getMany();
    }
}
