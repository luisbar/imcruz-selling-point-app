package com.imcruzparts.pdv.domain;

/**
 * Interfaz a ser implementada por cada caso de uso
 */
public abstract class UseCase<P1, P2, P3> {

    public void execute(){};
    public void execute(P1 param1){};
    public void execute(P1 param1, P2 param2){};
    public void execute(P1 param1, P2 param2, P3 param3){};
}
