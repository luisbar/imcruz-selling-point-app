package com.imcruzparts.pdv.domain.route;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.sellerroute.RouteApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class RemoveSellerRoute extends UseCase {

    private ApiRepository mSellerRouteApiRepository;

    public RemoveSellerRoute() {
        this.mSellerRouteApiRepository = new RouteApiRepository();
    }

    @Override
    public void execute(Object param1) {
        mSellerRouteApiRepository.deleteOne(param1);
    }
}
