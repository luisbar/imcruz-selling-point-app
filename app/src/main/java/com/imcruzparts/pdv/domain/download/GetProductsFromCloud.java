package com.imcruzparts.pdv.domain.download;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.product.ProductApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

/**
 * Ejecuta {@link ApiRepository#getMany()} para
 * obtener todos los productos almacenados en la nube
 */
public class GetProductsFromCloud extends UseCase {

    ApiRepository mProductApiRepository;

    public GetProductsFromCloud() {
        this.mProductApiRepository = new ProductApiRepository();
    }

    @Override
    public void execute() {
        mProductApiRepository.getMany();
    }
}
