package com.imcruzparts.pdv.domain.visitdetail;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.activity.ActivityDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class CreateActivities extends UseCase {

    private DatabaseRepository mActivityDatabaseRepository;

    public CreateActivities() {
        this.mActivityDatabaseRepository = new ActivityDatabaseRepository();
    }

    @Override
    public void execute(Object param1) {
        mActivityDatabaseRepository.addMany(param1.toString());
    }
}
