package com.imcruzparts.pdv.domain.main;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.main.MainApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class SignOut extends UseCase {

    private ApiRepository mMainApiRepository;

    public SignOut() {
        this.mMainApiRepository = new MainApiRepository();
    }

    @Override
    public void execute() {
        mMainApiRepository.post(null);
    }
}
