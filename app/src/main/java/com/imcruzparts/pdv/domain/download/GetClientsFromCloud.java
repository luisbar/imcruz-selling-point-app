package com.imcruzparts.pdv.domain.download;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.home.HomeApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

/**
 * Ejecuta {@link ApiRepository#getMany()} para obtener
 * todos los clientes almacenados en la nube
 */
public class GetClientsFromCloud extends UseCase {

    private ApiRepository mHomeApiRepository;

    public GetClientsFromCloud() {
        this.mHomeApiRepository = new HomeApiRepository();
    }

    @Override
    public void execute() {
        mHomeApiRepository.getMany();
    }
}
