package com.imcruzparts.pdv.domain.home;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.home.model.ClientModelLocal;
import com.imcruzparts.pdv.domain.UseCase;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Ejecuta metodo de {@link HomeDatabaseRepository} para actualizar
 * un {@link com.imcruzparts.pdv.data.home.model.PdvModelLocal}
 * con su respectivo {@link ClientModelLocal}
 */
public class UpdateClient extends UseCase<UpdateClient.Params,Object,Object> {

    private DatabaseRepository mHomeDatabaseRepository;

    public UpdateClient() {
        this.mHomeDatabaseRepository = new HomeDatabaseRepository(false);
    }

    @Override
    public void execute(UpdateClient.Params param1) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date = dateFormat.format(new Date());

        mHomeDatabaseRepository.update(HomeDatabaseRepository.Params.forUpdate(
                param1.IdPDV,
                param1.Nombre,
                param1.Departamento,
                param1.Direccion,
                param1.Latitud,
                param1.Longitud,
                "MODIFICADO",
                date,
                null,
                param1.IdCliente,
                param1.NombreCliente,
                param1.Apellido,
                param1.NroDocumento,
                param1.Celular,
                param1.Telefono,
                param1.Email,
                "MODIFICADO",
                param1.RazonSocial,
                param1.Nit,
                date,
                null
        ));
    }

    public static class Params {



        private Integer IdPDV;
        private String Nombre;
        private String Departamento;
        private String Direccion;
        private Double Latitud;
        private Double Longitud;

        private Integer IdCliente;
        private String NombreCliente;
        private String Apellido;
        private String NroDocumento;
        private String Celular;
        private String Telefono;
        private String Email;
        private String RazonSocial;
        private String Nit;

        public Params(Integer idPDV, String nombre, String departamento, String direccion,
                      Double latitud, Double longitud, Integer idCliente, String nombreCliente,
                      String apellido, String nroDocumento, String celular, String telefono,
                      String email, String razonSocial, String nit) {
            IdPDV = idPDV;
            Nombre = nombre;
            Departamento = departamento;
            Direccion = direccion;
            Latitud = latitud;
            Longitud = longitud;
            IdCliente = idCliente;
            NombreCliente = nombreCliente;
            Apellido = apellido;
            NroDocumento = nroDocumento;
            Celular = celular;
            Telefono = telefono;
            Email = email;
            RazonSocial = razonSocial;
            Nit = nit;
        }

        public static Params forExecute(Integer idPDV, String nombre, String departamento, String direccion,
                                        Double latitud, Double longitud, Integer idCliente, String nombreCliente,
                                        String apellido, String nroDocumento, String celular, String telefono,
                                        String email, String razonSocial, String nit) {
            return new Params(
                    idPDV,
                    nombre,
                    departamento,
                    direccion,
                    latitud,
                    longitud,
                    idCliente,
                    nombreCliente,
                    apellido,
                    nroDocumento,
                    celular,
                    telefono,
                    email,
                    razonSocial,
                    nit
            );
        }
    }
}
