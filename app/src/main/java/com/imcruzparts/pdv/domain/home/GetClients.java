package com.imcruzparts.pdv.domain.home;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

/**
 * Ejecuta {@link HomeDatabaseRepository#queryMany(String)} para obtener
 * todos los clientes almacenados en la base de datos
 */
public class GetClients extends UseCase {

    private DatabaseRepository mHomeDatabaseRepository;

    public GetClients() {
        this.mHomeDatabaseRepository = new HomeDatabaseRepository(false);
    }

    @Override
    public void execute() {
        mHomeDatabaseRepository.queryMany(null);
    }
}
