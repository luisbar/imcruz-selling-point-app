package com.imcruzparts.pdv.domain.request;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.request.RequestDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class AddNote extends UseCase {

    private DatabaseRepository mRequestDatabaseRepository;

    public AddNote() {
        this.mRequestDatabaseRepository = new RequestDatabaseRepository();
    }

    @Override
    public void execute(Object param1, Object param2) {
        mRequestDatabaseRepository.update(RequestDatabaseRepository.Params.paramsForUpdate(
                Integer.valueOf(param1.toString()),
                "note",
                param2.toString()
        ));
    }
}
