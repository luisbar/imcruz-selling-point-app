package com.imcruzparts.pdv.domain.activity;


import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.sellerroute.RouteDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class FinalizeSellerRoute extends UseCase {

    private DatabaseRepository mRouteDatabaseRepository;

    public FinalizeSellerRoute() {
        this.mRouteDatabaseRepository = new RouteDatabaseRepository("FINALIZADA");
    }

    @Override
    public void execute(Object param1) {
        mRouteDatabaseRepository.update(param1.toString());
    }
}
