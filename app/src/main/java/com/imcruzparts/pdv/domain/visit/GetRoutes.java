package com.imcruzparts.pdv.domain.visit;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.sellerroute.RouteDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class GetRoutes extends UseCase {

    DatabaseRepository mRouteDatabaseRepository;

    public GetRoutes() {
        this.mRouteDatabaseRepository = new RouteDatabaseRepository();
    }

    @Override
    public void execute() {
        mRouteDatabaseRepository.queryMany(null);
    }
}
