package com.imcruzparts.pdv.domain.route;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.route.RouteApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class RemoveAssignedRoute extends UseCase {

    private ApiRepository mRouteApiRepository;

    public RemoveAssignedRoute() {
        mRouteApiRepository = new RouteApiRepository();
    }

    @Override
    public void execute(Object param1) {
        mRouteApiRepository.deleteOne(param1);
    }
}
