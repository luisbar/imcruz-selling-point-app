package com.imcruzparts.pdv.domain.visit;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.sellerroute.RouteApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class GetRoutesFromCloud extends UseCase {

    private ApiRepository mRouteApiRepository;

    public GetRoutesFromCloud() {
        this.mRouteApiRepository = new RouteApiRepository();
    }

    @Override
    public void execute() {
        mRouteApiRepository.getMany();
    }
}
