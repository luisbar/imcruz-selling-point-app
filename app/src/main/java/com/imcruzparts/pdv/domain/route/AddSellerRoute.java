package com.imcruzparts.pdv.domain.route;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.sellerroute.RouteApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class AddSellerRoute extends UseCase {

    private ApiRepository mSellerRouteApiRepository;

    public AddSellerRoute() {
        this.mSellerRouteApiRepository = new RouteApiRepository();
    }

    @Override
    public void execute(Object param1) {
        mSellerRouteApiRepository.post(param1);
    }
}
