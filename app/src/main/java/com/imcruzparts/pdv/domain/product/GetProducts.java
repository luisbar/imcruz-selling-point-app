package com.imcruzparts.pdv.domain.product;

import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.product.ProductDatabaseRepository;
import com.imcruzparts.pdv.domain.UseCase;

/**
 * Ejecuta {@link ProductDatabaseRepository#queryMany(String)} para
 * obtener todos los productos almacenados en la base de datos
 */
public class GetProducts extends UseCase {

    DatabaseRepository mProductDatabaseRepository;

    public GetProducts() {
        this.mProductDatabaseRepository = new ProductDatabaseRepository();
    }

    @Override
    public void execute() {
        mProductDatabaseRepository.queryMany(null);
    }
}
