package com.imcruzparts.pdv.domain.upload;

import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.activity.ActivityApiRepository;
import com.imcruzparts.pdv.domain.UseCase;

public class SendActivities extends UseCase {

    private ApiRepository mActivityApiRepository;

    public SendActivities() {
        this.mActivityApiRepository = new ActivityApiRepository();
    }

    @Override
    public void execute(Object param1) {
        mActivityApiRepository.post(param1);
    }
}
