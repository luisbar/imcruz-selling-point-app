package com.imcruzparts.pdv.common;

/**
 * Objeto generico para disparar los escuchadores
 * en los presenters y las views
 */
public class Action {

    private String type;
    private Object payload;
    private boolean error;

    public Action(String type) {
        this.type = type;
    }

    public Action(String type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public Action(String type, Object payload, boolean error) {
        this.type = type;
        this.payload = payload;
        this.error = error;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
