package com.imcruzparts.pdv.common;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Santa Cruz - Bolivia
 * PDV
 * com.imcruzparts.pdv.common
 * 18/07/2017 - 01:06
 * Created by: Ayrton Argani
 */
public class DateDeserializer implements JsonDeserializer<Date>
{
    @Override
    public Date deserialize(JsonElement json, Type typfOfT, JsonDeserializationContext context)
    {
        try
        {
            String dateStr = json.getAsString();
            if (dateStr != null) dateStr = dateStr.replace("/Date(", "");
            if (dateStr != null) dateStr = dateStr.replace("+0000)/", "");
            if (dateStr != null) dateStr = dateStr.replace(")/", "");
            long time = Long.parseLong(dateStr);
            return new Date(time);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }

    }
}