package com.imcruzparts.pdv.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
/**
 * Santa Cruz - Bolivia
 * PDV
 * com.imcruzparts.pdv.common
 * 18/07/2017 - 01:01
 * Created by: Ayrton Argani
 */
public class DateSerializer implements JsonSerializer<Date>
{
    @Override
    public JsonElement serialize(Date date, Type typfOfT, JsonSerializationContext context)
    {
        if (date == null)
            return null;

        String dateStr = "/Date(" + date.getTime() + ")/";
        return new JsonPrimitive(dateStr);
    }
}