package com.imcruzparts.pdv.common;

/**
 * Interfaz a ser implementada por todos los mappers
 * @param <From> objeto a mapear
 * @param <To> objeto mapeado
 */
public interface Mapper<From, To> {

    To map(From from);
}
