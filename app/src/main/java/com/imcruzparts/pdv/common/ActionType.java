package com.imcruzparts.pdv.common;

import com.imcruzparts.pdv.presentation.product.ProductView;

/**
 * Todas las acciones realizadas por la app divididas
 * por clases
 */
public class ActionType {
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.sign.SignView}
     */
    public static final String VALIDATED_PASSWORD = "validatedPassword";
    public static final String VALIDATED_USERNAME = "validatedUsername";

    public static final String THERE_IS_USER_LOGGED = "thereIsUserLogged";
    public static final String SIGN_IN_SUCCESS_V = "signInSuccessV";
    public static final String SIGN_IN_ERROR_V = "signInErrorV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.sign.SignPresenter}
     */
    public static final String CHECK_USER_LOGGED = "checkUserLogged";
    public static final String SIGN_IN_SUCCESS_P = "signInSuccessP";
    public static final String SIGN_IN_ERROR_P = "signInErrorP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation.ClientInformationView}
     */
    public static final String VALIDATED_FIRST_NAME = "validatedFirstName";
    public static final String VALIDATED_LAST_NAME = "validatedLastName";
    public static final String VALIDATED_DOCUMENT_NUMBER = "validatedDocumentNumber";
    public static final String VALIDATED_CELL_PHONE_NUMBER = "validatedCellPhoneNumber";
    public static final String VALIDATED_PHONE_NUMBER = "validatedPhoneNumber";
    public static final String VALIDATED_EMAIL = "validatedEmail";
    public static final String VALIDATED_SOCIAL_REASON = "validatedSocialReason";
    public static final String VALIDATED_NIT = "validatedNit";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation.BusinessInformationView}
     */
    public static final String VALIDATED_NAME = "validatedName";
    public static final String VALIDATED_STATE = "validatedState";
    public static final String VALIDATED_ADDRESS = "validatedAddress";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.home.presentation.client.ClientView}
     */
    public static final String ADD_CLIENT_SUCCESS_V = "addClientSuccessV";
    public static final String ADD_CLIENT_ERROR_V = "addClientErrorV";
    public static final String UPDATE_CLIENT_SUCCESS_V = "updateClientSuccessV";
    public static final String UPDATE_CLIENT_ERROR_V = "updateClientErrorV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.home.presentation.client.ClientPresenter}
     */
    public static final String ADD_CLIENT_SUCCESS_P = "addClientSuccessP";
    public static final String ADD_CLIENT_ERROR_P = "addClientErrorP";
    public static final String UPDATE_CLIENT_SUCCESS_P = "updateClientSuccessP";
    public static final String UPDATE_CLIENT_ERROR_P = "updateClientErrorP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.home.HomeView}
     */
    public static final String CLIENTS_OBTAINED_V = "clientsObtainedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.home.HomePresenter}
     */
    public static final String CLIENTS_OBTAINED_P = "clientsObtainedP";
    /**
     * Acciones de {@link ProductView}
     */
    public static final String PRODUCTS_OBTAINED_V = "productsObtainedV";
    public static final String TYPES_OBTAINED_V = "typesObtainedV";
    public static final String PRODUCTS_FILTERED_BY_TYPE = "productsFilteredByType";
    public static final String PRODUCTS_FILTERED_BY_BRAND = "productsFilteredByBrand";
    public static final String PRODUCTS_FILTERED_BY_SEGMENT = "productsFilteredBySegment";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.product.ProductPresenter}
     */
    public static final String PRODUCTS_OBTAINED_P = "productsObtainedP";
    public static final String TYPES_OBTAINED_P = "typessObtainedP";

    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.MainActivity}
     */
    public static final String SIGN_OUT_SUCCESS_V = "signOutSuccessV";
    public static final String SIGN_OUT_ERROR_V = "signOutErrorV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.MainPresenter}
     */
    public static final String SIGN_OUT_SUCCESS_P = "signOutSuccessP";
    public static final String SIGN_OUT_ERROR_P = "signOutErrorP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.VisitView}
     */
    public static final String PENDING_ROUTES_OBTAINED_V = "pendingRoutesObtainedV";
    public static final String PENDING_ROUTES_NOT_OBTAINED_V = "pendingRoutesNotObtainedV";
    public static final String INITIALIZED_ROUTES_OBTAINED_V = "initializedRoutesObtainedV";

    public static final String PENDING_REQUEST_OBTAINED_V = "pendingRequestObtainedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.VisitPresenter}
     */
    public static final String PENDING_ROUTES_OBTAINED_P = "pendingRoutesObtainedP";
    public static final String PENDING_ROUTES_NOT_OBTAINED_P = "pendingRoutesNotObtainedP";
    public static final String INITIALIZED_ROUTES_OBTAINED_P = "initializedRoutesObtainedP";

    public static final String PENDING_REQUEST_OBTAINED_P = "pendingRequestObtainedP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.presentation.visitdetail.VisitDetailView}
     */
    public static final String ACTIVITIES_CREATED_V = "activitiesCreatedV";
    public static final String ACTIVITIES_NOT_CREATED_V = "activitiesNotCreatedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.presentation.visitdetail.VisitDetailPresenter}
     */
    public static final String ACTIVITIES_CREATED_P = "activitiesCreatedP";
    public static final String ACTIVITIES_NOT_CREATED_P = "activitiesNotCreatedP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityView}
     */
    public static final String ACTIVITY_FINALIZED_V = "activityFinalizedV";
    public static final String ACTIVITY_NOT_FINALIZED_V = "activityNotFinalizedV";
    public static final String NOTE_ADDED_V = "noteAddedV";
    public static final String NOTE_NOT_ADDED_V = "noteNotAddedV";

    public static final String REQUEST_DELIVERED_V = "requestDeliveredV";
    public static final String REQUEST_NOT_DELIVERED_V = "requestNotDeliveredV";
    public static final String NOTE_ADDED_TO_REQUEST_V = "noteAddedToRequestV";
    public static final String NOTE_NOT_ADDED_TO_REQUEST_V = "noteNotAddedToRequestV";

    public static final String SELLER_ROUTE_FINALIZE_V = "sellerRouteFinalizeV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityPresenter}
     */
    public static final String ACTIVITY_FINALIZED_P = "activityFinalizedP";
    public static final String ACTIVITY_NOT_FINALIZED_P = "activityNotFinalizedP";
    public static final String NOTE_ADDED_P = "noteAddedP";
    public static final String NOTE_NOT_ADDED_P = "noteNotAddedP";

    public static final String REQUEST_DELIVERED_P = "requestDeliveredP";
    public static final String REQUEST_NOT_DELIVERED_P = "requestNotDeliveredP";
    public static final String NOTE_ADDED_TO_REQUEST_P = "noteAddedToRequestP";
    public static final String NOTE_NOT_ADDED_TO_REQUEST_P = "noteNotAddedToRequestP";

    public static final String SELLER_ROUTE_FINALIZE_P = "sellerRouteFinalizeP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestView}
     */
    public static final String REQUEST_ADDED_V = "requestAddedV";
    public static final String REQUEST_NOT_ADDED_V = "requestNotAddedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestPresenter}
     */
    public static final String REQUEST_ADDED_P = "requestAddedP";
    public static final String REQUEST_NOT_ADDED_P = "requestNotAddedP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.download.DownloadView}
     */
    public static final String CLIENTS_DOWNLOADED_V = "clientsDownloadedV";
    public static final String PRODUCTS_DOWNLOADED_V = "productsDownloadedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.download.DownloadPresenter}
     */
    public static final String CLIENTS_DOWNLOADED_P = "clientsDownloadedP";
    public static final String PRODUCTS_DOWNLOADED_P = "productsDownloadedP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.upload.UploadView}
     */
    public static final String CLIENTS_UPLOADED_V = "clientsUploadedV";
    public static final String ACTIVITIES_UPLOADED_V = "activitiesUploadedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.upload.UploadPresenter}
     */
    public static final String CLIENTS_UPLOADED_P = "clientsUploadedP";
    public static final String ACTIVITIES_UPLOADED_P = "activitiesUploadedp";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.tracking.TrackingService}
     */
    public static final String GET_PENDING_VISITS = "getPendingVisits";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.route.RouteView}
     */
    public static final String ROUTES_GOTTEN_V = "routesGottenV";
    public static final String SELLING_POINT_ROUTES_GOTTEN_V = "sellingPointRoutesGottenV";
    public static final String ROUTES_PROGRAMMED_V = "routesProgrammedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.route.RoutePresenter}
     */
    public static final String ROUTES_GOTTEN_P = "routesGottenP";
    public static final String SELLING_POINT_ROUTES_GOTTEN_P = "sellingPointRoutesGottenP";
    public static final String ROUTES_PROGRAMMED_P = "routesProgrammedP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute.SellingPointRouteView}
     */
    public static final String ROUTE_ADDED_V = "routeAddedV";
    public static final String ROUTE_REMOVED_V = "routeRemovedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute.SellingPointRoutePresenter}
     */
    public static final String ROUTE_ADDED_P = "routeAddedP";
    public static final String ROUTE_REMOVED_P = "routeRemovedP";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.route.presentation.schedule.ScheduleView}
     */
    public static final String SELLER_ROUTE_ADDED_V = "sellerRouteAddedV";
    public static final String SELLER_ROUTE_REMOVED_V = "sellerRouteRemovedV";
    /**
     * Acciones de {@link com.imcruzparts.pdv.presentation.route.presentation.schedule.SchedulePresenter}
     */
    public static final String SELLER_ROUTE_ADDED_P = "sellerRouteAddedP";
    public static final String SELLER_ROUTE_REMOVED_P = "sellerRouteRemovedP";
}
