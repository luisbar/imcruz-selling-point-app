package com.imcruzparts.pdv.common;


import org.greenrobot.eventbus.EventBus;

/**
 * Mascara para el uso del patron pusblisher/subscriber
 * mediante la libreria EventBus
 */
public class EventBusMask {
    /**
     * Registra escuchadores para el objeto especificado
     * @param subscriber objeto a registrar sus escuchadores
     */
    public static void register(Object subscriber) {
        EventBus.getDefault().register(subscriber);
    }

    /**
     * Elimina escuchadores para el objeto especificado
     * @param subscriber objeto a eliminar sus escuchadores
     */
    public static void unregister(Object subscriber) {
        EventBus.getDefault().unregister(subscriber);
    }
    /**
     * Dispara los escuchadores de acuerdo a la acción especificada, como redux
     * @param action objeto de tipo {@link Action}
     */
    public static void post(Object action) {
        EventBus.getDefault().post(action);
    }
}
