package com.imcruzparts.pdv.presentation.visit.util;

import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.request.model.RequestDetailModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Mapea objeto de List<{@link RequestModelLocal}>
 * a List<{@link RequestModel}>
 */
public class RmlToRm implements Mapper<List<RequestModelLocal>, List<RequestModel>> {

    @Override
    public List<RequestModel> map(List<RequestModelLocal> requestModelLocals) {
        List<RequestModel> lstRequest = new ArrayList();

        for (RequestModelLocal requestModelLocal : requestModelLocals) {
            lstRequest.add(
                    new RequestModel(
                            requestModelLocal.getIdPedido(),
                            requestModelLocal.getIdPdv(),
                            requestModelLocal.getFecha(),
                            requestModelLocal.getEstado(),
                            requestModelLocal.getSubTotal(),
                            requestModelLocal.getDescuento(),
                            requestModelLocal.getMontoTotal(),
                            getDetail(requestModelLocal.getDetalle())
                    )
            );
        }

        return lstRequest;
    }

    private List<RequestModel.RequestDetail> getDetail(List<RequestDetailModelLocal> requestDetailModelLocals) {
        List<RequestModel.RequestDetail> lstDetail = new ArrayList();

        for (RequestDetailModelLocal requestDetailModelLocal : requestDetailModelLocals) {
            lstDetail.add(
                    new RequestModel.RequestDetail(
                            requestDetailModelLocal.getIdDetalle(),
                            requestDetailModelLocal.getCantidad(),
                            requestDetailModelLocal.getPrecio(),
                            requestDetailModelLocal.getDescuento(),
                            requestDetailModelLocal.getSubTotal(),
                            requestDetailModelLocal.getFechaAlta(),
                            requestDetailModelLocal.getFechaBaja(),
                            requestDetailModelLocal.getProducto().getIdProducto(),
                            requestDetailModelLocal.getProducto().getCiaCod(),
                            requestDetailModelLocal.getProducto().getCodProducto(),
                            requestDetailModelLocal.getProducto().getDescripcion(),
                            requestDetailModelLocal.getProducto().getSegmento(),
                            requestDetailModelLocal.getProducto().getCanal(),
                            requestDetailModelLocal.getProducto().getFabrica(),
                            requestDetailModelLocal.getProducto().getOrigen(),
                            requestDetailModelLocal.getProducto().getFamilia(),
                            requestDetailModelLocal.getProducto().getMarca(),
                            requestDetailModelLocal.getProducto().getGrupo(),
                            requestDetailModelLocal.getProducto().getTipo(),
                            requestDetailModelLocal.getProducto().getPrecio(),
                            requestDetailModelLocal.getProducto().getDescuento()
                    )
            );
        }

        return lstDetail;
    }
}
