package com.imcruzparts.pdv.presentation.product;

import java.io.Serializable;

public class ProductModel implements Serializable {

    private Integer productId;
    private String ciaCode;
    private String productCode;
    private String description;
    private String segment;
    private String channel;
    private String fabric;
    private String origin;
    private String family;
    private String brand;
    private String group;
    private String type;
    private Double price;
    private Double discount;
    // Propiedades utilizadas cuando se utiliza ProductView
    // para realizar un nuevo pedido
    private Integer quantity;
    private Double total;

    public ProductModel(Integer productId, String ciaCode, String productCode, String description, String segment, String channel, String fabric, String origin, String family, String brand, String group, String type, Double price, Double discount) {
        this.productId = productId;
        this.ciaCode = ciaCode;
        this.productCode = productCode;
        this.description = description;
        this.segment = segment;
        this.channel = channel;
        this.fabric = fabric;
        this.origin = origin;
        this.family = family;
        this.brand = brand;
        this.group = group;
        this.type = type;
        this.price = price;
        this.discount = discount;
    }

    public ProductModel(Integer productId, String ciaCode, String productCode, String description, String segment, String channel, String fabric, String origin, String family, String brand, String group, String type, Double price, Double discount, Integer quantity, Double total) {
        this.productId = productId;
        this.ciaCode = ciaCode;
        this.productCode = productCode;
        this.description = description;
        this.segment = segment;
        this.channel = channel;
        this.fabric = fabric;
        this.origin = origin;
        this.family = family;
        this.brand = brand;
        this.group = group;
        this.type = type;
        this.price = price;
        this.discount = discount;
        this.quantity = quantity;
        this.total = total;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getCiaCode() {
        return ciaCode;
    }

    public void setCiaCode(String ciaCode) {
        this.ciaCode = ciaCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getFabric() {
        return fabric;
    }

    public void setFabric(String fabric) {
        this.fabric = fabric;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object obj) {
        ProductModel productModel = (ProductModel) obj;
        return this.productId.intValue() == productModel.productId.intValue();
    }
}
