package com.imcruzparts.pdv.presentation.visit.presentation.visitdetail;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.visit.VisitModel;
import com.imcruzparts.pdv.presentation.visit.VisitView;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityView;

import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class VisitDetailView extends Fragment {


    @BindView(R.id.input_visit_detail_route_name)
    EditText inputVisitDetailRouteName;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable closeIcon;

    @BindString(R.string.txt_74)
    String txt74;
    @BindString(R.string.txt_86)
    String txt86;

    Unbinder unbinder;
    @BindView(R.id.input_visit_detail_route_description)
    EditText inputVisitDetailRouteDescription;
    @BindView(R.id.input_visit_detail_route_country)
    EditText inputVisitDetailRouteCountry;
    @BindView(R.id.input_visit_detail_route_zone)
    EditText inputVisitDetailRouteZone;
    @BindView(R.id.input_visit_detail_route_distributor)
    EditText inputVisitDetailRouteDistributor;
    @BindView(R.id.input_visit_detail_route_supervisor)
    EditText inputVisitDetailRouteSupervisor;
    @BindView(R.id.input_visit_detail_route_state)
    EditText inputVisitDetailRouteState;
    @BindView(R.id.input_visit_detail_route_order_date)
    EditText inputVisitDetailRouteOrderDate;
    @BindView(R.id.layout_progress)
    LinearLayout layoutProgress;

    private MainActivity mMainActivity;
    private VisitDetailPresenter mVisitDetailPresenter;
    private VisitModel mVisitModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visit_detail, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);
        mVisitDetailPresenter = new VisitDetailPresenter();

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt74);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(closeIcon);

        loadData();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mVisitDetailPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mVisitDetailPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_visit_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                mMainActivity.onBackPressed();
                break;

            case R.id.action_init_route:

                mVisitDetailPresenter.createActivities(mVisitModel.getId());
                layoutProgress.setVisibility(View.VISIBLE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.ACTIVITIES_CREATED_V:
                Fragment mapView = new ActivityView();
                Bundle args = new Bundle();

                args.putSerializable("Activities", (Serializable) action.getPayload());
                args.putSerializable("Requests", new ArrayList());
                mapView.setArguments(args);

                mMainActivity.removeCurrentFragment(this);
                mMainActivity.loadFragment(R.id.container, mapView, "ActivityView");
                break;

            case ActionType.ACTIVITIES_NOT_CREATED_V:
                mMainActivity.showSnackBar(txt86);
                break;
        }
    }

    /**
     * Carga el detalle de la ruta en la vista
     */
    private void loadData() {
        mVisitModel = (VisitModel) getArguments().getSerializable("VisitModel");

        inputVisitDetailRouteName.setText(mVisitModel.getRoute().getName());
        inputVisitDetailRouteDescription.setText(mVisitModel.getRoute().getDescription());
        inputVisitDetailRouteCountry.setText(mVisitModel.getRoute().getCountry());
        inputVisitDetailRouteZone.setText(mVisitModel.getRoute().getZone());
        inputVisitDetailRouteDistributor.setText(mVisitModel.getDistributorName());
        inputVisitDetailRouteSupervisor.setText(mVisitModel.getSupervisorName());
        inputVisitDetailRouteState.setText(mVisitModel.getState());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        inputVisitDetailRouteOrderDate.setText(dateFormat.format(mVisitModel.getOrderDate()));
    }
}
