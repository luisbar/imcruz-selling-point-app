package com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.home.HomeModel;
import com.imcruzparts.pdv.presentation.home.presentation.client.ClientView;
import com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation.BusinessInformationView;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.greenrobot.eventbus.Subscribe;

import java.util.Iterator;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class ClientInformationView extends Fragment implements BlockingStep, ClientView.OnAddClientListener {

    @BindView(R.id.input_client_information_first_name)
    EditText inputClientInformationFirstName;
    @BindView(R.id.input_client_information_last_name)
    EditText inputClientInformationLastName;
    @BindView(R.id.input_client_information_document_number)
    EditText inputClientInformationDocumentNumber;
    @BindView(R.id.input_client_information_cell_phone_number)
    EditText inputClientInformationCellPhoneNumber;
    @BindView(R.id.input_client_information_phone_number)
    EditText inputClientInformationPhoneNumber;
    @BindView(R.id.input_client_information_email)
    EditText inputClientInformationEmail;
    @BindView(R.id.input_client_information_social_reason)
    EditText inputClientInformationSocialReason;
    @BindView(R.id.input_client_information_nit)
    EditText inputClientInformationNit;

    @BindString(R.string.txt_28)
    String txt28;
    @BindString(R.string.txt_29)
    String txt29;
    @BindString(R.string.txt_30)
    String txt30;
    @BindString(R.string.txt_31)
    String txt31;
    @BindString(R.string.txt_32)
    String txt32;
    @BindString(R.string.txt_33)
    String txt33;
    @BindString(R.string.txt_34)
    String txt34;
    @BindString(R.string.txt_35)
    String txt35;
    @BindString(R.string.txt_36)
    String txt36;
    @BindView(R.id.scroll_client_information)
    ScrollView scrollClientInformation;

    private boolean mFirstName;
    private boolean mLastName;
    private boolean mDocumentNumber;
    private boolean mCellPhoneNumber;
    private boolean mPhoneNumber;
    private boolean mEmail;
    private boolean mSocialReason;
    private boolean mNit;

    private Unbinder mUnbinder;
    private ClientInformationPresenter mClientInformationPresenter;
    private MainActivity mMainActivity;
    private ClientInformationReceiver mClientInformationReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_information, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mClientInformationPresenter = new ClientInformationPresenter();
        mMainActivity = (MainActivity) this.getActivity();
        mClientInformationReceiver = (ClientInformationReceiver) getBusinessFragment();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadDataForUpdating();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    /**
     * Ejecuta metodos de {@link ClientInformationPresenter} que valida los inputs
     */
    @OnTextChanged({
            R.id.input_client_information_first_name,
            R.id.input_client_information_last_name,
            R.id.input_client_information_document_number,
            R.id.input_client_information_cell_phone_number,
            R.id.input_client_information_phone_number,
            R.id.input_client_information_email,
            R.id.input_client_information_social_reason,
            R.id.input_client_information_nit
    })
    public void onTextChanged() {
        if (inputClientInformationFirstName.isFocused())
            mClientInformationPresenter.validateFirstName(inputClientInformationFirstName.getText().toString());
        if (inputClientInformationLastName.isFocused())
            mClientInformationPresenter.validateLastName(inputClientInformationLastName.getText().toString());
        if (inputClientInformationDocumentNumber.isFocused())
            mClientInformationPresenter.validateDocumentNumber(inputClientInformationDocumentNumber.getText().toString());
        if (inputClientInformationCellPhoneNumber.isFocused())
            mClientInformationPresenter.validateCellPhoneNumber(inputClientInformationCellPhoneNumber.getText().toString());
        if (inputClientInformationPhoneNumber.isFocused())
            mClientInformationPresenter.validatePhoneNumber(inputClientInformationPhoneNumber.getText().toString());
        if (inputClientInformationEmail.isFocused())
            mClientInformationPresenter.validateEmail(inputClientInformationEmail.getText().toString());
        if (inputClientInformationSocialReason.isFocused())
            mClientInformationPresenter.validateSocialReason(inputClientInformationSocialReason.getText().toString());
        if (inputClientInformationNit.isFocused())
            mClientInformationPresenter.validateNit(inputClientInformationNit.getText().toString());

        mClientInformationReceiver.setClientInformation(
                inputClientInformationFirstName.getText().toString(),
                inputClientInformationLastName.getText().toString(),
                inputClientInformationDocumentNumber.getText().toString(),
                inputClientInformationCellPhoneNumber.getText().toString(),
                inputClientInformationPhoneNumber.getText().toString(),
                inputClientInformationEmail.getText().toString(),
                inputClientInformationSocialReason.getText().toString(),
                inputClientInformationNit.getText().toString()
        );
    }

    /**
     * Si algun input es invalido entonces no pasa a {@link BusinessInformationView}
     *
     * @return objecto error
     */
    @Override
    public VerificationError verifyStep() {
        return mFirstName &&
                mLastName &&
                mDocumentNumber &&
                mCellPhoneNumber &&
                mPhoneNumber &&
                mEmail &&
                mSocialReason &&
                mNit
                ? null
                : new VerificationError(txt28);
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        mClientInformationPresenter.validateFirstName(inputClientInformationFirstName.getText().toString());
        mClientInformationPresenter.validateLastName(inputClientInformationLastName.getText().toString());
        mClientInformationPresenter.validateDocumentNumber(inputClientInformationDocumentNumber.getText().toString());
        mClientInformationPresenter.validateCellPhoneNumber(inputClientInformationCellPhoneNumber.getText().toString());
        mClientInformationPresenter.validatePhoneNumber(inputClientInformationPhoneNumber.getText().toString());
        mClientInformationPresenter.validateEmail(inputClientInformationEmail.getText().toString());
        mClientInformationPresenter.validateSocialReason(inputClientInformationSocialReason.getText().toString());
        mClientInformationPresenter.validateNit(inputClientInformationNit.getText().toString());

        mMainActivity.showSnackBar(error.getErrorMessage());
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        mClientInformationReceiver.setClientInformation(
                inputClientInformationFirstName.getText().toString(),
                inputClientInformationLastName.getText().toString(),
                inputClientInformationDocumentNumber.getText().toString(),
                inputClientInformationCellPhoneNumber.getText().toString(),
                inputClientInformationPhoneNumber.getText().toString(),
                inputClientInformationEmail.getText().toString(),
                inputClientInformationSocialReason.getText().toString(),
                inputClientInformationNit.getText().toString()
        );
        callback.goToNextStep();
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        callback.complete();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     *
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.VALIDATED_FIRST_NAME:
                mFirstName = (boolean) action.getPayload();

                if (!mFirstName) inputClientInformationFirstName.setError(txt29);
                break;

            case ActionType.VALIDATED_LAST_NAME:
                mLastName = (boolean) action.getPayload();

                if (!mLastName) inputClientInformationLastName.setError(txt30);
                break;

            case ActionType.VALIDATED_DOCUMENT_NUMBER:
                mDocumentNumber = (boolean) action.getPayload();

                if (!mDocumentNumber) inputClientInformationDocumentNumber.setError(txt31);
                break;

            case ActionType.VALIDATED_CELL_PHONE_NUMBER:
                mCellPhoneNumber = (boolean) action.getPayload();

                if (!mCellPhoneNumber) inputClientInformationCellPhoneNumber.setError(txt32);
                break;

            case ActionType.VALIDATED_PHONE_NUMBER:
                mPhoneNumber = (boolean) action.getPayload();

                if (!mPhoneNumber) inputClientInformationPhoneNumber.setError(txt33);
                break;

            case ActionType.VALIDATED_EMAIL:
                mEmail = (boolean) action.getPayload();

                if (!mEmail) inputClientInformationEmail.setError(txt34);
                break;

            case ActionType.VALIDATED_SOCIAL_REASON:
                mSocialReason = (boolean) action.getPayload();

                if (!mSocialReason) inputClientInformationSocialReason.setError(txt35);
                break;

            case ActionType.VALIDATED_NIT:
                mNit = (boolean) action.getPayload();

                if (!mNit) inputClientInformationNit.setError(txt36);
                break;
        }
    }

    /**
     * Navega la pila de fragment en busca de {@link BusinessInformationView}
     *
     * @return {@link BusinessInformationView} object
     */
    private Fragment getBusinessFragment() {
        Iterator<Fragment> fragments = mMainActivity.getSupportFragmentManager().getFragments().iterator();

        while (fragments.hasNext()) {
            Fragment fragment = fragments.next();
            if (fragment instanceof BusinessInformationView)
                return fragment;
        }

        return null;
    }

    @Override
    public void saveNewClient() {
        // TODO: nada
    }

    @Override
    public void updateClient() {
        // TODO: nada
    }

    @Override
    public void clearBusinessInformationView() {
        // TODO: nada
    }

    /**
     * Esuchador que se dispara cuando se a guardado un cliente,
     * de {@link ClientView}, restaura la vista a su
     * estado inicial
     */
    @Override
    public void clearClientInformationView() {
        inputClientInformationFirstName.getText().clear();
        inputClientInformationLastName.getText().clear();
        inputClientInformationDocumentNumber.getText().clear();
        inputClientInformationCellPhoneNumber.getText().clear();
        inputClientInformationPhoneNumber.getText().clear();
        inputClientInformationEmail.getText().clear();
        inputClientInformationSocialReason.getText().clear();
        inputClientInformationNit.getText().clear();

        mFirstName = false;
        mLastName = false;
        mDocumentNumber = false;
        mCellPhoneNumber = false;
        mPhoneNumber = false;
        mEmail = false;
        mSocialReason = false;
        mNit = false;

        inputClientInformationFirstName.requestFocus();
        scrollClientInformation.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void loadBusinessInformation(HomeModel homeModel) {
        // TODO: nada
    }

    @Override
    public void loadClientInformation(HomeModel.ClientModel clientModel) {
        inputClientInformationFirstName.setText(clientModel.getFirstName());
        inputClientInformationLastName.setText(clientModel.getLastName());
        inputClientInformationDocumentNumber.setText(clientModel.getDocumentNumber());
        inputClientInformationCellPhoneNumber.setText(clientModel.getCellPhoneNumber());
        inputClientInformationPhoneNumber.setText(clientModel.getPhoneNumber());
        inputClientInformationEmail.setText(clientModel.getEmail());
        inputClientInformationSocialReason.setText(clientModel.getSocialReason());
        inputClientInformationNit.setText(clientModel.getNit());

        mFirstName = true;
        mLastName = true;
        mDocumentNumber = true;
        mCellPhoneNumber = true;
        mPhoneNumber = true;
        mEmail = true;
        mSocialReason = true;
        mNit = true;

        inputClientInformationFirstName.requestFocus();
    }

    /**
     * Metodo que ejecuta el metodo {@link ClientView#loadDataForClientInformationView()}
     */
    public void loadDataForUpdating() {
        ClientView clientView = (ClientView) mMainActivity.getSupportFragmentManager().findFragmentByTag("ClientView");
        clientView.loadDataForClientInformationView();
    }

    /**
     * Interface que es utilizada por {@link BusinessInformationView} y es ejecutada
     * por el metodo {@link #onNextClicked}
     */
    public interface ClientInformationReceiver {

        void setClientInformation(String firstName, String lastName, String documentNumber,
                                  String cellPhoneNumber, String phoneNumber, String email,
                                  String socialReason, String nit);
    }
}
