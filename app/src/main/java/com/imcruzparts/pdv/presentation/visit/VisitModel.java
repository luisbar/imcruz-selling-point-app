package com.imcruzparts.pdv.presentation.visit;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class VisitModel implements Serializable {

    private Integer id;
    private Route route;
    private String distributorName;
    private String supervisorName;
    private Employee employee;
    private List<Business> businesses;
    private String description;
    private Date orderDate;
    private String state;
    private String created;
    private String deleted;

    public VisitModel(Integer id, String distributorName, String supervisorName, String description,
                      Date orderDate, String state, String created, String deleted,
                      Integer idRoute, String name, String routeDescription, String country,
                      String zone, String createdRoute, String deletedRoute,
                      Integer idEmployee, String lastName, String address, String email,
                      String createdEmployee, String deletedEmployee, String firstName,
                      String role, String phoneNumber,
                      List<Business> businesses) {
        this.id = id;
        this.distributorName = distributorName;
        this.supervisorName = supervisorName;
        this.description = description;
        this.orderDate = orderDate;
        this.state = state;
        this.created = created;
        this.deleted = deleted;
        this.route = new Route(idRoute, name, routeDescription, country, zone, createdRoute, deletedRoute);
        this.employee = new Employee(idEmployee,lastName, address, email, createdEmployee,
                deletedEmployee, firstName, role, phoneNumber);
        this.businesses = businesses;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public class Route implements Serializable {

        private Integer id;
        private String name;
        private String description;
        private String country;
        private String zone;
        private String created;
        private String deleted;

        public Route(Integer id, String name, String description, String country, String zone, String created, String deleted) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.country = country;
            this.zone = zone;
            this.created = created;
            this.deleted = deleted;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }
    }

    public class Employee implements Serializable {

        private Integer id;
        private String lastName;
        private String address;
        private String email;
        private String created;
        private String deleted;
        private String firstName;
        private String role;
        private String phoneNumber;

        public Employee(Integer id, String lastName, String address, String email, String created, String deleted, String firstName, String role, String phoneNumber) {
            this.id = id;
            this.lastName = lastName;
            this.address = address;
            this.email = email;
            this.created = created;
            this.deleted = deleted;
            this.firstName = firstName;
            this.role = role;
            this.phoneNumber = phoneNumber;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }

    public static class Business implements Serializable {

        private Integer id;
        private String name;
        private String country;
        private String address;
        private Double latitude;
        private Double longitude;
        private String state;
        private String created;
        private String deleted;

        public Business(Integer id, String name, String country, String address, Double latitude, Double longitude, String state, String created, String deleted) {
            this.id = id;
            this.name = name;
            this.country = country;
            this.address = address;
            this.latitude = latitude;
            this.longitude = longitude;
            this.state = state;
            this.created = created;
            this.deleted = deleted;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }
    }
}
