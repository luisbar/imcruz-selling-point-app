package com.imcruzparts.pdv.presentation.visit.presentation.activity;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.activity.AddNote;
import com.imcruzparts.pdv.domain.activity.FinalizeActivity;
import com.imcruzparts.pdv.domain.activity.FinalizeSellerRoute;
import com.imcruzparts.pdv.domain.request.DeliverRequest;

import org.greenrobot.eventbus.Subscribe;

/**
 * Clase que valida la vista {@link ActivityView},
 * y dispara el escuchador {@link ActivityView#onMessageEvent(Action)}
 */
public class ActivityPresenter {

    private UseCase mFinalizeActivity;
    private UseCase mAddNote;
    private UseCase mDeliverRequest;
    private UseCase mAddNoteToRequest;
    private UseCase mFinzalizeSellerRoute;

    public ActivityPresenter() {
        this.mFinalizeActivity = new FinalizeActivity();
        this.mAddNote = new AddNote();
        this.mDeliverRequest = new DeliverRequest();
        this.mAddNoteToRequest = new com.imcruzparts.pdv.domain.request.AddNote();
        this.mFinzalizeSellerRoute = new FinalizeSellerRoute();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.ACTIVITY_FINALIZED_P:
                EventBusMask.post(new Action(ActionType.ACTIVITY_FINALIZED_V));
                break;

            case ActionType.ACTIVITY_NOT_FINALIZED_P:
                EventBusMask.post(new Action(ActionType.ACTIVITY_NOT_FINALIZED_V));
                break;

            case ActionType.NOTE_ADDED_P:
                EventBusMask.post(new Action(ActionType.NOTE_ADDED_V));
                break;

            case ActionType.NOTE_NOT_ADDED_P:
                EventBusMask.post(new Action(ActionType.NOTE_NOT_ADDED_V));
                break;

            case ActionType.REQUEST_DELIVERED_P:
                EventBusMask.post(new Action(ActionType.REQUEST_DELIVERED_V));
                break;

            case ActionType.REQUEST_NOT_DELIVERED_P:
                EventBusMask.post(new Action(ActionType.REQUEST_NOT_DELIVERED_V));
                break;

            case ActionType.NOTE_ADDED_TO_REQUEST_P:
                EventBusMask.post(new Action(ActionType.NOTE_ADDED_TO_REQUEST_V));
                break;

            case ActionType.NOTE_NOT_ADDED_TO_REQUEST_P:
                EventBusMask.post(new Action(ActionType.NOTE_NOT_ADDED_TO_REQUEST_V));
                break;

            case ActionType.SELLER_ROUTE_FINALIZE_P:
                EventBusMask.post(new Action(ActionType.SELLER_ROUTE_FINALIZE_V, null, action.isError()));
                break;
        }
    }

    /**
     * Ejecuta metodo que cambia el estado de la actividad seleccionada
     * a FINALIZADA
     * @param idActivity identificador de la actividad a finalizar
     */
    public void finalizeActivity(int idActivity) {
        mFinalizeActivity.execute(idActivity);
    }

    /**
     * Añade observación a la actividad seleccionada
     * @param idActivity identificador de la actividad seleccionada
     * @param note observación a añadir a la actividad
     */
    public void addNote(int idActivity, String note) {
        mAddNote.execute(idActivity, note);
    }

    /**
     * Ejecuta metodo que cambia el estado del pedido seleccionado seleccionada
     * a ENTREGADO
     * @param idRequest identificador del pedido a entregar
     */
    public void deliverRequest(int idRequest) {
        mDeliverRequest.execute(idRequest);
    }

    /**
     * Añade observación al pedido seleccionado
     * @param idRequest identificador del pedido seleccionado
     * @param note observación a añadir al pedido
     */
    public void addNoteToRequest(int idRequest, String note) {
        mAddNoteToRequest.execute(idRequest, note);
    }

    /**
     * Ejecuta metodo que finaliza la ruta actualmente con estado INICIADA
     */
    public void finalizeSellerRoute(int idSellerRoute) {
        mFinzalizeSellerRoute.execute(idSellerRoute);
    }
}
