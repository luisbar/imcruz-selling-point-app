package com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.route.AddUnassignedRoute;
import com.imcruzparts.pdv.domain.route.RemoveAssignedRoute;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;

/**
 * Clase que valida la vista {@link SellingPointRouteView},
 * y dispara el escuchador {@link SellingPointRouteView#onMessageEvent(Action)}
 */
public class SellingPointRoutePresenter {

    private UseCase mAddUnassignedRoute;
    private UseCase mRemoveAssignedRoute;

    public SellingPointRoutePresenter() {
        mAddUnassignedRoute = new AddUnassignedRoute();
        mRemoveAssignedRoute = new RemoveAssignedRoute();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.ROUTE_ADDED_P:
                if (action.isError())
                    EventBusMask.post(new Action(
                            ActionType.ROUTE_ADDED_V,
                            action.getPayload(),
                            action.isError()
                    ));
                else {
                    String json = new Gson().toJson(action.getPayload());
                    Type type = new TypeToken<SellingPointRouteModel>() {}.getType();
                    SellingPointRouteModel routeAdded = new Gson().fromJson(json, type);

                    EventBusMask.post(new Action(
                            ActionType.ROUTE_ADDED_V,
                            routeAdded,
                            action.isError()
                    ));
                }
                break;

            case ActionType.ROUTE_REMOVED_P:
                EventBusMask.post(new Action(
                        ActionType.ROUTE_REMOVED_V,
                        action.getPayload(),
                        action.isError()
                ));
                break;
        }
    }

    /**
     * Ejecuta metodo que elimina una rutaPdv de una rutaVenta
     * @param routeAssigned objeto rutaPdv a eliminar de rutaVenta
     */
    public void removeRoute(SellingPointRouteModel routeAssigned) {
        mRemoveAssignedRoute.execute(routeAssigned);
    }

    /**
     * Ejecuta metodo que agrega una rutaPdv a una rutaVenta
     * @param routeUnassigned objeto rutaPdv a agregar a rutaVenta
     */
    public void addRoute(SellingPointRouteModel routeUnassigned) {
        mAddUnassignedRoute.execute(routeUnassigned);
    }
}
