package com.imcruzparts.pdv.presentation.visit.util;

import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;
import com.imcruzparts.pdv.presentation.visit.VisitModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Mapea objeto List<{@link SellerRouteModelLocal}> a List<{@link VisitModel}>
 */
public class SrmlToVm implements Mapper<List<SellerRouteModelLocal>, List<VisitModel>> {

    private List<VisitModel.Business> getBusinesses(RealmList<PdvModelLocal> pdVs) {
        List<VisitModel.Business> lstBusiness = new ArrayList();

        for (PdvModelLocal pdv : pdVs) {
            new VisitModel.Business(
                    pdv.getIdPDV(),
                    pdv.getNombre(),
                    pdv.getDepartamento(),
                    pdv.getDireccion(),
                    pdv.getLatitud(),
                    pdv.getLongitud(),
                    pdv.getEstado(),
                    pdv.getFechaAlta(),
                    pdv.getFechaBaja()
            );
        }


        return lstBusiness;
    }

    @Override
    public List<VisitModel> map(List<SellerRouteModelLocal> sellerRouteModelLocals) {
        List<VisitModel> lstVisits = new ArrayList();

        for (SellerRouteModelLocal sellerRouteModelLocal : sellerRouteModelLocals) {
            lstVisits.add(new VisitModel(
                    sellerRouteModelLocal.getIdRutaVendedor(),
                    sellerRouteModelLocal.getDistribuidor(),
                    sellerRouteModelLocal.getSupervisor(),
                    sellerRouteModelLocal.getDescripcion(),
                    sellerRouteModelLocal.getFechaProgramacion(),
                    sellerRouteModelLocal.getEstado(),
                    sellerRouteModelLocal.getFechaAlta(),
                    sellerRouteModelLocal.getFechaBaja(),
                    sellerRouteModelLocal.getRutaVenta().getIdRuta(),
                    sellerRouteModelLocal.getRutaVenta().getNombre(),
                    sellerRouteModelLocal.getRutaVenta().getDescripcion(),
                    sellerRouteModelLocal.getRutaVenta().getDepartamento(),
                    sellerRouteModelLocal.getRutaVenta().getZona(),
                    sellerRouteModelLocal.getRutaVenta().getFechaAlta(),
                    sellerRouteModelLocal.getRutaVenta().getFechaBaja(),
                    sellerRouteModelLocal.getVendedor().getIdEmpleado(),
                    sellerRouteModelLocal.getVendedor().getApellido(),
                    sellerRouteModelLocal.getVendedor().getDireccion(),
                    sellerRouteModelLocal.getVendedor().getEmail(),
                    sellerRouteModelLocal.getVendedor().getFechaAlta(),
                    sellerRouteModelLocal.getVendedor().getFechaBaja(),
                    sellerRouteModelLocal.getVendedor().getNombre(),
                    sellerRouteModelLocal.getVendedor().getRol(),
                    sellerRouteModelLocal.getVendedor().getTelefono(),
                    getBusinesses(sellerRouteModelLocal.getPDVs()))
            );
        }

        return lstVisits;
    }
}
