package com.imcruzparts.pdv.presentation.upload;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class UploadView extends Fragment {

    @BindString(R.string.txt_111)
    String txt111;
    @BindString(R.string.txt_69)
    String txt69;
    @BindString(R.string.txt_8)
    String txt8;
    @BindString(R.string.txt_73)
    String txt73;
    @BindString(R.string.txt_120)
    String txt120;
    @BindString(R.string.txt_121)
    String txt121;

    @BindDrawable(R.mipmap.ic_menu)
    Drawable menu;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Unbinder unbinder;
    @BindView(R.id.check_upload_clients)
    CheckBox checkUploadClients;
    @BindView(R.id.check_upload_activities)
    CheckBox checkUploadProducts;
    @BindView(R.id.layout_progress)
    LinearLayout layoutProgress;

    private MainActivity mMainActivity;
    private UploadPresenter mUploadPresenter;

    private boolean showMenu = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upload, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(true);
        mUploadPresenter = new UploadPresenter(mMainActivity);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt111);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mUploadPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mUploadPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_upload, menu);
        menu.getItem(0).setVisible(
                showMenu
                ? true
                : false
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.openDrawer();
                break;

            case R.id.action_upload_clients_activities:
                layoutProgress.setVisibility(View.VISIBLE);
                showMenu(false);

                if (checkUploadClients.isChecked())
                    mUploadPresenter.sendClients();
                if (checkUploadProducts.isChecked())
                    mUploadPresenter.sendActivities();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.CLIENTS_UPLOADED_V:
                if (action.isError())
                    showError((Integer) action.getPayload());
                else
                    mMainActivity.showSnackBar(txt120);

                checkUploadClients.setChecked(false);
                if (!checkUploadProducts.isChecked() && !checkUploadClients.isChecked())
                    layoutProgress.setVisibility(View.GONE);
                break;

            case ActionType.ACTIVITIES_UPLOADED_V:
                if (action.isError())
                    showError((Integer) action.getPayload());
                else
                    mMainActivity.showSnackBar(txt121);

                checkUploadProducts.setChecked(false);
                if (!checkUploadProducts.isChecked() && !checkUploadClients.isChecked())
                    layoutProgress.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick({R.id.check_upload_clients, R.id.check_upload_activities})
    public void onViewClicked(View view) {
        showMenu(checkUploadClients.isChecked() || checkUploadProducts.isChecked());
    }

    private void showMenu(boolean flag) {
        showMenu = flag;
        mMainActivity.invalidateOptionsMenu();
    }

    private void showError(int idError) {
        String error = idError == R.string.txt_8
                ? txt8
                : idError == R.string.txt_69
                ? txt69
                : idError == R.string.txt_73
                ? txt73
                : null;

        mMainActivity.showSnackBar(error);
    }
}
