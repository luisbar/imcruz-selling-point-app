package com.imcruzparts.pdv.presentation;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.main.SignOut;

import org.greenrobot.eventbus.Subscribe;

/**
 * Clase que valida la vista {@link MainActivity},
 * y dispara el escuchador {@link MainActivity#onMessageEvent(Action)}
 */
public class MainPresenter {

    private UseCase mSignOut;

    public MainPresenter() {
        this.mSignOut = new SignOut();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.SIGN_OUT_SUCCESS_P:
                EventBusMask.post(new Action(ActionType.SIGN_OUT_SUCCESS_V));
                break;

            case ActionType.SIGN_OUT_ERROR_P:
                EventBusMask.post(new Action(ActionType.SIGN_OUT_ERROR_V));
                break;
        }
    }

    /**
     * Metodo que ejecuta el cierre de sesión
     */
    public void signOut() {
        mSignOut.execute();
    }
}
