package com.imcruzparts.pdv.presentation.visit.presentation.activity.util;

import android.support.design.internal.SnackbarContentLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityView;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestMadeAdapter extends RecyclerView.Adapter<RequestMadeAdapter.RequestMadeViewHolder> {

    private List<com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestModel> lstRequest;
    private OnRequestMadePressed mOnRequestMadePressed;
    private Fragment mFragment;

    private CheckBox mCheckRequestMade;
    private int positionDeliveredRequest;

    public RequestMadeAdapter(List<RequestModel> lstRequest, Fragment fragment) {
        this.lstRequest = lstRequest;
        this.mOnRequestMadePressed = (OnRequestMadePressed) fragment;
        this.mFragment = fragment;
    }

    @Override
    public RequestMadeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_request_made, parent, false);


        return new RequestMadeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RequestMadeViewHolder holder, int position) {
        try {
            holder.setData(lstRequest.get(position), position);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lstRequest.size();
    }

    /**
     * Añade un nuevo pedido hecho al recyclcer view
     * @param requestAdded
     */
    public void addRequestMade(RequestModel requestAdded) {
        lstRequest.add(requestAdded);
        notifyDataSetChanged();
    }

    public class RequestMadeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.check_request_made)
        CheckBox checkRequestMade;
        @BindView(R.id.text_request_made)
        TextView textRequestMade;
        @BindView(R.id.icon_request_made)
        ImageView iconOptions;

        @BindString(R.string.txt_99)
        String txt99;
        @BindString(R.string.txt_100)
        String txt100;
        @BindString(R.string.txt_101)
        String txt101;

        public RequestMadeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(final RequestModel request, final int position) throws ParseException {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            Date date = dateFormat.parse(request.getDate());

            dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            textRequestMade.setText("PEDIDO REALIZADO EL: " + dateFormat.format(date));
            checkRequestMade.setChecked(request.getState().equals("ENTREGADO") ? true : false);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnRequestMadePressed.requestMadePressed(request);
                }
            });
            // Crea el popup cuando se presiona el dots menu
            iconOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(mFragment.getActivity(), iconOptions);
                    popup.inflate(R.menu.menu_item_activity);

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_activity_option:
                                    showSnackbar(itemView, request);
                                    break;
                            }
                            return false;
                        }
                    });

                    popup.show();
                }
            });
            // Verifica si ya se hizo check al checkbox
            checkRequestMade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkRequestMade.isChecked()) {
                        checkRequestMade.setChecked(false);
                        showSnackbar(itemView, checkRequestMade, txt100, txt101, request, position);
                    } else {
                        showSnackbar(itemView, txt99);
                        checkRequestMade.setChecked(true);
                    }
                }
            });
        }
    }

    public CheckBox getmCheckRequestMade() {
        return mCheckRequestMade;
    }

    public int getPositionDeliveredRequest() {
        return positionDeliveredRequest;
    }

    public List<RequestModel> getLstRequest() {
        return lstRequest;
    }

    /**
     * Muestra un snackbar diciendo que ya se entrego el pedido seleccionado
     * @param itemView view para mostrar el snackbar
     * @param txt99 texto a mostrar en el snackbar
     */
    private void showSnackbar(View itemView, String txt99) {
        Snackbar.make(itemView, txt99, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Muestra un snackbar para establecer el pedido como entregado
     * @param itemView view para mostrar el snackbar
     * @param checkRequestMade checkbox a actualizar
     * @param txt100 texto a mostrar
     * @param txt101 texto de la acción
     */
    private void showSnackbar(View itemView, final CheckBox checkRequestMade, String txt100,
                              String txt101, final RequestModel request, int position) {

        this.mCheckRequestMade = checkRequestMade;
        this.positionDeliveredRequest = position;

        Snackbar.make(itemView, txt100, Snackbar.LENGTH_INDEFINITE)
                .setAction(txt101, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnRequestMadePressed.checkBoxPressed(request);
                    }
                })
                .show();
    }

    /**
     * Muestra un snackbar para agregar una observación al pedido
     * seleccionada
     * @param itemview view para mostrar el snackbar
     */
    private void showSnackbar(final View itemview, final RequestModel request) {
        Snackbar snackbar = Snackbar.make(itemview, null, Snackbar.LENGTH_INDEFINITE);

        Snackbar.SnackbarLayout snackbarView = (Snackbar.SnackbarLayout) snackbar.getView();
        SnackbarContentLayout content = (SnackbarContentLayout)snackbarView.getChildAt(0);

        final View view = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.layout_activity_snackbar, null);

        content.addView(view, 0);
        snackbar.show();

        Button accept = (Button) view.findViewById(R.id.btn_accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText note = (EditText) view.findViewById(R.id.input_note);
                mOnRequestMadePressed.noteAdded(request, note.getText().toString());
            }
        });
    }

    /**
     * Interface para disparar un evento cuando un item del recycler es
     * presionado, el evento es disparado {@link ActivityView}
     */
    public interface OnRequestMadePressed {

        void requestMadePressed(RequestModel request);
        void checkBoxPressed(RequestModel request);
        void noteAdded(RequestModel request, String note);
    }
}
