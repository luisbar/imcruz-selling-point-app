package com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.request.AddRequest;
import com.imcruzparts.pdv.presentation.product.ProductModel;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.util.RmlToRm;

import org.greenrobot.eventbus.Subscribe;

import java.util.Arrays;
import java.util.List;

public class RequestPresenter {

    private UseCase mAddRequest;

    public RequestPresenter() {
        this.mAddRequest = new AddRequest();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.REQUEST_ADDED_P:
                EventBusMask.post(new Action(ActionType.REQUEST_ADDED_V,
                        new RmlToRm().map((RequestModelLocal) action.getPayload())));
                break;

            case ActionType.REQUEST_NOT_ADDED_P:
                EventBusMask.post(new Action(ActionType.REQUEST_NOT_ADDED_V));
                break;
        }
    }

    /**
     * Calcula el total del pedido cada que se agrega o quita un item
     * @param lstProducts lista de productos con su precio y cantidad
     * @return lista que contiene el subtotal, descuento y total
     */
    public List calculateTotal(List<ProductModel> lstProducts) {
        Double subTotal = 0.0, discount = 0.0 , total;

        for (ProductModel productModel : lstProducts) {
            subTotal = subTotal + productModel.getTotal();
            discount = discount + (productModel.getDiscount() * productModel.getQuantity());
        }

        total = subTotal - discount;

        return Arrays.asList(subTotal, discount, total);
    }

    /**
     * Ejecuta metodo que agrega un pedido a la base de datos local
     * @param idSellingPoint id de pdv
     * @param idActivity id de actividad
     * @param subTotal subtotal del pedido
     * @param discount total descuento del pedido
     * @param totalAmount total del pedido
     * @param productsId lista de ids de producto del pedido
     * @param quantityByProducts lista de cantidad por prducto
     */
    public void addRequest(int idSellingPoint, int idActivity, double subTotal,
                           double discount, double totalAmount, List<Integer> productsId,
                           List<Integer> quantityByProducts) {

        mAddRequest.execute(AddRequest.Params.paramsForAddRequest(
                idSellingPoint,
                idActivity,
                subTotal,
                discount,
                totalAmount,
                productsId,
                quantityByProducts
        ));
    }
}
