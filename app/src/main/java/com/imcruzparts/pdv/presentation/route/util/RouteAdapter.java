package com.imcruzparts.pdv.presentation.route.util;

import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.route.RouteModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.RouteViewHolder> {

    private List<RouteModel> lstRoutes;
    private OnRoutePressed mOnRoutePressed;
    private Fragment mFragment;

    private int itemPositionSelected;

    public RouteAdapter(List lstRoutes, Fragment fragment, View emptyView) {
        this.lstRoutes = lstRoutes;
        this.mOnRoutePressed = (OnRoutePressed) fragment;
        this.mFragment = fragment;
        emptyView.setVisibility(lstRoutes.isEmpty() ? View.VISIBLE : View.GONE);
    }

    @Override
    public RouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_route, parent, false);

        return new RouteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RouteViewHolder holder, int position) {
        RouteModel routeModel = lstRoutes.get(position);
        holder.setData(routeModel.getNombre(), routeModel.getIdRuta(), position);
    }

    @Override
    public int getItemCount() {
        return lstRoutes.size();
    }

    public class RouteViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_name)
        TextView textName;
        @BindView(R.id.icon_dots)
        ImageView iconDots;

        public RouteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(String name, final int idRoute, final int position) {
            textName.setText(name);
            iconDots.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupMenu popup = new PopupMenu(mFragment.getActivity(), iconDots);
                    popup.inflate(R.menu.menu_item_route);

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_route_setting:
                                    mOnRoutePressed.setting(idRoute);
                                    break;

                                case R.id.action_route_assign:
                                    mOnRoutePressed.assign(idRoute);
                                    itemPositionSelected = position;
                                    break;
                            }
                            return false;
                        }
                    });

                    popup.show();
                }
            });
        }
    }

    public int getItemPositionSelected() {
        return itemPositionSelected;
    }

    /**
     * Interfaz que dispara evento en {@link com.imcruzparts.pdv.presentation.route.RouteView}
     * cuado un item del menu del recycler es presionado
     */
    public interface OnRoutePressed {

        void setting(int idRoute);
        void assign(int idRoute);
    }
}
