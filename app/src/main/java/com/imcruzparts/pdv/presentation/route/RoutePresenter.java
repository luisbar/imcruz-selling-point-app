package com.imcruzparts.pdv.presentation.route;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.DateDeserializer;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.common.NetDateTimeAdapter;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.route.GetRoutes;
import com.imcruzparts.pdv.domain.route.GetSellersAndRoutesProgrammed;
import com.imcruzparts.pdv.domain.route.GetSellingPointRoutes;
import com.imcruzparts.pdv.presentation.route.presentation.schedule.ScheduleModel;
import com.imcruzparts.pdv.presentation.route.presentation.schedule.util.SellerModel;
import com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute.SellingPointRouteModel;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Clase que valida la vista {@link RouteView},
 * y dispara el escuchador {@link RouteView#onMessageEvent(Action)}
 */
public class RoutePresenter {

    private UseCase mGetRoutes;
    private UseCase mGetSellingPointRoutes;
    private UseCase mGetSellersAndRoutesProgrammed;

    public RoutePresenter() {
        this.mGetRoutes = new GetRoutes();
        this.mGetSellingPointRoutes = new GetSellingPointRoutes();
        this.mGetSellersAndRoutesProgrammed = new GetSellersAndRoutesProgrammed();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.ROUTES_GOTTEN_P:
                if (action.isError())
                    EventBusMask.post(new Action(ActionType.ROUTES_GOTTEN_V, action.getPayload(), action.isError()));
                else {
                    Type type = new TypeToken<List<RouteModel>>() {}.getType();
                    String json = new Gson().toJson(action.getPayload());
                    List<RouteModel> lstRoutes = new Gson().fromJson(json, type);

                    EventBusMask.post(new Action(ActionType.ROUTES_GOTTEN_V,
                            lstRoutes, false));
                }
                break;

            case ActionType.SELLING_POINT_ROUTES_GOTTEN_P:
                if (action.isError())
                    EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_V, action.getPayload(), action.isError()));
                else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<SellingPointRouteModel>>() {}.getType();

                    Object assignedRoutes = ((List) action.getPayload()).get(0);
                    Object unassignedRoutes = ((List) action.getPayload()).get(1);

                    assignedRoutes = gson.toJson(assignedRoutes);
                    unassignedRoutes = gson.toJson(unassignedRoutes);

                    List<SellingPointRouteModel> lstRoutesAssigned = new Gson().fromJson(assignedRoutes.toString(), type);
                    List<SellingPointRouteModel> lstRoutesUnassigned = new Gson().fromJson(unassignedRoutes.toString(), type);

                    EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_V,
                            Arrays.asList(
                                    lstRoutesAssigned,
                                    lstRoutesUnassigned
                            )
                            , false));
                }
                break;

            case ActionType.ROUTES_PROGRAMMED_P:
                if (action.isError())
                    EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_V, action.getPayload(), action.isError()));
                else {
                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new NetDateTimeAdapter()).create();
                    Type sellerType = new TypeToken<List<SellerModel>>() {}.getType();
                    Type scheduleType = new TypeToken<List<ScheduleModel>>() {}.getType();

                    Object sellers = ((List) action.getPayload()).get(0);
                    Object schedule = ((List) action.getPayload()).get(1);

                    sellers = gson.toJson(sellers);
                    schedule = gson.toJson(schedule);

                    List<SellerModel> lstSeller = gson.fromJson(sellers.toString(), sellerType);
                    List<ScheduleModel> lstSchedule = gson.fromJson(schedule.toString(), scheduleType);

                    EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_V,
                            Arrays.asList(
                                    lstSeller,
                                    lstSchedule
                            )
                            , false));
                }
                break;
        }
    }

    /**
     * Ejecuta metodos que trae las rutas desde la nube
     */
    public void getRoutes() {
        mGetRoutes.execute();
    }

    /**
     * Ejecuta metodo que trae las rutas pdv desde la nube
     */
    public void getSellingPointRoutes(int idRoute) {
        mGetSellingPointRoutes.execute(idRoute);
    }

    /**
     * Ejecuta metodo que trae los vendedores y las rutas programadas
     * desde la nube
     */
    public void getSellersAndRoutesProgrammed(int idRoute) {
        mGetSellersAndRoutesProgrammed.execute(idRoute);
    }
}