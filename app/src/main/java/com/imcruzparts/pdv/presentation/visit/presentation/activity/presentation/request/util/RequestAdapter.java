package com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.util;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.product.ProductModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RequestHeaderViewHolder> implements Serializable {


    private List<ProductModel> lstProducts;

    private final int HEADER = 0;
    private final int BODY = 1;

    private OnQuantityChanged mOnQuantityChanged;
    private boolean newRequest;

    public RequestAdapter(Fragment fragment, boolean newRequest) {
        this.lstProducts = new ArrayList();
        this.mOnQuantityChanged = (OnQuantityChanged) fragment;
        this.newRequest = newRequest;
    }

    @Override
    public int getItemViewType(int position) {
        return position == HEADER ? HEADER : BODY;
    }

    @Override
    public RequestHeaderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RequestHeaderViewHolder requestHeaderViewHolder = null;

        switch (viewType) {

            case HEADER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_request_header, parent, false);
                TextView whiteSpace = (TextView) view.findViewById(R.id.white_space);
                whiteSpace.setVisibility(newRequest ? View.VISIBLE : View.GONE);
                requestHeaderViewHolder = new RequestHeaderViewHolder(view);
                break;

            case BODY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_request, parent, false);
                requestHeaderViewHolder = new RequestBodyViewHolder(view);
                break;
        }


        return requestHeaderViewHolder;
    }

    @Override
    public void onBindViewHolder(RequestHeaderViewHolder holder, int position) {
        if (position > 0) {
            ProductModel productModel = lstProducts.get(position - 1);
            holder.setData(productModel.getDescription(),
                    productModel.getPrice(),
                    productModel.getDiscount(),
                    productModel.getTotal(),
                    productModel.getQuantity(),
                    position - 1);
        } else
            holder.setData(null, null, null, null, null, -1);

    }

    @Override
    public int getItemCount() {
        return lstProducts.size() + 1;
    }

    /**
     * Añade un producto a la lista
     * @param productModel objeto de tipo ProductModel
     */
    public void addProduct(ProductModel productModel) {
        if (!lstProducts.contains(productModel)) {
            // Seteamos cantidad y precio por default
            if (newRequest) {
                productModel.setQuantity(1);
                productModel.setTotal(productModel.getPrice());
            } else {
                productModel.setQuantity(productModel.getQuantity());
                productModel.setTotal(productModel.getTotal());
            }

            lstProducts.add(productModel);
        }
    }

    /**
     * Remueve un item de la lista
     * @param isCompuntingLayout bandera pa ver si el recycler esta siendo utilizado
     * @param position posicion del item a remover
     */
    private void removeProduct(boolean isCompuntingLayout, int position) {
        lstProducts.remove(position);

        if (!isCompuntingLayout)
            notifyDataSetChanged();
        else
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
    }

    public List<ProductModel> getLstProducts() {
        return lstProducts;
    }

    public class RequestHeaderViewHolder extends RecyclerView.ViewHolder {

        public RequestHeaderViewHolder(View itemView) {
            super(itemView);
        }

        public void setData(String name, Double price, Double discount, Double total, Integer quantity, int position) {

        }
    }

    public class RequestBodyViewHolder extends RequestHeaderViewHolder {

        @BindView(R.id.input_request_quantity)
        EditText inputRequestQuantity;
        @BindView(R.id.text_request_name)
        TextView textRequestName;
        @BindView(R.id.text_request_price)
        TextView textRequestPrice;
        @BindView(R.id.text_request_discount)
        TextView textRequestDiscount;
        @BindView(R.id.text_request_total)
        TextView textRequestTotal;
        @BindView(R.id.icon_request_delete)
        ImageView iconRequestDelete;

        private int position;

        public RequestBodyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(String name, Double price, Double discount, Double total, Integer quantity, int position ) {
            textRequestName.setText(name);
            textRequestPrice.setText(price + " Bs");
            textRequestDiscount.setText(discount + " Bs");
            textRequestTotal.setText(total + " Bs");
            iconRequestDelete.setVisibility(newRequest ? View.VISIBLE : View.GONE);
            // Primero se actualiza la posicion, antes de setearle valor al
            // inputRequestQuantity, caso contrario se dispara el escuchador
            // onTextChanged y genera un error
            this.position = position;
            inputRequestQuantity.setText(quantity.toString());
            inputRequestQuantity.setFocusableInTouchMode(newRequest);
            inputRequestQuantity.setLongClickable(newRequest);
            inputRequestQuantity.requestFocus();
        }

        @OnTextChanged(R.id.input_request_quantity)
        public void onTextChanged() {
            Double price = Double.valueOf(textRequestPrice.getText().toString().split("\\s+")[0]);
            Integer quantity = Integer.valueOf(inputRequestQuantity.getText().toString().isEmpty()
                    ? "0"
                    : inputRequestQuantity.getText().toString());
            Double total = price * quantity;

            textRequestTotal.setText(total + " Bs");

            lstProducts.get(position).setQuantity(quantity);
            lstProducts.get(position).setTotal(total);

            mOnQuantityChanged.quantityChanged();
        }

        @OnClick(R.id.icon_request_delete)
        public void onClick() {
            removeProduct(true, position);
            mOnQuantityChanged.quantityChanged();
        }
    }

    /**
     * Interfaz que dispara evento en {@link com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestView}
     * cuando la cantidad es modificada
     */
    public interface OnQuantityChanged {

        void quantityChanged();
    }
}
