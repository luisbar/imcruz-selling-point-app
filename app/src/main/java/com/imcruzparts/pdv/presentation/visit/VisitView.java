package com.imcruzparts.pdv.presentation.visit;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityView;
import com.imcruzparts.pdv.presentation.visit.presentation.visitdetail.VisitDetailView;
import com.imcruzparts.pdv.presentation.visit.util.VisitAdapter;

import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class VisitView extends Fragment implements VisitAdapter.OnItemPressed {

    @BindDrawable(R.mipmap.ic_menu)
    Drawable menu;

    @BindString(R.string.txt_11)
    String txt11;
    @BindString(R.string.txt_8)
    String txt8;
    @BindString(R.string.txt_73)
    String txt73;
    @BindString(R.string.txt_69)
    String txt69;
    @BindString(R.string.txt_75)
    String txt75;
    @BindString(R.string.txt_76)
    String txt76;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_visit_routes)
    RecyclerView rvVisitRoutes;
    Unbinder unbinder;
    @BindView(R.id.empty_image)
    ImageView emptyImage;
    @BindView(R.id.empty_title)
    TextView emptyTitle;
    @BindView(R.id.empty_description)
    TextView emptyDescription;
    @BindView(R.id.empty_view)
    FrameLayout emptyView;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @BindView(R.id.layout_progress)
    LinearLayout layoutProgress;

    private MainActivity mMainActivity;
    private VisitPresenter mVisitPresenter;
    private VisitAdapter mVisitAdapter;
    private Bundle args;

    private boolean showMenu = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visit, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(true);
        mVisitPresenter = new VisitPresenter();
        mVisitAdapter = new VisitAdapter(new ArrayList(), this);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEmptyView();
        rvVisitRoutes.setAdapter(mVisitAdapter);
        rvVisitRoutes.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));
        mVisitAdapter.checkIfEmpty();

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt11);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mVisitPresenter.onStart();
        mVisitPresenter.getRoutes();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mVisitPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_visit, menu);
        menu.getItem(0).setVisible(showMenu ? true : false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                mMainActivity.openDrawer();
                break;

            case R.id.action_download:
                showMenu = false;
                mMainActivity.invalidateOptionsMenu();

                mVisitPresenter.getRoutesFromloud();
                layoutProgress.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                rvVisitRoutes.setVisibility(View.GONE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.PENDING_ROUTES_OBTAINED_V:

                mVisitAdapter.setVisits((List<VisitModel>) action.getPayload());
                mVisitAdapter.checkIfEmpty();
                layoutProgress.setVisibility(View.GONE);
                rvVisitRoutes.setVisibility(View.VISIBLE);
                break;

            case ActionType.PENDING_ROUTES_NOT_OBTAINED_V:

                String error = (int) action.getPayload() == R.string.txt_8
                        ? txt8
                        : (int) action.getPayload() == R.string.txt_73
                        ? txt73
                        : (int) action.getPayload() == R.string.txt_69
                        ? txt69
                        : null;
                mMainActivity.showSnackBar(error);
                mVisitAdapter.checkIfEmpty();
                layoutProgress.setVisibility(View.GONE);
                rvVisitRoutes.setVisibility(View.VISIBLE);
                break;

            case ActionType.PENDING_REQUEST_OBTAINED_V:
                args = new Bundle();
                
                args.putSerializable("Requests", (Serializable) action.getPayload());
                break;

            case ActionType.INITIALIZED_ROUTES_OBTAINED_V:
                if (args == null) {
                    args = new Bundle();
                    args.putSerializable("Requests", new ArrayList());
                }

                Fragment mapView = new ActivityView();

                args.putSerializable("Activities", (Serializable) action.getPayload());
                mapView.setArguments(args);

                mMainActivity.loadFragment(R.id.container, mapView, "ActivityView");
                break;
        }

        // Muestra el menu
        showMenu = true;
        mMainActivity.invalidateOptionsMenu();
    }

    /**
     * Configura view para mostrar cuando no haya rutas disponibles
     */
    private void setEmptyView() {
        emptyTitle.setText(txt75);
        emptyDescription.setText(txt76);
        emptyImage.setImageDrawable(mMainActivity.getDrawable(R.drawable.ic_menu_agenda));
        mVisitAdapter.setEmptyView(emptyView);
    }

    /**
     * Escuchador disparado por VisiAdapter, cuando el usuario presiona un item
     * del recycler view
     * @param visitModel objeto de tipo {@link VisitModel}
     */
    @Override
    public void itemPressed(VisitModel visitModel) {
        Fragment detail = new VisitDetailView();
        Bundle args = new Bundle();

        args.putSerializable("VisitModel", visitModel);
        detail.setArguments(args);

        mMainActivity.loadFragmentAndAddedToStack(R.id.container, detail, "VisitDetailView");
    }
}
