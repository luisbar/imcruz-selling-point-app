package com.imcruzparts.pdv.presentation.visit.presentation.activity.util;

import android.support.design.internal.SnackbarContentLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityModel;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityView;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityViewHolder> {

    private List<ActivityModel> lstActivities;
    private OnItemPressed mOnItemPressed;
    private Fragment mFragment;

    private CheckBox mCheckActivity;
    private int positionFinalizedActivity;

    public ActivityAdapter(List<ActivityModel> lstActivities, Fragment fragment) {
        this.lstActivities = lstActivities;
        this.mOnItemPressed = (OnItemPressed) fragment;
        this.mFragment = fragment;
    }

    @Override
    public ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity, parent, false);


        return new ActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ActivityViewHolder holder, int position) {
        holder.setData(lstActivities.get(position), position);
    }

    @Override
    public int getItemCount() {
        return lstActivities.size();
    }

    public class ActivityViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.check_activity)
        CheckBox checkActivity;
        @BindView(R.id.text_activity)
        TextView textActivity;
        @BindView(R.id.icon_options)
        ImageView iconOptions;

        @BindString(R.string.txt_88)
        String txt88;
        @BindString(R.string.txt_89)
        String txt89;
        @BindString(R.string.txt_90)
        String txt90;
        @BindString(R.string.txt_91)
        String txt91;

        public ActivityViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(final ActivityModel activity, final int position) {
            textActivity.setText(activity.getDescription());
            checkActivity.setChecked(activity.getState().equals("FINALIZADA") ? true : false);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemPressed.itemPressed(activity);
                }
            });
            // Crea el popup cuando se presiona el dots menu
            iconOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(mFragment.getActivity(), iconOptions);
                    popup.inflate(R.menu.menu_item_activity);

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_activity_option:
                                    showSnackbar(itemView, activity);
                                    break;
                            }
                            return false;
                        }
                    });

                    popup.show();
                }
            });
            // Verifica si ya se hizo check al checkbox
            checkActivity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkActivity.isChecked()) {
                        checkActivity.setChecked(false);
                        showSnackbar(itemView, checkActivity, txt89, txt90, activity, position);
                    } else {
                        showSnackbar(itemView, txt88);
                        checkActivity.setChecked(true);
                    }
                }
            });
        }
    }

    public CheckBox getmCheckActivity() {
        return mCheckActivity;
    }

    public int getPositionFinalizedActivity() {
        return positionFinalizedActivity;
    }

    public List<ActivityModel> getLstActivities() {
        return lstActivities;
    }

    /**
     * Muestra un snackbar diciendo que ya se finalizo la actividad seleccionada
     * @param itemView view para mostrar el snackbar
     * @param txt88 texto a mostrar en el snackbar
     */
    private void showSnackbar(View itemView, String txt88) {
        Snackbar.make(itemView, txt88, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Muestra un snackbar para finalizar la actividad seleccionad
     * @param itemView view para mostrar el snackbar
     * @param checkActivity checkbox a actualizar
     * @param txt89 texto a mostrar
     * @param txt90 texto de la acción
     */
    private void showSnackbar(View itemView, final CheckBox checkActivity, String txt89,
                              String txt90, final ActivityModel activity, int position) {

        this.mCheckActivity = checkActivity;
        this.positionFinalizedActivity = position;

        Snackbar.make(itemView, txt89, Snackbar.LENGTH_INDEFINITE)
                .setAction(txt90, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemPressed.checkBoxPressed(activity);
                    }
                })
                .show();
    }

    /**
     * Muestra un snackbar para agregar una observación a la actividad
     * seleccionada
     * @param itemview view para mostrar el snackbar
     */
    private void showSnackbar(final View itemview, final ActivityModel activity) {
        Snackbar snackbar = Snackbar.make(itemview, null, Snackbar.LENGTH_INDEFINITE);

        Snackbar.SnackbarLayout snackbarView = (Snackbar.SnackbarLayout) snackbar.getView();
        SnackbarContentLayout content = (SnackbarContentLayout)snackbarView.getChildAt(0);

        final View view = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.layout_activity_snackbar, null);

        content.addView(view, 0);
        snackbar.show();

        Button accept = (Button) view.findViewById(R.id.btn_accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText note = (EditText) view.findViewById(R.id.input_note);
                mOnItemPressed.noteAdded(activity, note.getText().toString());
            }
        });
    }

    /**
     * Interface para disparar un evento cuando un item del recycler es
     * presionada, el evento es disparado {@link ActivityView}
     */
    public interface OnItemPressed {

        void itemPressed(ActivityModel activity);
        void checkBoxPressed(ActivityModel activity);
        void noteAdded(ActivityModel activity, String note);
    }
}
