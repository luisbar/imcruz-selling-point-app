package com.imcruzparts.pdv.presentation.route.presentation.schedule;

import java.io.Serializable;
import java.util.Date;

public class ScheduleModel implements Serializable {

    private String Descripcion;
    private String Distribuidor;
    private Integer IdDistribuidor;
    private String Estado;
    private String FechaAlta;
    private String FechaBaja;
    private Date FechaProgramacion;
    private Integer IdRutaVendedor;
    private Object PDVs;
    private Route RutaVenta;
    private Object Supervisor;
    private Seller Vendedor;

    public ScheduleModel() {
    }

    public ScheduleModel(String descripcion, String distribuidor, Integer idDistribuidor, String estado, String fechaAlta, String fechaBaja, Date fechaProgramacion, Integer idRutaVendedor, Object PDVs, Route rutaVenta, Object supervisor, Seller vendedor) {
        Descripcion = descripcion;
        Distribuidor = distribuidor;
        IdDistribuidor = idDistribuidor;
        Estado = estado;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
        FechaProgramacion = fechaProgramacion;
        IdRutaVendedor = idRutaVendedor;
        this.PDVs = PDVs;
        RutaVenta = rutaVenta;
        Supervisor = supervisor;
        Vendedor = vendedor;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getDistribuidor() {
        return Distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        Distribuidor = distribuidor;
    }

    public Integer getIdDistribuidor() {
        return IdDistribuidor;
    }

    public void setIdDistribuidor(Integer idDistribuidor) {
        IdDistribuidor = idDistribuidor;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public Date getFechaProgramacion() {
        return FechaProgramacion;
    }

    public void setFechaProgramacion(Date fechaProgramacion) {
        FechaProgramacion = fechaProgramacion;
    }

    public Integer getIdRutaVendedor() {
        return IdRutaVendedor;
    }

    public void setIdRutaVendedor(Integer idRutaVendedor) {
        IdRutaVendedor = idRutaVendedor;
    }

    public Object getPDVs() {
        return PDVs;
    }

    public void setPDVs(Object PDVs) {
        this.PDVs = PDVs;
    }

    public Route getRutaVenta() {
        return RutaVenta;
    }

    public void setRutaVenta(Route rutaVenta) {
        RutaVenta = rutaVenta;
    }

    public Object getSupervisor() {
        return Supervisor;
    }

    public void setSupervisor(Object supervisor) {
        Supervisor = supervisor;
    }

    public Seller getVendedor() {
        return Vendedor;
    }

    public void setVendedor(Seller vendedor) {
        Vendedor = vendedor;
    }

    public static class Route implements Serializable {

        private String Departamento;
        private String Descripcion;
        private String FechaAlta;
        private String FechaBaja;
        private Integer IdRuta;
        private String Nombre;
        private String Zona;
        private Integer IdDistribuidor;
        private String Distribuidor;

        public Route(Integer idRuta) {
            IdRuta = idRuta;
        }

        public Route(String departamento, String descripcion, String fechaAlta, String fechaBaja, Integer idRuta, String nombre, String zona, Integer idDistribuidor, String distribuidor) {
            Departamento = departamento;
            Descripcion = descripcion;
            FechaAlta = fechaAlta;
            FechaBaja = fechaBaja;
            IdRuta = idRuta;
            Nombre = nombre;
            Zona = zona;
            IdDistribuidor = idDistribuidor;
            Distribuidor = distribuidor;
        }

        public String getDepartamento() {
            return Departamento;
        }

        public void setDepartamento(String departamento) {
            Departamento = departamento;
        }

        public String getDescripcion() {
            return Descripcion;
        }

        public void setDescripcion(String descripcion) {
            Descripcion = descripcion;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdRuta() {
            return IdRuta;
        }

        public void setIdRuta(Integer idRuta) {
            IdRuta = idRuta;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public String getZona() {
            return Zona;
        }

        public void setZona(String zona) {
            Zona = zona;
        }
    }

    public static class Seller implements Serializable {

        private String Apellido;
        private String Direccion;
        private Distributor Distribuidor;
        private String Email;
        private String FechaAlta;
        private String FechaBaja;
        private Integer IdEmpleado;
        private String Nombre;
        private Object Sesion;
        private String Telefono;

        public Seller(Integer idEmpleado) {
            IdEmpleado = idEmpleado;
        }

        public Seller(String apellido, String direccion, Distributor distribuidor, String email, String fechaAlta, String fechaBaja, Integer idEmpleado, String nombre, Object sesion, String telefono) {
            Apellido = apellido;
            Direccion = direccion;
            Distribuidor = distribuidor;
            Email = email;
            FechaAlta = fechaAlta;
            FechaBaja = fechaBaja;
            IdEmpleado = idEmpleado;
            Nombre = nombre;
            Sesion = sesion;
            Telefono = telefono;
        }

        public String getApellido() {
            return Apellido;
        }

        public void setApellido(String apellido) {
            Apellido = apellido;
        }

        public String getDireccion() {
            return Direccion;
        }

        public void setDireccion(String direccion) {
            Direccion = direccion;
        }

        public Distributor getDistribuidor() {
            return Distribuidor;
        }

        public void setDistribuidor(Distributor distribuidor) {
            Distribuidor = distribuidor;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdEmpleado() {
            return IdEmpleado;
        }

        public void setIdEmpleado(Integer idEmpleado) {
            IdEmpleado = idEmpleado;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public Object getSesion() {
            return Sesion;
        }

        public void setSesion(Object sesion) {
            Sesion = sesion;
        }

        public String getTelefono() {
            return Telefono;
        }

        public void setTelefono(String telefono) {
            Telefono = telefono;
        }
    }

    public static class Distributor implements Serializable {

        private String Direccion;
        private String Estado;
        private String FechaAlta;
        private String FechaBaja;
        private Integer IdDistribuidor;
        private String Nit;
        private String RazonSocial;
        private String Responsable;

        public Distributor(String direccion, String estado, String fechaAlta, String fechaBaja, Integer idDistribuidor, String nit, String razonSocial, String responsable) {
            Direccion = direccion;
            Estado = estado;
            FechaAlta = fechaAlta;
            FechaBaja = fechaBaja;
            IdDistribuidor = idDistribuidor;
            Nit = nit;
            RazonSocial = razonSocial;
            Responsable = responsable;
        }

        public String getDireccion() {
            return Direccion;
        }

        public void setDireccion(String direccion) {
            Direccion = direccion;
        }

        public String getEstado() {
            return Estado;
        }

        public void setEstado(String estado) {
            Estado = estado;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdDistribuidor() {
            return IdDistribuidor;
        }

        public void setIdDistribuidor(Integer idDistribuidor) {
            IdDistribuidor = idDistribuidor;
        }

        public String getNit() {
            return Nit;
        }

        public void setNit(String nit) {
            Nit = nit;
        }

        public String getRazonSocial() {
            return RazonSocial;
        }

        public void setRazonSocial(String razonSocial) {
            RazonSocial = razonSocial;
        }

        public String getResponsable() {
            return Responsable;
        }

        public void setResponsable(String responsable) {
            Responsable = responsable;
        }
    }
}
