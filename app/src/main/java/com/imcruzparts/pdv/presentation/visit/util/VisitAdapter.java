package com.imcruzparts.pdv.presentation.visit.util;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.visit.VisitModel;
import com.imcruzparts.pdv.presentation.visit.VisitView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.VisitViewHolder> {

    private List<VisitModel> lstVisits;
    private OnItemPressed mOnItemPressed;

    public VisitAdapter(List<VisitModel> lstVisits, Fragment fragment) {
        this.lstVisits = lstVisits;
        this.mOnItemPressed = (OnItemPressed) fragment;
    }

    @Override
    public VisitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_visit, parent, false);

        return new VisitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VisitViewHolder holder, int position) {
        holder.setData(lstVisits.get(position));
    }

    @Override
    public int getItemCount() {
        return lstVisits.size();
    }

    public void setVisits(List<VisitModel> lstVisits) {
        this.lstVisits = new ArrayList();
        this.lstVisits.addAll(lstVisits);
        notifyDataSetChanged();
    }

    public class VisitViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_visit_name_route)
        TextView textVisitNameRoute;
        @BindView(R.id.text_visit_description_route)
        TextView textVisitDescriptionRoute;
        @BindView(R.id.text_visit_state)
        TextView textVisitNameState;
        @BindView(R.id.text_visit_date)
        TextView textVisitNameDate;

        public VisitViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(final VisitModel visitModel) {

            textVisitNameRoute.setText(visitModel.getRoute().getName());
            textVisitDescriptionRoute.setText(visitModel.getRoute().getDescription());
            textVisitNameState.setText(visitModel.getState());

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            textVisitNameDate.setText(format.format(visitModel.getOrderDate()));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemPressed.itemPressed(visitModel);
                }
            });
        }
    }

    private View mEmptyView;
    /**
     * Set the empty view to be used so that
     * @param emptyView
     */
    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
    }

    /**
     * Check if we should show the empty view
     */
    public void checkIfEmpty() {
        if (mEmptyView != null) {
            int s = getItemCount();
            mEmptyView.setVisibility(getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Interface que es implementada por {@link VisitView}
     * , para que se dispare un evento cuando el usuario presiona un item del recycler view
     */
    public interface OnItemPressed {

        void itemPressed(VisitModel visitModel);
    }
}
