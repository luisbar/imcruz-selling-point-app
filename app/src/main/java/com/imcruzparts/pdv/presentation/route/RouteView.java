package com.imcruzparts.pdv.presentation.route;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.route.presentation.schedule.ScheduleModel;
import com.imcruzparts.pdv.presentation.route.presentation.schedule.ScheduleView;
import com.imcruzparts.pdv.presentation.route.presentation.schedule.util.SellerModel;
import com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute.SellingPointRouteModel;
import com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute.SellingPointRouteView;
import com.imcruzparts.pdv.presentation.route.util.CustomSnackbar;
import com.imcruzparts.pdv.presentation.route.util.RouteAdapter;

import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RouteView extends Fragment implements RouteAdapter.OnRoutePressed {

    @BindString(R.string.txt_113)
    String txt113;
    @BindString(R.string.txt_8)
    String txt8;
    @BindString(R.string.txt_73)
    String txt73;
    @BindString(R.string.txt_126)
    String txt126;
    @BindString(R.string.txt_127)
    String txt127;

    @BindDrawable(R.mipmap.ic_menu)
    Drawable menu;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_route)
    RecyclerView rvRoute;
    Unbinder unbinder;
    @BindView(R.id.layout_progress)
    LinearLayout layoutProgress;
    @BindView(R.id.empty_image)
    ImageView emptyImage;
    @BindView(R.id.empty_title)
    TextView emptyTitle;
    @BindView(R.id.empty_description)
    TextView emptyDescription;
    @BindView(R.id.empty_view)
    FrameLayout emptyView;

    private MainActivity mMainActivity;
    private RoutePresenter mRoutePresenter;
    private RouteAdapter mRouteAdapter;

    private List<RouteModel> lstRoutes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(true);
        mRoutePresenter = new RoutePresenter();
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt113);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(menu);

        emptyTitle.setText(txt126);
        emptyDescription.setText(txt127);
        emptyImage.setImageDrawable(mMainActivity.getDrawable(R.drawable.home_map_marker));
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mRoutePresenter.onStart();
        if (lstRoutes == null) {
            layoutProgress.setVisibility(View.VISIBLE);
            mRoutePresenter.getRoutes();
        } else {
            mRouteAdapter = new RouteAdapter(
                    lstRoutes,
                    this,
                    emptyView);
            rvRoute.setAdapter(mRouteAdapter);
            rvRoute.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mRoutePresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.openDrawer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Evento que es dispara por {@link RouteAdapter}, cuando
     * el item setting del menu del recycler es presionado
     */
    @Override
    public void setting(int idRoute) {
        layoutProgress.setVisibility(View.VISIBLE);
        mRoutePresenter.getSellingPointRoutes(idRoute);
    }

    /**
     * Evento que es dispara por {@link RouteAdapter}, cuando
     * el item assign del menu del recycler es presionado
     */
    @Override
    public void assign(int idRoute) {
        layoutProgress.setVisibility(View.VISIBLE);
        mRoutePresenter.getSellersAndRoutesProgrammed(idRoute);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.ROUTES_GOTTEN_V:
                layoutProgress.setVisibility(View.GONE);
                if (action.isError()) {
                    showError(action.getPayload());
                } else {
                    lstRoutes = (List<RouteModel>) action.getPayload();
                    mRouteAdapter = new RouteAdapter(
                            lstRoutes,
                            this,
                            emptyView);
                    rvRoute.setAdapter(mRouteAdapter);
                    rvRoute.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));
                }
                break;

            case ActionType.SELLING_POINT_ROUTES_GOTTEN_V:
                if (action.isError()) {
                    layoutProgress.setVisibility(View.GONE);
                    showError(action.getPayload());
                } else {
                    List payload = (List) action.getPayload();
                    List<SellingPointRouteModel> lstSellingPointRoutesAssigned = (List<SellingPointRouteModel>) payload.get(0);
                    List<SellingPointRouteModel> lstSellingPointRoutesUnassigned = (List<SellingPointRouteModel>) payload.get(1);

                    Bundle args = new Bundle();
                    Fragment sellingPointRouteView = new SellingPointRouteView();

                    args.putSerializable("sellingPointRoutesAssigned", (Serializable) lstSellingPointRoutesAssigned);
                    args.putSerializable("sellingPointRoutesUnassigned", (Serializable) lstSellingPointRoutesUnassigned);
                    sellingPointRouteView.setArguments(args);

                    mMainActivity.loadFragmentAndAddedToStack(R.id.container, sellingPointRouteView);
                }
                break;

            case ActionType.ROUTES_PROGRAMMED_V:
                if (action.isError()) {
                    layoutProgress.setVisibility(View.GONE);
                    showError(action.getPayload());
                } else {
                    List payload = (List) action.getPayload();
                    List<SellerModel> lstSeller = (List<SellerModel>) payload.get(0);
                    List<ScheduleModel> lstSchedule = (List<ScheduleModel>) payload.get(1);

                    Bundle args = new Bundle();
                    Fragment scheduleView = new ScheduleView();

                    args.putSerializable("sellers", (Serializable) lstSeller);
                    args.putSerializable("schedules", (Serializable) lstSchedule);
                    args.putInt("idRoute", lstRoutes.get(mRouteAdapter.getItemPositionSelected()).getIdRuta());
                    args.putString("route", lstRoutes.get(mRouteAdapter.getItemPositionSelected()).getNombre());
                    args.putInt("idDistributor", lstRoutes.get(mRouteAdapter.getItemPositionSelected()).getIdDistribuidor());
                    args.putString("distributor", lstRoutes.get(mRouteAdapter.getItemPositionSelected()).getDistribuidor());
                    args.putSerializable("routeObject", lstRoutes.get(mRouteAdapter.getItemPositionSelected()));
                    scheduleView.setArguments(args);

                    mMainActivity.loadFragmentAndAddedToStack(R.id.container, scheduleView);
                }
                break;
        }
    }

    private void showError(Object payload) {
        String error = (int) payload == R.string.txt_8
                ? txt8
                : (int) payload == R.string.txt_73
                ? txt73
                : null;

        mMainActivity.showSnackBar(error);
    }
}
