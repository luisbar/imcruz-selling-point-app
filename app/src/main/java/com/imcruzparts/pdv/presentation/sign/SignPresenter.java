package com.imcruzparts.pdv.presentation.sign;


import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.sign.model.SessionModelLocal;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.sign.GetUserLogged;
import com.imcruzparts.pdv.domain.sign.SignIn;
import com.imcruzparts.pdv.presentation.sign.util.SmlToSm;

import org.greenrobot.eventbus.Subscribe;

/**
 * Clase que valida la vista {@link SignView},
 * y dispara el escuchador {@link SignView#onMessageEvent(Action)}
 */
public class SignPresenter {

    private UseCase mSignIn;
    private UseCase mGetUserLogged;


    public SignPresenter() {
        this.mSignIn = new SignIn();
        this.mGetUserLogged = new GetUserLogged();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Verifica si hay una sesión activa
     */
    public void getUserLogged() {
        mGetUserLogged.execute();
    }
    /**
     * Metodo que ejecuta el inicio de sesión
     * @param username nombre de usuario u correo
     * @param password clave
     */
    public void signIn(String username, String password) {
        mSignIn.execute(SignIn.Params.forExecute(username, password));
    }

    /**
     * Valida el valor del inputSignPassword de {@link SignView}
     * @param password valor introducido en inputSignPassword de {@link SignView}
     */
    public void validatePassword(String password) {
        boolean payload = password.length() >= 3;

        EventBusMask
                .post(new Action(ActionType.VALIDATED_PASSWORD,
                        payload, false));
    }

    /**
     * Valida el valor del inputSignUsername de {@link SignView}
     * @param username valor introducido en inputSignUsername de {@link SignView}
     */
    public void validateUsername(String username) {
        boolean payload = username.length() >= 4;

        EventBusMask
                .post(new Action(ActionType.VALIDATED_USERNAME,
                        payload, false));
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.CHECK_USER_LOGGED:

                if (action.getPayload() != null)
                    EventBusMask.post(new Action(ActionType.THERE_IS_USER_LOGGED,
                            new SmlToSm().map((SessionModelLocal) action.getPayload())));
                break;

            case ActionType.SIGN_IN_SUCCESS_P:

                EventBusMask.post(new Action(ActionType.SIGN_IN_SUCCESS_V));
                break;

            case ActionType.SIGN_IN_ERROR_P:

                EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_V, action.getPayload()));
        }
    };
}
