package com.imcruzparts.pdv.presentation;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.download.DownloadView;
import com.imcruzparts.pdv.presentation.home.HomeView;
import com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation.BusinessInformationView;
import com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation.ClientInformationView;
import com.imcruzparts.pdv.presentation.product.ProductView;
import com.imcruzparts.pdv.presentation.route.RouteView;
import com.imcruzparts.pdv.presentation.sign.SignView;
import com.imcruzparts.pdv.presentation.tracking.TrackingService;
import com.imcruzparts.pdv.presentation.upload.UploadView;
import com.imcruzparts.pdv.presentation.visit.VisitView;

import org.greenrobot.eventbus.Subscribe;

import java.util.Iterator;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Renderiza {@link SignView}
 */
public class MainActivity extends BaseActivity implements View.OnClickListener, FragmentManager.OnBackStackChangedListener, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer)
    DrawerLayout drawer;
    @BindView(R.id.navigation)
    NavigationView navigation;

    @BindString(R.string.txt_70)
    String txt70;

    private Unbinder mUnbinder;
    private HomeView homeFragment;
    private ProductView productoFragment;
    private VisitView visitFragment;
    private MainPresenter mMainPresenter;

    private final Handler mDrawerActionHandler = new Handler();
    private static final long DRAWER_CLOSE_DELAY_MS = 350;
    private int mNavItemId;

    private RecyclerView.Adapter mRequestAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Esconde el status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUnbinder = ButterKnife.bind(this);
        mMainPresenter = new MainPresenter();
        Fragment signFragment = new SignView();
        loadFragment(R.id.container, signFragment);

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (!isMyServiceRunning(TrackingService.class)) {
            Intent intent = new Intent(this, TrackingService.class);
            startService(intent);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mMainPresenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mMainPresenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onClick(View v) {
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        int fragmentsQuantity = getSupportFragmentManager().getBackStackEntryCount();

        if (fragmentsQuantity == 0)
            super.onBackPressed();
        else
            getSupportFragmentManager().popBackStack();
    }

    /**
     * Habilita o desabilita el drawer
     * @param flag bandera para habilitar o desabilitar el drawer
     */
    public void enableDrawer(boolean flag) {
        if (!flag)
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        else
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    /**
     * Pone escuchador para el icono de cerrar en el menu drawer
     */
    private void enableCloseIcon() {
        ImageView closeIcon = (ImageView) navigation.getHeaderView(0).findViewById(R.id.close_icon);
        closeIcon.setOnClickListener(this);
    }

    /**
     * Abre el drawer menu
     */
    public void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }

    /**
     * Se agrego este escuchador para remover de forma manual los fragments {@link ClientInformationView} y
     * {@link BusinessInformationView} de la pila, por que el {@link #onBackPressed()} no los remueve
     */
    @Override
    public void onBackStackChanged() {
        Iterator<Fragment> fragments = getSupportFragmentManager().getFragments().iterator();

        while (fragments.hasNext()) {

            Fragment fragment = fragments.next();
            if (fragment instanceof ClientInformationView ||
                    fragment instanceof BusinessInformationView)
                removeCurrentFragment(fragment);
        }

        Log.e("onBackStackChanged: ", getSupportFragmentManager().getFragments().toString());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {
        menuItem.setChecked(true);
        mNavItemId = menuItem.getItemId();
        drawer.closeDrawer(GravityCompat.START);

        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);

        return true;
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.SIGN_OUT_SUCCESS_V:
                loadFragment(R.id.container, new SignView());
                break;

            case ActionType.SIGN_OUT_ERROR_V:
                showSnackBar(txt70);
                break;
        }
    }

    /**
     * Metodo que inicializa un nuevo fragment de acuerdo
     * a la opción seleccionada en en drawer
     * @param itemId identificador del item seleccionado
     */
    private void navigate(final int itemId) {
        switch (itemId) {

            case R.id.action_clients:
                homeFragment = new HomeView();
                this.loadFragment(R.id.container,homeFragment);
                break;

            case R.id.action_catalog:
                productoFragment = new ProductView();
                this.loadFragment(R.id.container,productoFragment);
                break;

            case R.id.action_visits:
                visitFragment = new VisitView();
                this.loadFragment(R.id.container, visitFragment, "VisitView");
                break;

            case R.id.action_upload:
                loadFragment(R.id.container, new UploadView());
                break;

            case R.id.action_download:
                loadFragment(R.id.container, new DownloadView());
                break;

            case R.id.action_route:
                loadFragment(R.id.container, new RouteView());
                break;

            case R.id.menu_salir:
                signOut();
                break;
        }
    }

    /**
     * Configura el header del drawer
     * @param user nombre de usuario
     * @param detail detalle de usuario
     */
    public void setHeaderView(String user,String detail) {
        View view;

        if (navigation.getHeaderCount() == 0)// Veriica si hay un header
            view = navigation.inflateHeaderView(R.layout.nav_header);
         else
            view = navigation.getHeaderView(0);

        String[] splited = user.split("\\s+");
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(
                        splited[0].substring(0, 1),
                        getResources().getColor(R.color.colorAccent));
        ImageView icon = (ImageView) view.findViewById(R.id.imgAvatar);
        icon.setImageDrawable(drawable);
        TextView name = (TextView) view.findViewById(R.id.txtUsername);
        name.setText(user);
        TextView email = (TextView) view.findViewById(R.id.txtUserEmail);
        email.setText(detail);
        // Configura el navigationView
        setNavigation(detail);
    }

    /**
     * Configura el navigation view
     */
    public void setNavigation(String role){
        navigation.getMenu().clear();
        switch (role) {
            case "PREVENTISTA":
                navigation.inflateMenu(R.menu.menu_navigation_seller);
                mNavItemId = R.id.action_clients;
                break;

            case "SUPERVISOR":
                navigation.inflateMenu(R.menu.menu_navigation_supervisor);
                mNavItemId = R.id.action_upload;
                break;
        }

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawer, null, R.string.open,
                R.string.close);

        navigation.setNavigationItemSelectedListener(this);
        navigation.getMenu().findItem(mNavItemId).setChecked(true);
        drawer.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();
        navigate(mNavItemId);
    }

    public RecyclerView.Adapter getmRequestAdapter() {
        return mRequestAdapter;
    }

    public void setmRequestAdapter(RecyclerView.Adapter mRequestAdapter) {
        this.mRequestAdapter = mRequestAdapter;
    }

    /**
     * Metodo que ejecuta el cierre de sesión
     */
    public void signOut() {
        mMainPresenter.signOut();
    }

    /**
     * Verifica si el servicio especificado ya fue iniciado
     * @param serviceClass nombre servicio
     * @return booleano que indica si el servicio esta o no corriendo
     */
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
