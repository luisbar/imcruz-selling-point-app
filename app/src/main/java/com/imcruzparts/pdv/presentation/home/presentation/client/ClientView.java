package com.imcruzparts.pdv.presentation.home.presentation.client;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.home.HomeModel;
import com.imcruzparts.pdv.presentation.home.presentation.client.util.StepperAdapter;
import com.stepstone.stepper.StepperLayout;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)},
 * esta vista es utilizada para insertar un nuevo cliente si mHomeModel == null
 * o para modificar datos de un cliente guardado si mHomeModel != null
 */
public class ClientView extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable iconClose;
    @BindView(R.id.stepper)
    StepperLayout stepper;

    @BindString(R.string.txt_12)
    String toolbarTitleForNew;
    @BindString(R.string.txt_44)
    String toolbarTitleForUpdating;
    @BindString(R.string.txt_40)
    String txt40;
    @BindString(R.string.txt_41)
    String txt41;
    @BindString(R.string.txt_45)
    String txt45;
    @BindString(R.string.txt_46)
    String txt46;

    private Unbinder mUnbinder;
    private MainActivity mMainActivity;
    private ClientPresenter mClientPresenter;
    private StepperAdapter mAdapter;

    private boolean mShowAddClientItemMenu;
    private HomeModel mHomeModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);
        mClientPresenter = new ClientPresenter();

        mAdapter = new StepperAdapter(mMainActivity.getSupportFragmentManager(), mMainActivity);
        stepper.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(getArguments() == null ? toolbarTitleForNew : toolbarTitleForUpdating);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconClose);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mClientPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mClientPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_client, menu);
        menu.getItem(0).setVisible(mShowAddClientItemMenu ? true : false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.onBackPressed();
                break;

            case R.id.action_add_client:
                OnAddClientListener mOnAddClientListener = mAdapter.getmBusinessInformationView();

                if (mHomeModel == null)
                    mOnAddClientListener.saveNewClient();
                else
                    mOnAddClientListener.updateClient();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.ADD_CLIENT_SUCCESS_V:
                mMainActivity.showSnackBar(txt40);

                OnAddClientListener mOnAddClientListener = mAdapter.getmClientInformationView();
                mOnAddClientListener.clearClientInformationView();

                mOnAddClientListener = mAdapter.getmBusinessInformationView();
                mOnAddClientListener.clearBusinessInformationView();

                stepper.onBackClicked();
                break;

            case ActionType.ADD_CLIENT_ERROR_V:
                mMainActivity.showSnackBar(txt41);
                break;

            case ActionType.UPDATE_CLIENT_SUCCESS_V:
                mMainActivity.showSnackBar(txt45);
                break;

            case ActionType.UPDATE_CLIENT_ERROR_V:
                mMainActivity.showSnackBar(txt46);
                break;
        }
    }

    /**
     * Muestra u oculta el add client item del menu en el toolbar
     * @param flag bandera para mostrar u ocultar el menu
     */
    public void showAddClientItemMenu(boolean flag) {
        mShowAddClientItemMenu = flag;
        mMainActivity.invalidateOptionsMenu();
    }

    /**
     * Ejecuta el metodo {@link ClientPresenter#addClient(String, String, String, String, String, String, String, String, String, String, String, double, double)}
     * @param firstName nombre del cliente
     * @param lastName apellidos del cliente
     * @param documentNumber numero de documento del cliente
     * @param cellPhoneNumber numero de celular del cliente
     * @param phoneNumber numero de telefono fijo del cliente
     * @param email correo del cliente
     * @param socialReason razon social del cliente
     * @param nit nit del cliente
     * @param name nombre del negocio del cliente
     * @param state departamento donde esta el negocio del cliente
     * @param address dirección del negocio del cliente
     * @param latitud latitud del negocio del cliente
     * @param longitud longitud del negocio del cliente
     */
    public void addClient(String firstName, String lastName, String documentNumber,
                          String cellPhoneNumber, String phoneNumber, String email,
                          String socialReason, String nit, String name,
                          String state, String address, double latitud, double longitud) {

        mClientPresenter.addClient(firstName, lastName, documentNumber,
                cellPhoneNumber, phoneNumber, email,
                socialReason, nit, name,
                state, address, latitud, longitud);
    }

    /**
     * Ejecuta el metodo {@link ClientPresenter#updateClient(int, String, String, String, String, String, String, String, String, int, String, String, String, double, double)}
     * @param clientId identificador del cliente
     * @param firstName nombre del cliente
     * @param lastName apellidos del cliente
     * @param documentNumber numero de documento del cliente
     * @param cellPhoneNumber numero de celular del cliente
     * @param phoneNumber numero de telefono fijo del cliente
     * @param email correo del cliente
     * @param socialReason razon social del cliente
     * @param nit nit del cliente
     * @param businessId identificador del punto de venta
     * @param name nombre del negocio del cliente
     * @param state departamento donde esta el negocio del cliente
     * @param address dirección del negocio del cliente
     * @param latitud latitud del negocio del cliente
     * @param longitud longitud del negocio del cliente
     */
    public void updateClient(int clientId, String firstName, String lastName, String documentNumber,
                          String cellPhoneNumber, String phoneNumber, String email,
                          String socialReason, String nit, int businessId,String name,
                          String state, String address, double latitud, double longitud) {

        mClientPresenter.updateClient(clientId, firstName, lastName, documentNumber,
                cellPhoneNumber, phoneNumber, email,
                socialReason, nit, businessId, name,
                state, address, latitud, longitud);
    }

    /**
     * Metodo que dispara el evento
     * {@link com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation.ClientInformationView#loadClientInformation(HomeModel.ClientModel)}
     */
    public void loadDataForClientInformationView() {
        if (getArguments() != null) {
            mHomeModel = (HomeModel) getArguments().getSerializable("HomeModel");
            OnAddClientListener mOnAddClientListener = mAdapter.getmClientInformationView();
            mOnAddClientListener.loadClientInformation(mHomeModel.getClient());
        }
    }

    /**
     * Metodo que dispara el evento
     * {@link com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation.BusinessInformationView#loadBusinessInformation(HomeModel)}
     */
    public void loadDataForBusinessInformationView() {
        if (getArguments() != null) {
            mHomeModel = (HomeModel) getArguments().getSerializable("HomeModel");
            OnAddClientListener mOnAddClientListener = mAdapter.getmBusinessInformationView();
            mOnAddClientListener.loadBusinessInformation(mHomeModel);
        }
    }

    /**
     * Interface que es utilizada por {@link com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation.BusinessInformationView}
     * y {@link com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation.ClientInformationView}
     * y es ejecutada en el {@link #onOptionsItemSelected(MenuItem)}, en {@link #onMessageEvent(Action)} y en {@link #loadDataForBusinessInformationView()}
     */
    public interface OnAddClientListener {

        void saveNewClient();
        void updateClient();
        void clearBusinessInformationView();
        void clearClientInformationView();
        void loadBusinessInformation(HomeModel homeModel);
        void loadClientInformation(HomeModel.ClientModel clientModel);
    }
}
