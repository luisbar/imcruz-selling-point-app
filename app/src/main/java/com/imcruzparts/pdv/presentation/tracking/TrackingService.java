package com.imcruzparts.pdv.presentation.tracking;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.activity.ActivityDatabaseRepository;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class TrackingService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GpsStatus.Listener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationManager mLocationManager;
    private boolean mGpsEnabled = false;

    private List<ActivityModelLocal> lstPendigVisits;

    private final float ERROR_MARGIN = 10;
    private final double DISTANCE = 100;

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationManager = mLocationManager == null ? (LocationManager) getSystemService(Context.LOCATION_SERVICE) : mLocationManager;
        mGpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        configLocationRequest();
        buildGoogleApiClient();
        getPendingVisits();
        Log.e("onCreate: ", "si");
    }

    /**
     * Habilita la actualización de posición si el gps esta habilitado y recibe un
     * intent cuando el gps es habilitado o deshabilitado
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mGpsEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (intent != null &&
                intent.getAction() != null &&
                intent.getAction().matches("GPS")) {

            if (mGpsEnabled)
                startLocationUpdates();
            else
                stopLocationUpdates();
        }
        Log.e("onStartCommand: ", "si");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
        configLocationRequest();
        EventBusMask.unregister(this);
        Log.e("onDestroy: ", "si");
    }

    /**
     * Escuchador de {@link GpsStatus}
     * @param event Integer
     */
    @Override
    public void onGpsStatusChanged(int event) {
        switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                startLocationUpdates();
                Log.e("started: ", "si");
                break;

            case GpsStatus.GPS_EVENT_STOPPED:
                Log.e("stopped: ", "si");
                stopLocationUpdates();
                break;

            case GpsStatus.GPS_EVENT_FIRST_FIX:
                Log.e("fix: ", "si");
                break;

            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                Log.e("status: ", "si");
                break;
        }
    }

    /**
     * Escuchador para {@link com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks}
     * cuando la petición de conexión fue exitosa
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mGpsEnabled) startLocationUpdates();
        Log.e("onConnected: ", "si");
    }

    /**
     * Escuchador para {@link com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks}
     * cuando la petición de conexión fue suspendida
     */
    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        Log.e("onConnectionSuspended: ", "si");
    }

    /**
     * Escuchador para {@link com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks}
     * cuando la petición de conexión fallo
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("onConnectionFailed", connectionResult.getErrorMessage().toString());
    }

    /**
     * Escuchador que es disparado cuando la ubicación cambia,
     * si el margen de error menor igual a ERROR_MARGIN y la distancia
     * entre los dos puntos menor igual a DISTANCE, entonces
     * actualiza el estado de la actividad a FINALIZADA
     * @param location Location object
     */
    @Override
    public void onLocationChanged(Location location) {
        if (location.getAccuracy() <= ERROR_MARGIN) {

            for (int i = lstPendigVisits.size() - 1; i >= 0 ; i--) {
                ActivityModelLocal activity = lstPendigVisits.get(i);

                double distanceDiff = SphericalUtil.computeDistanceBetween(
                        new LatLng(location.getLatitude(), location.getLongitude()),
                        new LatLng(activity.getPdv().getLatitud(), activity.getPdv().getLongitud()));

                if (distanceDiff <= DISTANCE) {
                   new ActivityDatabaseRepository().update(
                           ActivityDatabaseRepository.Params.paramsForUpdate(
                                   activity.getIdActividad(),
                                   "stateNotVisibleActivity",
                                   "FINALIZADA"
                           )
                   );

                   lstPendigVisits.remove(i);
                }
            }
        }

        Log.e("pdvs: ", lstPendigVisits.size() + "");
        Log.e("onLocationChanged: ", String.valueOf(location.getLatitude()) + "|" + String.valueOf(location.getLongitude()));
        Log.e("onLocationChanged: ", location.getAccuracy() + "|" + location.getProvider());
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.GET_PENDING_VISITS:
                lstPendigVisits = (List<ActivityModelLocal>) action.getPayload();
                Log.e("onMessageEvent: ", lstPendigVisits.toString());
                break;
        }
    }

    /**
     * Configura {@link GoogleApiClient} y setea los escuchadores para
     * {@link GoogleApiClient}
     */
    private void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        if (!mGoogleApiClient.isConnected()) mGoogleApiClient.connect();
        Log.e("buildGoogleApiClient: ", "si");
    }

    /**
     * Este configura el objeto {@link LocationRequest}
     */
    private void configLocationRequest() {
        mLocationRequest = mLocationRequest == null ? new LocationRequest() : mLocationRequest;
        mLocationRequest.setInterval(2000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Log.e("configLocationRequest: ", "si");
    }

    /**
     * Añade escuchador para obtener las nuevas ubicaciones
     */
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("startLocationUpdates: ", "not ok");
            return;
        }
        Log.e("startLocationUpdates: ", "ok");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Para la actualización de ubicación
     */
    private void stopLocationUpdates() {
        Log.e("stopLocationUpdates: ", "si");
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
    }

    /**
     * Obtiene las actividades con valor VISITAR y que tengan estado
     * PENDIENTE
     */
    private void getPendingVisits() {
        EventBusMask.register(this);
        new ActivityDatabaseRepository().queryMany(null);
    }
}
