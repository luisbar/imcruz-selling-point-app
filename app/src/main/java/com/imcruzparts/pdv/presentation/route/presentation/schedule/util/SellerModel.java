package com.imcruzparts.pdv.presentation.route.presentation.schedule.util;

import java.io.Serializable;

public class SellerModel implements Serializable {

    private String Apellido;
    private String Direccion;
    private Distributor Distribuidor;
    private String Email;
    private String FechaAlta;
    private Object FechaBaja;
    private Integer IdEmpleado;
    private String Nombre;
    private Object Sesion;
    private String Telefono;

    public SellerModel(String apellido, String direccion, Distributor distribuidor, String email, String fechaAlta, Object fechaBaja, Integer idEmpleado, String nombre, Object sesion, String telefono) {
        Apellido = apellido;
        Direccion = direccion;
        Distribuidor = distribuidor;
        Email = email;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
        IdEmpleado = idEmpleado;
        Nombre = nombre;
        Sesion = sesion;
        Telefono = telefono;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public Distributor getDistribuidor() {
        return Distribuidor;
    }

    public void setDistribuidor(Distributor distribuidor) {
        Distribuidor = distribuidor;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public Object getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(Object fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public Integer getIdEmpleado() {
        return IdEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        IdEmpleado = idEmpleado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public Object getSesion() {
        return Sesion;
    }

    public void setSesion(Object sesion) {
        Sesion = sesion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    @Override
    public String toString() {
        return Nombre + " " + Apellido;
    }

    public static class Distributor implements Serializable {

        private String Direccion;
        private String Estado;
        private String FechaAlta;
        private String FechaBaja;
        private Integer IdDistribuidor;
        private String Nit;
        private String RazonSocial;
        private String Responsable;

        public Distributor(String direccion, String estado, String fechaAlta, String fechaBaja, Integer idDistribuidor, String nit, String razonSocial, String responsable) {
            Direccion = direccion;
            Estado = estado;
            FechaAlta = fechaAlta;
            FechaBaja = fechaBaja;
            IdDistribuidor = idDistribuidor;
            Nit = nit;
            RazonSocial = razonSocial;
            Responsable = responsable;
        }

        public String getDireccion() {
            return Direccion;
        }

        public void setDireccion(String direccion) {
            Direccion = direccion;
        }

        public String getEstado() {
            return Estado;
        }

        public void setEstado(String estado) {
            Estado = estado;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdDistribuidor() {
            return IdDistribuidor;
        }

        public void setIdDistribuidor(Integer idDistribuidor) {
            IdDistribuidor = idDistribuidor;
        }

        public String getNit() {
            return Nit;
        }

        public void setNit(String nit) {
            Nit = nit;
        }

        public String getRazonSocial() {
            return RazonSocial;
        }

        public void setRazonSocial(String razonSocial) {
            RazonSocial = razonSocial;
        }

        public String getResponsable() {
            return Responsable;
        }

        public void setResponsable(String responsable) {
            Responsable = responsable;
        }
    }
}
