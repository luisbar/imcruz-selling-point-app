package com.imcruzparts.pdv.presentation.route.presentation.schedule;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.common.NetDateTimeAdapter;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.route.AddSellerRoute;
import com.imcruzparts.pdv.domain.route.RemoveSellerRoute;
import com.imcruzparts.pdv.presentation.route.RouteModel;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Clase que valida la vista {@link ScheduleView},
 * y dispara el escuchador {@link ScheduleView#onMessageEvent(Action)}
 */
public class SchedulePresenter {

    private UseCase mAddSellerRoute;
    private UseCase mRemoveSellerRoute;

    public SchedulePresenter() {
        mAddSellerRoute = new AddSellerRoute();
        mRemoveSellerRoute = new RemoveSellerRoute();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.SELLER_ROUTE_ADDED_P:
                if (action.isError())
                    EventBusMask.post(new Action(ActionType.SELLER_ROUTE_ADDED_V, action.getPayload(), action.isError()));
                else {
                    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new NetDateTimeAdapter()).create();
                    Type scheduleType = new TypeToken<ScheduleModel>() {}.getType();

                    String schedule = gson.toJson(action.getPayload());

                    ScheduleModel scheduleModel = gson.fromJson(schedule.toString(), scheduleType);
                    EventBusMask.post(new Action(
                            ActionType.SELLER_ROUTE_ADDED_V,
                            scheduleModel,
                            action.isError()));
                }
                break;

            case ActionType.SELLER_ROUTE_REMOVED_P:
                EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_V, action.getPayload(), action.isError()));
                break;
        }
    }

    /**
     * Ejecuta metodo que guarda una nueva ruta vendedor
     */
    public void addSellerRoute(int idDistributor, int idSeller, int idRoute, Date programationDate, String note, RouteModel routeModel) {
        ScheduleModel sellerRoute = new ScheduleModel();

        sellerRoute.setIdDistribuidor(idDistributor);
        sellerRoute.setRutaVenta(new ScheduleModel.Route(
                routeModel.getDepartamento(),
                routeModel.getDescripcion(),
                routeModel.getFechaAlta(),
                routeModel.getFechaBaja(),
                routeModel.getIdRuta(),
                routeModel.getNombre(),
                routeModel.getZona(),
                routeModel.getIdDistribuidor(),
                routeModel.getDistribuidor()
        ));
        sellerRoute.setVendedor(new ScheduleModel.Seller(idSeller));
        sellerRoute.setFechaProgramacion(programationDate);
        sellerRoute.setDescripcion(note);

        mAddSellerRoute.execute(sellerRoute);
    }

    /**
     * Ejecuta metodo que elimina una ruta vendedor en la nube
     */
    public void removeSellerRoute(ScheduleModel sellerRoute) {
       mRemoveSellerRoute.execute(sellerRoute);
    }
}
