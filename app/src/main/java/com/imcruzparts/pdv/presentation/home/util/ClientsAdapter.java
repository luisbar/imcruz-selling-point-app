package com.imcruzparts.pdv.presentation.home.util;

import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.home.HomeModel;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.ClientsViewHolder> implements StickyRecyclerHeadersAdapter<ClientsAdapter.ClientsHeaderViewHolder> {

    private List<HomeModel> lstClients;
    private List<HomeModel> lstClientsNotFiltered;
    private Pagination mPagination;
    private OnItemClicked mOnItemClicked;
    private boolean paginated;

    public ClientsAdapter(Fragment fragment, boolean paginated) {
        this.mPagination = (Pagination) fragment;
        this.mOnItemClicked = (OnItemClicked) fragment;
        this.paginated = paginated;
        lstClients = new ArrayList();
    }

    @Override
    public ClientsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_clients, parent, false);
        ClientsViewHolder clientsViewHolder = new ClientsViewHolder(itemView);

        return clientsViewHolder;
    }

    @Override
    public void onBindViewHolder(ClientsViewHolder holder, int position) {
        String clientName =  lstClients.get(position).getClient().getFirstName() +
                " " +
                lstClients.get(position).getClient().getLastName();
        String bussinesName = lstClients.get(position).getName();

        holder.setData(clientName, bussinesName, lstClients.get(position));
        paginate(position);
    }

    @Override
    public int getItemCount() {
        return lstClients.size();
    }

    /**
     * Si paginated = true, entonces dispara escuchador en
     * {@link com.imcruzparts.pdv.presentation.home.HomeView}
     * para traer mas clientes
     * @param position posición actual de la lista de clientes
     */
    public void paginate(int position) {
        if (paginated && position == lstClients.size() - 1)
            mPagination.getMore();
    }

    /**
     * Para cuando paginated = true
     * @param isCompuntingLayout bandera para saber si el recycler view esta
     *                           siendo utilizado
     * @param lstClients nuevos clientes a adjuntar a la lista de clientes
     */
    public void addMoreClients(boolean isCompuntingLayout, List<HomeModel> lstClients) {
        this.lstClients.addAll(lstClients);

        if (!isCompuntingLayout)
            notifyDataSetChanged();
        else
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
    }

    /**
     * Si paginated = false, se utiliza este metodo para guardar
     * todos los clientes en el adapter
     * @param lstClients lista de todos los clientes en la bd
     */
    public void setClients(List<HomeModel> lstClients) {
        this.lstClients = new ArrayList();
        this.lstClients.addAll(lstClients);
        this.lstClientsNotFiltered = lstClients;
    }

    /**
     * Si paginated = false, entonces se utiliza este metodo para setear la
     * lista filtrada
     * @param lstClients lista de clientes filtrados
     */
    public void setFilteredClients(List<HomeModel> lstClients) {
        this.lstClients = new ArrayList();
        this.lstClients.addAll(lstClients);
        notifyDataSetChanged();
    }

    /**
     * Metodo que restaura la lstClients despues de haber cerrado
     * el filtro
     */
    public void restoreAdapter() {
        if (lstClientsNotFiltered != null) {
            lstClients = lstClientsNotFiltered;
            notifyDataSetChanged();
        }
    }

    public List<HomeModel> getLstClientsNotFiltered() {
        return lstClientsNotFiltered;
    }

    /**
     * Interface para obtener mas datos de la bd, si es que paginated = true,
     * si se habilita la paginación aqui, hay que habilitarla en HomeDatabaseRepository
     * tambien
     */
    public interface Pagination {

        void getMore();
    }

    /**
     * Interface para disparar un evento en {@link com.imcruzparts.pdv.presentation.home.HomeView}
     * cuando el usuario presione un item del recycler view
     */
    public interface OnItemClicked {

        void onItemClicked(HomeModel homeModel);
    }

    public class ClientsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_client_name)
        TextView textClientName;
        @BindView(R.id.text_business_name)
        TextView textBusinessName;

        @BindView(R.id.icon_name)
        ImageView iconName;

        public ClientsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(String clientName, String businessName, final HomeModel homeModel) {
            textClientName.setText(clientName);
            textBusinessName.setText(businessName);
            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .useFont(Typeface.DEFAULT)
                    .toUpperCase()
                    .endConfig()
                    .buildRound(
                            String.valueOf(clientName.substring(0,1)),
                            ColorGenerator.MATERIAL.getColor(clientName));
            iconName.setImageDrawable(drawable);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClicked.onItemClicked(homeModel);
                }
            });
        }
    }

    public static class ClientsHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public ClientsHeaderViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    @Override
    public long getHeaderId(int position) {
        if (position >= 0 && position < getItemCount()) {
            final HomeModel entity = lstClients.get(position);
            return entity.getClient().getFirstName().charAt(0);
        } else {
            return -1;
        }
    }

    @Override
    public ClientsHeaderViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_sticky_subheader, viewGroup, false);
        return new ClientsHeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(ClientsHeaderViewHolder holder, int i) {
        if (i >= 0) {
            final HomeModel entity = lstClients.get(i);
            String license = entity.getClient().getFirstName().substring(0,1);
            holder.title.setText(license);
        }
    }

    private View mEmptyView;
    /**
     * Set the empty view to be used so that
     * @param emptyView
     */
    public void setEmptyView(View emptyView) {
        if (mEmptyView != null) {
            unregisterAdapterDataObserver(mEmptyObserver);
        }
        mEmptyView = emptyView;
        registerAdapterDataObserver(mEmptyObserver);
    }

    /**
     * Check if we should show the empty view
     */
    private void checkIfEmpty() {
        if (mEmptyView != null) {
            mEmptyView.setVisibility(getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Data change observer
     */
    private RecyclerView.AdapterDataObserver mEmptyObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkIfEmpty();
        }
    };
}
