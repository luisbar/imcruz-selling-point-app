package com.imcruzparts.pdv.presentation.product.util;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.TextView;

import com.imcruzparts.pdv.R;

/**
 * Adapter para el filterView, el cual es un objeto tipo {@link android.support.v7.widget.SearchView}
 */
public class FilterSuggestionAdapter extends SimpleCursorAdapter {

    public FilterSuggestionAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textView = (TextView)view.findViewById(R.id.text_filter_aux);
        textView.setText(cursor.getString(1));
    }
}
