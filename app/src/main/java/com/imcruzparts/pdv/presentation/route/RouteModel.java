package com.imcruzparts.pdv.presentation.route;

import java.io.Serializable;

public class RouteModel implements Serializable {

    private String Departamento;
    private String Descripcion;
    private String FechaAlta;
    private String FechaBaja;
    private Integer IdRuta;
    private String Nombre;
    private String Zona;
    private Integer IdDistribuidor;
    private String Distribuidor;

    public RouteModel(String departamento, String descripcion, String fechaAlta, String fechaBaja, Integer idRuta, String nombre, String zona, Integer idDistribuidor, String distribuidor) {
        Departamento = departamento;
        Descripcion = descripcion;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
        IdRuta = idRuta;
        Nombre = nombre;
        Zona = zona;
        IdDistribuidor = idDistribuidor;
        Distribuidor = distribuidor;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String departamento) {
        Departamento = departamento;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public Integer getIdRuta() {
        return IdRuta;
    }

    public void setIdRuta(Integer idRuta) {
        IdRuta = idRuta;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getZona() {
        return Zona;
    }

    public void setZona(String zona) {
        Zona = zona;
    }

    public Integer getIdDistribuidor() {
        return IdDistribuidor;
    }

    public void setIdDistribuidor(Integer idDistribuidor) {
        IdDistribuidor = idDistribuidor;
    }

    public String getDistribuidor() {
        return Distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        Distribuidor = distribuidor;
    }
}
