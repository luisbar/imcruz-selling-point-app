package com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.util;

import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.request.model.RequestDetailModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Mapea objeto de {@link RequestModelLocal}
 * a {@link RequestModel}
 */
public class RmlToRm implements Mapper<RequestModelLocal, RequestModel> {

    @Override
    public RequestModel map(RequestModelLocal requestModelLocal) {

        return new RequestModel(
                requestModelLocal.getIdPedido(),
                requestModelLocal.getIdPdv(),
                requestModelLocal.getFecha(),
                requestModelLocal.getEstado(),
                requestModelLocal.getSubTotal(),
                requestModelLocal.getDescuento(),
                requestModelLocal.getMontoTotal(),
                getDetail(requestModelLocal.getDetalle())
        );
    }

    private List<RequestModel.RequestDetail> getDetail(List<RequestDetailModelLocal> requestDetailModelLocals) {
        List<RequestModel.RequestDetail> lstDetail = new ArrayList();

        for (RequestDetailModelLocal requestDetailModelLocal : requestDetailModelLocals) {
            lstDetail.add(
                    new RequestModel.RequestDetail(
                            requestDetailModelLocal.getIdDetalle(),
                            requestDetailModelLocal.getCantidad(),
                            requestDetailModelLocal.getPrecio(),
                            requestDetailModelLocal.getDescuento(),
                            requestDetailModelLocal.getSubTotal(),
                            requestDetailModelLocal.getFechaAlta(),
                            requestDetailModelLocal.getFechaBaja(),
                            requestDetailModelLocal.getProducto().getIdProducto(),
                            requestDetailModelLocal.getProducto().getCiaCod(),
                            requestDetailModelLocal.getProducto().getCodProducto(),
                            requestDetailModelLocal.getProducto().getDescripcion(),
                            requestDetailModelLocal.getProducto().getSegmento(),
                            requestDetailModelLocal.getProducto().getCanal(),
                            requestDetailModelLocal.getProducto().getFabrica(),
                            requestDetailModelLocal.getProducto().getOrigen(),
                            requestDetailModelLocal.getProducto().getFamilia(),
                            requestDetailModelLocal.getProducto().getMarca(),
                            requestDetailModelLocal.getProducto().getGrupo(),
                            requestDetailModelLocal.getProducto().getTipo(),
                            requestDetailModelLocal.getProducto().getPrecio(),
                            requestDetailModelLocal.getProducto().getDescuento()
                    )
            );
        }

        return lstDetail;
    }
}
