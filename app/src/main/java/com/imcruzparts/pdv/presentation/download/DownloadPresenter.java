package com.imcruzparts.pdv.presentation.download;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.download.GetClientsFromCloud;
import com.imcruzparts.pdv.domain.download.GetProductsFromCloud;

import org.greenrobot.eventbus.Subscribe;

/**
 * Clase que valida la vista {@link DownloadView},
 * y dispara el escuchador {@link DownloadView#onMessageEvent(Action)}
 */
public class DownloadPresenter {

    private UseCase mGetClients;
    private UseCase mGetProducts;

    public DownloadPresenter() {
        this.mGetClients = new GetClientsFromCloud();
        this.mGetProducts = new GetProductsFromCloud();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.CLIENTS_DOWNLOADED_P:
                EventBusMask.post(new Action(ActionType.CLIENTS_DOWNLOADED_V, action.getPayload(), action.isError()));
                break;

            case ActionType.PRODUCTS_DOWNLOADED_P:
                EventBusMask.post(new Action(ActionType.PRODUCTS_DOWNLOADED_V, action.getPayload(), action.isError()));
                break;
        }
    }

    /**
     * Ejecuta metodo que trae los clientes desde la nube
     * y los almacena en la base de datos local
     */
    public void getClients() {
        mGetClients.execute();
    }

    /**
     * Ejecuta metodo que trae los productos desde la nube
     * y los almacena en la base de datos local
     */
    public void getProducts() {
        mGetProducts.execute();
    }
}
