package com.imcruzparts.pdv.presentation.product.util;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.product.ProductModel;
import com.imcruzparts.pdv.presentation.product.ProductView;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> implements StickyRecyclerHeadersAdapter<ProductAdapter.ProductHeaderViewHolder> {

    private List<ProductModel> lstProducts;
    private List<ProductModel> lstProductsNotFiltered;
    private OnItemClicked mOnItemClicked;

    public ProductAdapter(List<ProductModel> lstProducts, Fragment fragment) {
        this.lstProducts = lstProducts;
        this.lstProductsNotFiltered = lstProducts;
        this.mOnItemClicked = (OnItemClicked) fragment;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_products,
                parent,
                false
        );

        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.setData(lstProducts.get(position).getDescription(),
                lstProducts.get(position).getProductCode(), lstProducts.get(position));
    }

    @Override
    public int getItemCount() {
        return lstProducts.size();
    }

    /**
     * Se utiliza este metodo para guardar
     * todos los PRODUCTOS en el adapter
     * @param lstProducts lista de todos los clientes en la bd
     */
    public void setProducts(List<ProductModel> lstProducts) {
        this.lstProducts = new ArrayList();
        this.lstProducts.addAll(lstProducts);
        this.lstProductsNotFiltered = lstProducts;
    }

    /**
     * Setea la lista filtrada
     * @param lstProducts lista de productos filtrados
     */
    public void setFilteredClients(List<ProductModel> lstProducts) {
        this.lstProducts = lstProducts;
        notifyDataSetChanged();
    }

    /**
     * Metodo que restaura la lstProducts despues de haber cerrado
     * el filtro
     */
    public void restoreAdapter() {
        lstProducts = lstProductsNotFiltered;
        notifyDataSetChanged();
    }

    public List<ProductModel> getLstProductsNotFiltered() {
        return lstProductsNotFiltered;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_product_description)
        TextView textProductDescription;
        @BindView(R.id.text_product_code)
        TextView textProductCode;

        @BindView(R.id.icon_name)
        ImageView iconName;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(String description, String code, final ProductModel productModel) {
            textProductDescription.setText(description);
            textProductCode.setText(code);

            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .useFont(Typeface.DEFAULT)
                    .toUpperCase()
                    .endConfig()
                    .buildRound(
                            String.valueOf(productModel.getDescription().substring(0,1)),
                            ColorGenerator.MATERIAL.getColor(productModel.getDescription()));
            iconName.setImageDrawable(drawable);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClicked.onItemClicked(productModel);
                }
            });
        }
    }

    @Override
    public long getHeaderId(int position) {
        if (position >= 0 && position < getItemCount()) {
            final ProductModel entity = lstProducts.get(position);
            return entity.getDescription().charAt(0);
        } else {
            return -1;
        }
    }

    @Override
    public ProductHeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_sticky_subheader, parent, false);
        return new ProductHeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(ProductHeaderViewHolder holder, int position) {
        if (position >= 0) {
            final ProductModel entity = lstProducts.get(position);
            String license = entity.getDescription().substring(0,1);
            holder.title.setText(license);
        }
    }

    public static class ProductHeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;

        public ProductHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private View mEmptyView;

    /**
     * Set the empty view to be used so that
     * @param emptyView
     */
    public void setEmptyView(View emptyView) {
        if (mEmptyView != null) {
            unregisterAdapterDataObserver(mEmptyObserver);
        }
        mEmptyView = emptyView;
        registerAdapterDataObserver(mEmptyObserver);
    }

    /**
     * Check if we should show the empty view
     */
    private void checkIfEmpty() {
        if (mEmptyView != null) {
            mEmptyView.setVisibility(getItemCount() > 0 ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Data change observer
     */
    private RecyclerView.AdapterDataObserver mEmptyObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkIfEmpty();
        }
    };

    /**
     * Interface para disparar un evento en {@link ProductView}
     * cuando el usuario presione un item del recycler view
     */
    public interface OnItemClicked {

        void onItemClicked(ProductModel productModel);
    }
}
