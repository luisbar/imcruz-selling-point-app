package com.imcruzparts.pdv.presentation.product;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.product.model.ProductModelLocal;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.product.GetProducts;
import com.imcruzparts.pdv.presentation.product.util.FilterObject;
import com.imcruzparts.pdv.presentation.product.util.PmlToPm;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductPresenter {

    private UseCase mGetProducts;

    public ProductPresenter() {
        this.mGetProducts = new GetProducts();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Ejecuta metodo de la capa de domain para solicitar
     * los productos en la bd
     */
    public void getProducts() {
        mGetProducts.execute();
    }

    public void getTypes() {
        EventBusMask.post(new Action(ActionType.TYPES_OBTAINED_P));
    }

    /**
     * Filtra la lista de productos de acuerdo a lo que el usuario introdujo en el
     * searchview, el dato introducido por el usuario es comparado con la descripcion
     * y el codigo del producto
     * @param lstProducts lista completa de productos
     * @param query dato introducido por el usuario
     * @return lista de productos filtrada
     */
    public List<ProductModel> filter(List<ProductModel> lstProducts, String query) {

        query = query.toLowerCase();
        List<ProductModel> lstFilteredProducts = new ArrayList<>();

        if (lstProducts!=null && !lstProducts.isEmpty()) {
            for (ProductModel product : lstProducts) {

                String description = product.getDescription().toLowerCase();
                String productCode = product.getProductCode().toLowerCase();

                if (description.contains(query) ||
                        productCode.contains(query)) {
                    lstFilteredProducts.add(product);
                }
            }
        }

        return lstFilteredProducts;
    }

    /**
     * Filtra la lista de productos de acuerdo al tipo, los cuales son
     * l(lubricantes), ll(llantas) y r(repuestos)
     * @param lstProducts lista completa de productos
     * @param type tipo seleccionado por el usuario
     * @return lista de productos filtrada por tipo
     */
    public List filterByType(List<ProductModel> lstProducts, String type) {

        type = transformType(type);
        List<ProductModel> lstFilteredProducts = new ArrayList<>();
        List<FilterObject> lstBrands = new ArrayList();

        if (lstProducts!=null && !lstProducts.isEmpty()) {
            for (ProductModel product : lstProducts) {

                String mType = product.getType().toLowerCase();
                mType = mType.trim();

                if (mType.equalsIgnoreCase(type)) {
                    lstFilteredProducts.add(product);

                    if (!lstBrands.contains(new FilterObject(product.getBrand().trim())))
                        lstBrands.add(new FilterObject(product.getBrand().trim()));
                }
            }
        }

        return Arrays.asList(lstFilteredProducts, lstBrands);
    }

    /**
     * Filtra los productos de acuerdo al tipo y maraca
     * @param lstProducts lista de productos completa
     * @param type tipo de producto
     * @param brand marca de producto
     * @return lista de productos filtrada por tipo y marca
     */
    public List filterByTypeAndBrand(List<ProductModel> lstProducts, String type, String brand) {

        type = transformType(type);
        List<ProductModel> lstFilteredProducts = new ArrayList<>();
        List<FilterObject> lstSegments = new ArrayList();

        if (lstProducts!=null && !lstProducts.isEmpty()) {
            for (ProductModel product : lstProducts) {

                String mType = product.getType().toLowerCase();
                String mBrand = product.getBrand().toLowerCase();

                mType = mType.trim();
                mBrand = mBrand.trim();

                if (mType.equalsIgnoreCase(type) &&
                        mBrand.equalsIgnoreCase(brand)) {
                    lstFilteredProducts.add(product);

                    if (!lstSegments.contains(new FilterObject(product.getSegment().trim())))
                        lstSegments.add(new FilterObject(product.getSegment().trim()));
                }
            }
        }

        return Arrays.asList(lstFilteredProducts, lstSegments);
    }

    /**
     * Filtra los productos por tipo, marca y segmento
     * @param lstProducts lista de productos completa
     * @param type tipo de producto
     * @param brand marca de producto
     * @param segment segmento de producto
     * @return lista de productos filtrada
     */
    public List filterByTypeBrandAndSegment(List<ProductModel> lstProducts, String type, String brand, String segment) {

        type = transformType(type);
        List<ProductModel> lstFilteredProducts = new ArrayList<>();

        if (lstProducts!=null && !lstProducts.isEmpty()) {
            for (ProductModel product : lstProducts) {

                String mType = product.getType().toLowerCase();
                String mBrand = product.getBrand().toLowerCase();
                String mSegment = product.getSegment().toLowerCase();

                mType = mType.trim();
                mBrand = mBrand.trim();
                mSegment = mSegment.trim();

                if (mType.equalsIgnoreCase(type) &&
                        mBrand.equalsIgnoreCase(brand) &&
                        mSegment.equalsIgnoreCase(segment)) {
                    lstFilteredProducts.add(product);
                }
            }
        }

        return Arrays.asList(lstFilteredProducts, new ArrayList<FilterObject>());
    }

    /**
     * Devuelve el valor al que pertenece la descripción del tipo
     * @param type descripción del tipo
     * @return valor del tipo
     */
    private String transformType(String type) {
        type = type.trim().equalsIgnoreCase("Lubricantes")
                ? "L"
                : type.trim().equalsIgnoreCase("Repuestos")
                ? "R"
                : null;

        return type;
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.PRODUCTS_OBTAINED_P:
                EventBusMask.post(new Action(
                        ActionType.PRODUCTS_OBTAINED_V,
                        new PmlToPm().map((List<ProductModelLocal>) action.getPayload()),
                        false
                ));
                break;

            case ActionType.TYPES_OBTAINED_P:
                EventBusMask.post(new Action(ActionType.TYPES_OBTAINED_V, Arrays.asList(
                        new FilterObject("Lubricantes"),
                        new FilterObject("Repuestos")
                )));
                break;
        }
    }
}
