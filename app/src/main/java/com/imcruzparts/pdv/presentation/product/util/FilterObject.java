package com.imcruzparts.pdv.presentation.product.util;

public class FilterObject {

    private String value;

    public FilterObject(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        FilterObject filterObject = (FilterObject) obj;
        return this.value.equalsIgnoreCase(filterObject.getValue());
    }
}
