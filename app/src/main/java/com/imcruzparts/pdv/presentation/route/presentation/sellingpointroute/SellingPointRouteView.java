package com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SellingPointRouteView extends Fragment implements OnMapReadyCallback {

    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable iconClose;

    @BindString(R.string.txt_123)
    String txt123;
    @BindString(R.string.txt_128)
    String txt128;
    @BindString(R.string.txt_129)
    String txt129;
    @BindString(R.string.txt_8)
    String txt8;
    @BindString(R.string.txt_73)
    String txt73;
    @BindString(R.string.txt_130)
    String txt130;
    @BindString(R.string.txt_131)
    String txt131;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Unbinder unbinder;
    @BindView(R.id.mapview_selling_point_route)
    MapView mapviewSellingPointRoute;

    private MainActivity mMainActivity;
    private List<SellingPointRouteModel> lstSellingPointRoutesAssigned;
    private List<SellingPointRouteModel> lstSellingPointRoutesUnassigned;
    private GoogleMap mGoogleMap;
    private SellingPointRoutePresenter mSellingPointRoutePresenter;
    private Snackbar mSnackbar;

    private final String ASSIGNED = "assigned";
    private final String UNASSIGNED = "unassigned";

    private int unassignedPosition;
    private int assignedPosition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selling_point_route, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);

        mSellingPointRoutePresenter = new SellingPointRoutePresenter();

        unbinder = ButterKnife.bind(this, view);

        MapsInitializer.initialize(mMainActivity);
        mapviewSellingPointRoute.onCreate(savedInstanceState);
        mapviewSellingPointRoute.getMapAsync(this);

        lstSellingPointRoutesAssigned = getArguments() != null
                ? (List<SellingPointRouteModel>) getArguments().getSerializable("sellingPointRoutesAssigned")
                : new ArrayList<SellingPointRouteModel>();

        lstSellingPointRoutesUnassigned = getArguments() != null
                ? (List<SellingPointRouteModel>) getArguments().getSerializable("sellingPointRoutesUnassigned")
                : new ArrayList<SellingPointRouteModel>();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt123);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconClose);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapviewSellingPointRoute.onStart();
        EventBusMask.register(this);
        mSellingPointRoutePresenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapviewSellingPointRoute.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapviewSellingPointRoute.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mapviewSellingPointRoute.onStop();
        mSellingPointRoutePresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (mSnackbar != null)
            mSnackbar.dismiss();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapviewSellingPointRoute != null)
            mapviewSellingPointRoute.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapviewSellingPointRoute.onLowMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        loadMarkers();

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-17.7832433,
                    -63.182739), 10));

        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                MarkerTag tag = (MarkerTag) marker.getTag();

                switch (tag.getState()) {

                    case ASSIGNED:
                        assignedPosition = tag.getPosition();
                        SellingPointRouteModel sellingPointRouteAssigned;
                        sellingPointRouteAssigned = lstSellingPointRoutesAssigned.get(tag.getPosition());
                        showSnackbarForRemoving(sellingPointRouteAssigned.getPDV().getNombre(), sellingPointRouteAssigned);
                        break;

                    case UNASSIGNED:
                        unassignedPosition = tag.getPosition();
                        SellingPointRouteModel sellingPointRouteUnassigned;
                        sellingPointRouteUnassigned = lstSellingPointRoutesUnassigned.get(tag.getPosition());
                        showSnackbarForAdding(sellingPointRouteUnassigned.getPDV().getNombre(), sellingPointRouteUnassigned);
                        break;
                }
                return false;
            }
        });
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.ROUTE_ADDED_V:
                if (action.isError()) {
                    String error = (int) action.getPayload() == R.string.txt_8
                            ? txt8
                            : (int) action.getPayload() == R.string.txt_73
                            ? txt73
                            : null;

                    mMainActivity.showSnackBar(error);
                } else {
                    mMainActivity.showSnackBar(txt130);
                    lstSellingPointRoutesAssigned.add((SellingPointRouteModel) action.getPayload());
                    lstSellingPointRoutesUnassigned.remove(unassignedPosition);
                    mGoogleMap.clear();
                    loadMarkers();
                }
                break;

            case ActionType.ROUTE_REMOVED_V:
                if (action.isError()) {
                    String error = (int) action.getPayload() == R.string.txt_8
                            ? txt8
                            : (int) action.getPayload() == R.string.txt_73
                            ? txt73
                            : null;

                    mMainActivity.showSnackBar(error);
                } else {
                    mMainActivity.showSnackBar(txt131);
                    lstSellingPointRoutesUnassigned.add(lstSellingPointRoutesAssigned.remove(assignedPosition));
                    mGoogleMap.clear();
                    loadMarkers();
                }
                break;
        }
    }

    /**
     * Muestra snackbar para agregar un pdv de la ruta
     */
    private void showSnackbarForAdding(String sellingPointName, final SellingPointRouteModel sellingPointRouteUnassigned) {
        View rootView =  mMainActivity.findViewById(R.id.container);
        mSnackbar = Snackbar.make(rootView, sellingPointName, Snackbar.LENGTH_INDEFINITE)
                        .setAction(txt128, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mSellingPointRoutePresenter.addRoute(sellingPointRouteUnassigned);
                            }
                        });
        mSnackbar.show();
    }

    /**
     * Muestra snackbar para remover un pdv de una ruta
     */
    private void showSnackbarForRemoving(String sellingPointName, final SellingPointRouteModel sellingPointRouteAssigned) {
        View rootView =  mMainActivity.findViewById(R.id.container);
        mSnackbar = Snackbar.make(rootView, sellingPointName, Snackbar.LENGTH_INDEFINITE)
                        .setAction(txt129, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mSellingPointRoutePresenter.removeRoute(sellingPointRouteAssigned);
                            }
                        });
        mSnackbar.show();
    }

    /**
     * Carga los markers al mapa
     */
    private void loadMarkers() {
        int i = 0;

        for (SellingPointRouteModel sellingPointRouteAssigned : lstSellingPointRoutesAssigned) {
            Marker marker = mGoogleMap.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(
                                    sellingPointRouteAssigned.getPDV().getLatitud(),
                                    sellingPointRouteAssigned.getPDV().getLongitud()
                            ))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            );
            marker.setTag(new MarkerTag(
                    i++,
                    ASSIGNED
            ));
        }

        i = 0;
        for (SellingPointRouteModel sellingPointRouteUnassigned : lstSellingPointRoutesUnassigned) {
            Marker marker = mGoogleMap.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(
                                    sellingPointRouteUnassigned.getPDV().getLatitud(),
                                    sellingPointRouteUnassigned.getPDV().getLongitud()
                            ))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            );
            marker.setTag(new MarkerTag(
                    i++,
                    UNASSIGNED
            ));
        }
    }

    /**
     * Pojo para setear en el marker, para asi identificar si pertenece a un pdv
     * asignado a la ruta o no
     */
    public class MarkerTag {

        private int position;
        private String state;

        public MarkerTag(int position, String state) {
            this.position = position;
            this.state = state;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}
