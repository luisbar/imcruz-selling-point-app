package com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation;

import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ScrollView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.home.HomeModel;
import com.imcruzparts.pdv.presentation.home.presentation.client.ClientView;
import com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation.ClientInformationView;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.greenrobot.eventbus.Subscribe;

import java.util.Arrays;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class BusinessInformationView extends Fragment implements Step, OnMapReadyCallback, GoogleMap.OnMapClickListener, ClientInformationView.ClientInformationReceiver, ClientView.OnAddClientListener, GoogleMap.OnMyLocationButtonClickListener {

    @BindView(R.id.input_business_information_name)
    EditText inputBusinessInformationName;
    @BindView(R.id.input_business_information_state)
    AutoCompleteTextView inputBusinessInformationState;
    @BindView(R.id.input_business_information_address)
    EditText inputBusinessInformationAddress;
    @BindView(R.id.mapview_business_information)
    MapView mapviewBusinessInformation;

    @BindString(R.string.txt_37)
    String txt37;
    @BindString(R.string.txt_38)
    String txt38;
    @BindString(R.string.txt_39)
    String txt39;
    @BindView(R.id.scroll_business_information)
    ScrollView scrollBusinessInformation;

    private Unbinder mUnbinder;
    private GoogleMap mGoogleMap;
    private BusinessInformationPresenter mBusinessInformationPresenter;
    private MainActivity mMainActivity;
    private ClientView mClientView;

    private boolean mNameIsValid;
    private boolean mStateIsValid;
    private boolean mAddressIsValid;
    private LatLng mLocation;

    private String mFirstName;
    private String mLastName;
    private String mDocumentNumber;
    private String mCellPhoneNumber;
    private String mPhoneNumber;
    private String mEmail;
    private String mSocialReason;
    private String mNit;
    private HomeModel mHomeModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_business_information, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mBusinessInformationPresenter = new BusinessInformationPresenter();
        mClientView = (ClientView) mMainActivity.getSupportFragmentManager().findFragmentByTag("ClientView");

        MapsInitializer.initialize(mMainActivity);
        mapviewBusinessInformation.onCreate(savedInstanceState);
        mapviewBusinessInformation.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadDataForUpdating();

        inputBusinessInformationState.setAdapter(new ArrayAdapter(
                this.getActivity(),
                android.R.layout.simple_list_item_1,
                Arrays.asList(
                        "Santa Cruz",
                        "Sucre",
                        "La Paz",
                        "Pando",
                        "Beni",
                        "Oruro",
                        "Potosi",
                        "Cochabamba",
                        "Tarija"
                )
        ));
    }

    @Override
    public void onStart() {
        super.onStart();
        mapviewBusinessInformation.onStart();
        EventBusMask.register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapviewBusinessInformation.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapviewBusinessInformation.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapviewBusinessInformation.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapviewBusinessInformation.onLowMemory();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setOnMyLocationButtonClickListener(this);

        if (mLocation != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mLocation, 10);
            mGoogleMap.animateCamera(cameraUpdate);
            mGoogleMap.addMarker(new MarkerOptions().position(mLocation));
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mLocation = latLng;
        mGoogleMap.clear();
        mGoogleMap.addMarker(new MarkerOptions().position(latLng));
        showOrHideAddClientItemMenu();
    }

    @OnTextChanged({
            R.id.input_business_information_name,
            R.id.input_business_information_state,
            R.id.input_business_information_address
    })
    public void onTextChanged() {
        if (inputBusinessInformationName.isFocused())
            mBusinessInformationPresenter.validateName(inputBusinessInformationName.getText().toString());
        if (inputBusinessInformationState.isFocused())
            mBusinessInformationPresenter.validateState(inputBusinessInformationState.getText().toString());
        if (inputBusinessInformationAddress.isFocused())
            mBusinessInformationPresenter.validateAddress(inputBusinessInformationAddress.getText().toString());
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.VALIDATED_NAME:
                mNameIsValid = (boolean) action.getPayload();

                if (!mNameIsValid) inputBusinessInformationName.setError(txt37);
                break;

            case ActionType.VALIDATED_STATE:
                mStateIsValid = (boolean) action.getPayload();

                if (!mStateIsValid)
                    inputBusinessInformationState.setError(txt38);
                else
                    inputBusinessInformationState.setError(null);
                break;

            case ActionType.VALIDATED_ADDRESS:
                mAddressIsValid = (boolean) action.getPayload();

                if (!mAddressIsValid) inputBusinessInformationAddress.setError(txt39);
                break;
        }

        showOrHideAddClientItemMenu();
    }

    /**
     * Metodo que dispara un evento en {@link ClientView} para esconder
     * o mostrar el add client item del toolbar
     */
    public void showOrHideAddClientItemMenu() {
        if (mNameIsValid && mStateIsValid && mAddressIsValid && mLocation != null)
            mClientView.showAddClientItemMenu(true);
        else
            mClientView.showAddClientItemMenu(false);
    }

    /**
     * Escuchador para recibir los datos del cliente desde {@link ClientInformationView}
     *
     * @param firstName       nombres del cliente
     * @param lastName        apellidos del cliente
     * @param documentNumber  numero de documento del cliente
     * @param cellPhoneNumber numero de celular del cliente
     * @param phoneNumber     numero de telefono fijo del cliente
     * @param email           correo del cliente
     * @param socialReason    razon social del cliente
     * @param nit             nit del cliente
     */
    @Override
    public void setClientInformation(String firstName, String lastName, String documentNumber,
                                     String cellPhoneNumber, String phoneNumber, String email,
                                     String socialReason, String nit) {

        mFirstName = firstName;
        mLastName = lastName;
        mDocumentNumber = documentNumber;
        mCellPhoneNumber = cellPhoneNumber;
        mPhoneNumber = phoneNumber;
        mEmail = email;
        mSocialReason = socialReason;
        mNit = nit;
    }

    /**
     * Esuchador cuando se a presionado el add client item del toolbar
     * de {@link ClientView}, y ejecuda el metodo
     * {@link ClientView#addClient(String, String, String, String, String, String, String, String, String, String, String, double, double)}
     */
    @Override
    public void saveNewClient() {
        mClientView.addClient(mFirstName, mLastName, mDocumentNumber, mCellPhoneNumber,
                mPhoneNumber, mEmail, mSocialReason, mNit,
                inputBusinessInformationName.getText().toString(),
                inputBusinessInformationState.getText().toString(),
                inputBusinessInformationAddress.getText().toString(),
                mLocation.latitude,
                mLocation.longitude);
    }

    /**
     * Esuchador cuando se a presionado el add client item del toolbar
     * de {@link ClientView}, y ejecuda el metodo
     * {@link ClientView#updateClient(int, String, String, String, String, String, String, String, String, int, String, String, String, double, double)}
     */
    @Override
    public void updateClient() {
        mClientView.updateClient(mHomeModel.getClient().get_id(), mFirstName, mLastName, mDocumentNumber, mCellPhoneNumber,
                mPhoneNumber, mEmail, mSocialReason, mNit, mHomeModel.get_id(),
                inputBusinessInformationName.getText().toString(),
                inputBusinessInformationState.getText().toString(),
                inputBusinessInformationAddress.getText().toString(),
                mLocation.latitude,
                mLocation.longitude);
    }

    /**
     * Esuchador que se dispara cuando se a guardado un cliente,
     * de {@link ClientView}, restaura la vista a su
     * estado inicial
     */
    @Override
    public void clearBusinessInformationView() {
        inputBusinessInformationName.getText().clear();
        inputBusinessInformationState.getText().clear();
        inputBusinessInformationAddress.getText().clear();
        mGoogleMap.clear();

        mNameIsValid = false;
        mStateIsValid = false;
        mAddressIsValid = false;
        mLocation = null;

        inputBusinessInformationName.requestFocus();
        scrollBusinessInformation.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void clearClientInformationView() {
        // TODO: nada
    }

    @Override
    public void loadBusinessInformation(HomeModel homeModel) {
        mHomeModel = homeModel;

        inputBusinessInformationName.setText(homeModel.getName());
        inputBusinessInformationState.setText(homeModel.getCountry());
        inputBusinessInformationAddress.setText(homeModel.getAddress());

        mLocation = new LatLng(homeModel.getLatitude(), homeModel.getLongitude());

        mNameIsValid = true;
        mStateIsValid = true;
        mAddressIsValid = true;

        inputBusinessInformationName.requestFocus();
        showOrHideAddClientItemMenu();
    }

    @Override
    public void loadClientInformation(HomeModel.ClientModel clientModel) {
        // TODo: nada
    }

    /**
     * Metodo que ejecuta el metodo {@link ClientView#loadDataForBusinessInformationView()}
     */
    public void loadDataForUpdating() {
        mClientView.loadDataForBusinessInformationView();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        LocationManager locManager = (LocationManager) mMainActivity.getSystemService(LOCATION_SERVICE);
        if (!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mMainActivity.showSnackBar("El GPS no está activado.", "Activar", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(
                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
        } else {
            mMainActivity.showSnackBar("Estableciendo ubicación...");
        }
        return false;
    }
}
