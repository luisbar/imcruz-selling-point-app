package com.imcruzparts.pdv.presentation.home.presentation.client.util;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation.BusinessInformationView;
import com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation.ClientInformationView;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Adapter para el componente {@link com.stepstone.stepper.StepperLayout}, es practicamente
 * un viewpager
 */
public class StepperAdapter extends AbstractFragmentStepAdapter {

    private BusinessInformationView mBusinessInformationView;
    private ClientInformationView mClientInformationView;

    public StepperAdapter(FragmentManager fragmentManager, Context context) {
        super(fragmentManager, context);
    }

    @Override
    public Step createStep(int position) {

        switch (position) {

            case 0:
                mClientInformationView = new ClientInformationView();
                return mClientInformationView;

            case 1:
                mBusinessInformationView = new BusinessInformationView();
                return mBusinessInformationView;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        return new StepViewModel.Builder(context)
                .setTitle(position == 0 ? R.string.txt_13 : R.string.txt_14)
                .create();
    }

    public BusinessInformationView getmBusinessInformationView() {
        return mBusinessInformationView;
    }

    public ClientInformationView getmClientInformationView() {
        return mClientInformationView;
    }
}
