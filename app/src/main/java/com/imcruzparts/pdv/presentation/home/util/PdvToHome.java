package com.imcruzparts.pdv.presentation.home.util;

import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.presentation.home.HomeModel;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Convierte objeto {@link List<PdvModelLocal>} a {@link List<HomeModel>}
 * para la vista {@link com.imcruzparts.pdv.presentation.home.HomeView}
 */
public class PdvToHome implements Mapper<List<PdvModelLocal>, List<HomeModel>> {

    @Override
    public List<HomeModel> map(List<PdvModelLocal> pdvModelLocals) {
        List<HomeModel> lstHome = new LinkedList();
        Iterator<PdvModelLocal> iterator = pdvModelLocals.iterator();

        while (iterator.hasNext()) {
            PdvModelLocal pdv = iterator.next();

            lstHome.add(
                    new HomeModel(
                            pdv.getIdPDV(),
                            pdv.getNombre(),
                            pdv.getDepartamento(),
                            pdv.getDireccion(),
                            pdv.getLatitud(),
                            pdv.getLongitud(),
                            HomeModel.ClientModel.constructor(
                                    pdv.getIdCliente().getIdCliente(),
                                    pdv.getIdCliente().getNombre(),
                                    pdv.getIdCliente().getApellido(),
                                    pdv.getIdCliente().getNroDocumento(),
                                    pdv.getIdCliente().getCelular(),
                                    pdv.getIdCliente().getTelefono(),
                                    pdv.getIdCliente().getEmail(),
                                    pdv.getIdCliente().getRazonSocial(),
                                    pdv.getIdCliente().getNit())
                    )
            );
        }

        return lstHome;
    }
}
