package com.imcruzparts.pdv.presentation.visit.presentation.activity;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.visit.VisitView;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.camera.CameraView;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestModel;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestView;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.util.ActivityAdapter;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.util.RequestMadeAdapter;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ActivityView extends Fragment implements OnMapReadyCallback,
        SlidingUpPanelLayout.PanelSlideListener,
        ActivityAdapter.OnItemPressed, RequestMadeAdapter.OnRequestMadePressed,
        RequestView.OnRequestAdded {

    @BindDrawable(R.mipmap.ic_menu)
    Drawable menu;

    @BindString(R.string.txt_11)
    String txt11;
    @BindString(R.string.txt_92)
    String txt92;
    @BindString(R.string.txt_93)
    String txt93;
    @BindString(R.string.txt_94)
    String txt94;
    @BindString(R.string.txt_95)
    String txt95;
    @BindString(R.string.txt_102)
    String txt102;
    @BindString(R.string.txt_103)
    String txt103;
    @BindString(R.string.txt_104)
    String txt104;
    @BindString(R.string.txt_105)
    String txt105;
    @BindString(R.string.txt_107)
    String txt107;
    @BindString(R.string.txt_108)
    String txt108;
    @BindString(R.string.txt_109)
    String txt109;
    @BindString(R.string.txt_110)
    String txt110;

    @BindColor(R.color.myWindowBackground)
    int imageBackground;

    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.icon_name)
    ImageView iconName;
    @BindView(R.id.text_client_name)
    TextView textClientName;
    @BindView(R.id.text_business_name)
    TextView textBusinessName;
    @BindView(R.id.icon_expanded_collapsed)
    ToggleButton iconExpandedCollapsed;
    @BindView(R.id.mapview_visit)
    com.google.android.gms.maps.MapView mapviewVisit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_slider)
    Toolbar toolbarSlider;
    @BindView(R.id.ll_text)
    LinearLayout llText;
    @BindView(R.id.rv_activities)
    RecyclerView rvActivities;
    @BindView(R.id.rv_requests)
    RecyclerView rvRequest;

    private Unbinder mUnbinder;
    private MainActivity mMainActivity;
    private List<ActivityModel> lstActivities;
    private ActivityAdapter mActivityAdapter;
    private ActivityPresenter mActivityPresenter;
    private List<RequestModel> lstRequest;
    private RequestMadeAdapter mRequestMadeAdapter;

    private int positionPdvSelected = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(true);
        mActivityPresenter = new ActivityPresenter();

        lstActivities = (List<ActivityModel>) getArguments().getSerializable("Activities");
        mActivityAdapter = new ActivityAdapter(new ArrayList(), this);

        lstRequest = (List<RequestModel>) getArguments().getSerializable("Requests");
        mRequestMadeAdapter = new RequestMadeAdapter(new ArrayList(), this);

        MapsInitializer.initialize(mMainActivity);
        mapviewVisit.onCreate(savedInstanceState);
        mapviewVisit.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt11);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(menu);

        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        slidingLayout.addPanelSlideListener(this);
        slidingLayout.setTouchEnabled(false);
        iconExpandedCollapsed.setTextOff("");
        iconExpandedCollapsed.setTextOn("");
        iconExpandedCollapsed.setText("");

        rvActivities.setAdapter(mActivityAdapter);
        rvActivities.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));

        rvRequest.setAdapter(mRequestMadeAdapter);
        rvRequest.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));

        loadSlider();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapviewVisit.onStart();
        EventBusMask.register(this);
        mActivityPresenter.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapviewVisit.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapviewVisit.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mActivityPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapviewVisit != null)
            mapviewVisit.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapviewVisit.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Esta poniendo 5 marker por pdvs, corregir eso
        int i = 0;
        for (ActivityModel activitie : lstActivities) {
            Marker marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(
                            activitie.getSellingPoint().getLatitude(),
                            activitie.getSellingPoint().getLongitude())
                    ));

            marker.setTag(i++);
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-17.7832433,
                -63.182739), 10));
        // Escuchador para los markers
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                positionPdvSelected = Integer.valueOf(marker.getTag().toString());
                loadSlider();

                if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    iconExpandedCollapsed.setChecked(false);
                }

                return false;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_activity, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.openDrawer();
                break;

            case R.id.action_finalize_seller_route:
                showSnackbar();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.icon_expanded_collapsed)
    public void onViewClicked() {
        if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        else
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {

    }

    @Override
    public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
        if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED)
            iconExpandedCollapsed.setChecked(false);
        else
            iconExpandedCollapsed.setChecked(true);
    }

    @Override
    public void itemPressed(ActivityModel activity) {
        Bundle args;

        switch (activity.getValue()) {

            case "FOTO":
                Fragment cameraView = new CameraView();
                args = new Bundle();

                args.putInt("idActivity", activity.getId());
                cameraView.setArguments(args);

                mMainActivity.loadFragmentAndAddedToStack(R.id.container, cameraView);
                break;

            case "PEDIDO":
                Fragment requestView = new RequestView();
                args = new Bundle();

                args.putInt("idActivity", activity.getId().intValue());
                args.putInt("idSellingPoint", activity.getSellingPoint().getId().intValue());
                requestView.setArguments(args);

                mMainActivity.loadFragmentAndAddedToStack(R.id.container, requestView, "RequestView");
                break;
        }
    }

    @Override
    public void checkBoxPressed(ActivityModel activity) {
        mActivityPresenter.finalizeActivity(activity.getId());
    }

    @Override
    public void noteAdded(ActivityModel activity, String note) {
        mActivityPresenter.addNote(activity.getId(), note);
    }

    @Override
    public void requestMadePressed(RequestModel request) {
        Fragment requestView = new RequestView();
        Bundle args = new Bundle();

        args.putSerializable("Request", request);
        requestView.setArguments(args);

        mMainActivity.loadFragmentAndAddedToStack(R.id.container, requestView, "RequestView");
    }

    @Override
    public void checkBoxPressed(RequestModel request) {
        mActivityPresenter.deliverRequest(request.getIdRequest());
    }

    @Override
    public void noteAdded(RequestModel request, String note) {
        mActivityPresenter.addNoteToRequest(request.getIdRequest(), note);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.ACTIVITY_FINALIZED_V:
                mMainActivity.showSnackBar(txt92);
                mActivityAdapter.getmCheckActivity().setChecked(true);
                mActivityAdapter.getLstActivities().get(mActivityAdapter.getPositionFinalizedActivity())
                        .setState("FINALIZADA");
                break;

            case ActionType.ACTIVITY_NOT_FINALIZED_V:
                mMainActivity.showSnackBar(txt93);
                break;

            case ActionType.NOTE_ADDED_V:
                mMainActivity.showSnackBar(txt94);
                break;

            case ActionType.NOTE_NOT_ADDED_V:
                mMainActivity.showSnackBar(txt95);
                break;

            case ActionType.REQUEST_DELIVERED_V:
                mMainActivity.showSnackBar(txt102);
                mRequestMadeAdapter.getmCheckRequestMade().setChecked(true);
                mRequestMadeAdapter.getLstRequest().get(mRequestMadeAdapter.getPositionDeliveredRequest())
                        .setState("ENTREGADO");
                break;

            case ActionType.REQUEST_NOT_DELIVERED_V:
                mMainActivity.showSnackBar(txt103);
                break;

            case ActionType.NOTE_ADDED_TO_REQUEST_V:
                mMainActivity.showSnackBar(txt104);
                break;

            case ActionType.NOTE_NOT_ADDED_TO_REQUEST_V:
                mMainActivity.showSnackBar(txt105);
                break;

            case ActionType.SELLER_ROUTE_FINALIZE_V:
                if (action.isError())
                    mMainActivity.showSnackBar(txt107);
                else {
                    mMainActivity.showSnackBar(txt108);
                    mMainActivity.loadFragment(R.id.container, new VisitView());
                }
                break;
        }
    }

    /**
     * Obtiene las actividades correspondientes a un pdv y que visible == true
     * @param idSellingPoint identificador de pdv
     * @return lista de actividades filtrada
     */
    private List<ActivityModel> getActivitiesWithSellingPointId(int idSellingPoint) {
        List<ActivityModel> lstActivitiesFiltered = new ArrayList();

        for (ActivityModel activity : lstActivities) {
            if (activity.getSellingPoint().getId().intValue() == idSellingPoint
                    && activity.getVisible().booleanValue() == true)
                lstActivitiesFiltered.add(activity);
        }

        return lstActivitiesFiltered;
    }

    /**
     * Obtiene los pedidos por id de pdv
     * @param idSellingPoint id pdv
     * @return
     */
    private List<RequestModel> getRequestsMadeWithSellingPoint(int idSellingPoint) {
        List<RequestModel> lstRequestMadeFiltered = new ArrayList();

        if (lstRequest != null) {
            for (RequestModel requestModel : lstRequest) {
                if (requestModel.getIdSellingPoint().intValue() == idSellingPoint)
                    lstRequestMadeFiltered.add(requestModel);
            }
        }

        return lstRequestMadeFiltered;
    }

    /**
     * Carga el slider con datos
     */
    private void loadSlider() {
        if (positionPdvSelected != -1) {
            ActivityModel activityModel = lstActivities.get(positionPdvSelected);

            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig().textColor(ColorGenerator.MATERIAL.getColor("A"))
                    .useFont(Typeface.DEFAULT)
                    .toUpperCase()
                    .endConfig()
                    .buildRound(
                            activityModel.getClient().getFirstNameClient().substring(0, 1),
                            imageBackground);

            iconName.setImageDrawable(drawable);
            textClientName.setText(activityModel.getClient().getFirstNameClient() + " "
                    + activityModel.getClient().getLastNameClient());
            textBusinessName.setText(activityModel.getSellingPoint().getName());

            mActivityAdapter = new ActivityAdapter(getActivitiesWithSellingPointId(activityModel.getSellingPoint().getId()), ActivityView.this);
            rvActivities.setAdapter(mActivityAdapter);

            mRequestMadeAdapter = new RequestMadeAdapter(getRequestsMadeWithSellingPoint(activityModel.getSellingPoint().getId()), ActivityView.this);
            rvRequest.setAdapter(mRequestMadeAdapter);
        }
    }

    /**
     * Esuchador para cuando se agrega un nuevo pedido, es disparado por RequestView
     * @param requestAdded
     */
    @Override
    public void requestAdded(RequestModel requestAdded) {
        lstRequest.add(requestAdded);

        mRequestMadeAdapter = new RequestMadeAdapter(getRequestsMadeWithSellingPoint(requestAdded.getIdSellingPoint()), this);
    }

    /**
     * Muestra snackbar para confirmación de que se quiere finalizar la ruta
     * de vendedor actualmente iniciada
     */
    private void showSnackbar() {
        View rootView = mMainActivity.findViewById(R.id.container);

        Snackbar.make(rootView, txt109, Snackbar.LENGTH_INDEFINITE)
                .setAction(txt110, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mActivityPresenter.finalizeSellerRoute(lstActivities.get(0).getSellerRoute().getId());
                    }
                })
                .show();
    }
}
