package com.imcruzparts.pdv.presentation.product;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.product.presentation.detail.DetailView;
import com.imcruzparts.pdv.presentation.product.util.FilterObject;
import com.imcruzparts.pdv.presentation.product.util.FilterSuggestionAdapter;
import com.imcruzparts.pdv.presentation.product.util.ProductAdapter;
import com.imcruzparts.pdv.presentation.util.DividerDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)},
 * esta vista es utilizada para el catalogo si getArguments == null
 * y para agregar un producto a un pedido si getArguments != null
 */
public class ProductView extends Fragment implements ProductAdapter.OnItemClicked {

    @BindView(R.id.layout_product_filter)
    LinearLayout layoutProductFilter;
    @BindView(R.id.rv_product)
    RecyclerView rvProduct;

    @BindString(R.string.txt_62)
    String txt62;
    @BindString(R.string.txt_63)
    String txt63;
    @BindString(R.string.txt_10)
    String txt10;
    @BindString(R.string.txt_43)
    String txt43;
    @BindString(R.string.txt_47)
    String txt47;

    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable iconClose;
    @BindDrawable(R.mipmap.ic_menu)
    Drawable iconMenu;

    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.empty_image)
    ImageView emptyIcon;
    @BindView(R.id.empty_title)
    TextView emptyTitle;
    @BindView(R.id.empty_description)
    TextView emptyDescription;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @BindView(R.id.layout_product_main)
    CoordinatorLayout layoutProductMain;

    private Unbinder mUnbinder;
    private MainActivity mMainActivity;
    private ProductPresenter mProductPresenter;
    private ProductAdapter mProductAdapter;

    private String mType;
    private String mBrand;
    private String mSegment;
    private String mFilter;
    private List<FilterObject> lstFilterObjects;
    private List<FilterObject> lstTypes;


    private FilterSuggestionAdapter mFilterSuggestionAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mProductAdapter = new ProductAdapter(new ArrayList(), this);

        mProductPresenter = new ProductPresenter();

        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEmptyView();
        rvProduct.setAdapter(mProductAdapter);
        rvProduct.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));
        // Agrega los headers de decoracion al recyclerview
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(mProductAdapter);
        rvProduct.addItemDecoration(headersDecor);
        // Agrega divisores entre los items del recyclerview
        rvProduct.addItemDecoration(new DividerDecoration(getActivity()));
        mProductAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                headersDecor.invalidateHeaders();
            }
        });

        showToolbar();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mProductPresenter.onStart();
        mProductPresenter.getProducts();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mProductPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_product, menu);
        super.onCreateOptionsMenu(menu, inflater);

        setSearchView(menu);
        setFilterView(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                if (getArguments() != null)
                    mMainActivity.onBackPressed();
                else
                    mMainActivity.openDrawer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Escuchador cuando presiona un item del recycler view, es para mostrar
     * el detalle de producto
     * @param productModel datos del producto seleccionado
     */
    @Override
    public void onItemClicked(ProductModel productModel) {
        if (getArguments() == null) {
            Fragment productDetailFragment = new DetailView();
            Bundle args = new Bundle();

            args.putSerializable("Product", productModel);
            productDetailFragment.setArguments(args);

            mMainActivity.loadFragmentAndAddedToStack(R.id.container, productDetailFragment);
        } else {
            mMainActivity.onBackPressed();
            OnProductSelected mOnProductSelected = (OnProductSelected) mMainActivity.getSupportFragmentManager().findFragmentByTag("RequestView");
            mOnProductSelected.productSelected(productModel);
        }
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.PRODUCTS_OBTAINED_V:

                mProductAdapter.setProducts((List<ProductModel>) action.getPayload());
                mProductAdapter.notifyDataSetChanged();
                loadFilterSaved();
                break;

            case ActionType.TYPES_OBTAINED_V:


                lstFilterObjects = lstFilterObjects == null
                        ? (List<FilterObject>) action.getPayload()
                        : lstFilterObjects;
                lstTypes = (List<FilterObject>) action.getPayload();
                loadData();
                break;
        }
    }

    private void setEmptyView() {
        emptyTitle.setText(txt62);
        emptyDescription.setText(txt63);
        emptyIcon.setImageDrawable(mMainActivity.getDrawable(R.drawable.alert_circle_outline));
        mProductAdapter.setEmptyView(emptyView);
    }

    /**
     * Crea el nuevo tag para agregarlo al contenedor de tags, el cual es
     * un linear layout
     * @param position posicion que ocupara el nuevo tag, si 0 = tipo, 1 = marca y 2 = segmento
     * @return view que representa un tag en la vista
     */
    public View createTagAndSetListener(final int position) {
        View tag = ProductView.this.mMainActivity.getLayoutInflater().inflate(R.layout.item_tag_filter, null);
        TextView text = (TextView) tag.findViewById(R.id.text_filter);
        ImageView closeIcon = (ImageView) tag.findViewById(R.id.icon_close);

        text.setText(mFilter);
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == layoutProductFilter.getChildCount() - 1) {
                    layoutProductFilter.removeViewAt(position);
                    updateRecyclerAndFilterView(true);
                }
            }
        });

        return tag;
    }

    /**
     * Funcion que actualiza el recyclerview y el filterView, ya sea cuando se agrega
     * un tag o se remueve uno
     * @param delete bandera para saber si se ha eliminado o agregado un tag
     */
    public void updateRecyclerAndFilterView(boolean delete) {
        List aux = null;

        switch (layoutProductFilter.getChildCount()) {
            case 0:
                aux = Arrays.asList(mProductAdapter.getLstProductsNotFiltered(), lstTypes);
                mType = null;
                break;

            case 1:
                if (!delete)
                    mType = mFilter;
                else
                    mBrand = null;
                aux = mProductPresenter.filterByType(mProductAdapter.getLstProductsNotFiltered(), mType);
                break;

            case 2:
                if (!delete)
                    mBrand = mFilter;
                else
                    mSegment = null;
                aux = mProductPresenter.filterByTypeAndBrand(mProductAdapter.getLstProductsNotFiltered(), mType, mBrand);
                break;

            case 3:
                if (!delete)
                    mSegment = mFilter;
                aux = mProductPresenter.filterByTypeBrandAndSegment(mProductAdapter.getLstProductsNotFiltered(), mType, mBrand, mSegment);
                break;
        }

        mProductAdapter.setFilteredClients((List<ProductModel>) aux.get(0));
        lstFilterObjects = (List<FilterObject>) aux.get(1);
        loadData();
    }

    /**
     * Si getArguments != null muestra el boton de cerrar vista
     * caso contrario muestra el boton para abrir el drawer
     */
    public void showToolbar() {
        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt10);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getArguments() != null) {
            mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconClose);
            mMainActivity.enableDrawer(false);
        } else {
            mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconMenu);
            mMainActivity.enableDrawer(true);
        }
    }

    /**
     * Configura el searchView
     * @param menu objeto de tipo menu
     */
    private void setSearchView(final Menu menu) {
        MenuItem item = menu.findItem(R.id.action_product_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        final AutoCompleteTextView filterAutoCompleteTextView;
        filterAutoCompleteTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        filterAutoCompleteTextView.setHint(txt43);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<ProductModel> lstFilteredProducts = mProductPresenter.filter(mProductAdapter.getLstProductsNotFiltered(), newText);
                mProductAdapter.setFilteredClients(lstFilteredProducts);
                return false;
            }
        });
        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                item = menu.findItem(R.id.action_product_filter);
                item.setVisible(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                item = menu.findItem(R.id.action_product_filter);
                item.setVisible(true);
                mProductAdapter.restoreAdapter();
                return true;
            }
        });
    }

    /**
     * Configura el filterView
     * @param menu objeto de tipo menu
     */
    private void setFilterView(final Menu menu) {
        MenuItem item = menu.findItem(R.id.action_product_filter);
        final SearchView filterView = (SearchView) MenuItemCompat.getActionView(item);

        final AutoCompleteTextView filterAutoCompleteTextView;
        filterAutoCompleteTextView = (AutoCompleteTextView) filterView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        filterAutoCompleteTextView.setThreshold(0);
        filterAutoCompleteTextView.setHint(txt47);


        filterView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (layoutProductFilter.getChildCount() < 3) {
                    mFilter = query;

                    layoutProductFilter.addView(createTagAndSetListener(layoutProductFilter.getChildCount()));
                    updateRecyclerAndFilterView(false);

                    filterAutoCompleteTextView.getText().clear();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mFilter = newText;
                loadData(newText);
                return false;
            }
        });

        filterView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                if (layoutProductFilter.getChildCount() < 3) {
                    Cursor cursor = (Cursor) filterView.getSuggestionsAdapter().getItem(position);
                    mFilter = cursor.getString(1);

                    layoutProductFilter.addView(createTagAndSetListener(layoutProductFilter.getChildCount()));
                    updateRecyclerAndFilterView(false);

                    filterAutoCompleteTextView.getText().clear();
                }
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                if (layoutProductFilter.getChildCount() < 3) {
                    Cursor cursor = (Cursor) filterView.getSuggestionsAdapter().getItem(position);
                    mFilter = cursor.getString(1);

                    layoutProductFilter.addView(createTagAndSetListener(layoutProductFilter.getChildCount()));
                    updateRecyclerAndFilterView(false);

                    filterAutoCompleteTextView.getText().clear();
                }
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                mProductPresenter.getTypes();
                item = menu.findItem(R.id.action_product_search);
                item.setVisible(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                item = menu.findItem(R.id.action_product_search);
                item.setVisible(true);
                return true;
            }
        });

        String[] columns = new String[]{"_id", "value",};
        mFilterSuggestionAdapter = new FilterSuggestionAdapter(this.getActivity(), R.layout.item_filter, null, columns,null, -1000);
        filterView.setSuggestionsAdapter(mFilterSuggestionAdapter);
    }

    /**
     * Carga datos al filterView, pero antes los filtra
     * de acuerdo a lo que el usuario introdujo
     * @param filterText
     */
    private void loadData(String filterText) {

        MatrixCursor matrixCursor = convertToCursor(filterText, lstFilterObjects);
        mFilterSuggestionAdapter.changeCursor(matrixCursor);
        mFilterSuggestionAdapter.notifyDataSetChanged();
    }

    /**
     * Carga datos al filterView
     */
    private void loadData() {
        MatrixCursor matrixCursor = convertToCursor(lstFilterObjects);
        mFilterSuggestionAdapter.changeCursor(matrixCursor);
        mFilterSuggestionAdapter.notifyDataSetChanged();
    }

    /**
     * Convierte la lista de filterObjects a tipo matrixCursor,
     * pero antes matchea con lo que el usuario introdujo
     * @param filterText texto introducido por el usuario
     * @param filterObjects lista de filterObjects desplegada por el filterView
     * @return objeto matrixCursor con los datos filtrados
     */
    private MatrixCursor convertToCursor(String filterText, List<FilterObject> filterObjects) {
        String[] columns = new String[]{"_id", "value"};

        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;

        for (FilterObject filterObject : filterObjects) {
            if (filterObject.getValue().toLowerCase().contains(filterText)) {
                String[] temp = new String[2];

                temp[0] = i++ + "";
                temp[1] = filterObject.getValue();

                cursor.addRow(temp);
            }
        }
        return cursor;
    }

    /**
     * Convierte la lista de filterObjects a tipo matrixCursor
     * @param filterObjects lista a convertir a matrixCursor
     * @return lista transformada a matrixCursor
     */
    private MatrixCursor convertToCursor(List<FilterObject> filterObjects) {
        String[] columns = new String[]{"_id", "value"};

        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;

        for (FilterObject filterObject : filterObjects) {
            String[] temp = new String[2];

            temp[0] = i++ + "";
            temp[1] = filterObject.getValue();

            cursor.addRow(temp);
        }
        return cursor;
    }

    /**
     * Carga el filtro si es que se filtro antes de ir a la vista de
     * detalle de producto
     */
    private void loadFilterSaved() {
        layoutProductFilter.removeAllViews();
        if (mType != null) {
            mFilter = mType;
            layoutProductFilter.addView(createTagAndSetListener(layoutProductFilter.getChildCount()));
            updateRecyclerAndFilterView(false);
        }

        if (mBrand != null) {
            mFilter = mBrand;
            layoutProductFilter.addView(createTagAndSetListener(layoutProductFilter.getChildCount()));
            updateRecyclerAndFilterView(false);
        }

        if (mSegment != null) {
            mFilter = mSegment;
            layoutProductFilter.addView(createTagAndSetListener(layoutProductFilter.getChildCount()));
            updateRecyclerAndFilterView(false);
        }
    }

    /**
     * Interface que dispara evento en {@link com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.RequestView}
     * cuando se presion sobre un item del recyclerview, es ejecutado en el {@link #onItemClicked(ProductModel)}
     */
    public interface OnProductSelected {

        void productSelected(ProductModel productModel);
    }
}