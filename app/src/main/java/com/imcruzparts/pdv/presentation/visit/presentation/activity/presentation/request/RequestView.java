package com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.product.ProductModel;
import com.imcruzparts.pdv.presentation.product.ProductView;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request.util.RequestAdapter;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)},
 * esta vista es utilzada para hacer un nuevo pedido, si getArguments().getSerializable("Request") == null,
 * caso contrario es pa mostrar un pedido ya realizado
 */
public class RequestView extends Fragment implements ProductView.OnProductSelected
        , RequestAdapter.OnQuantityChanged {

    @BindView(R.id.rv_request_detail)
    RecyclerView rvRequestDetail;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.text_request_subtotal)
    TextView textRequestSubtotal;
    @BindView(R.id.text_request_discount)
    TextView textRequestDiscount;
    @BindView(R.id.text_request_total)
    TextView textRequestTotal;
    @BindView(R.id.fab_request)
    FloatingActionButton fabRequest;

    @BindString(R.string.txt_64)
    String txt64;
    @BindString(R.string.txt_97)
    String txt97;
    @BindString(R.string.txt_98)
    String txt98;


    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable iconClose;

    private MainActivity mMainActivity;
    private RequestAdapter mRequestAdapter;
    private Unbinder mUnbinder;
    private RequestPresenter mRequestPresenter;
    private int idActivity;
    private int idSellingPoint;

    private boolean mShowAddRequestItemMenu;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);
        mRequestPresenter = new RequestPresenter();
        idActivity = getArguments() != null ? getArguments().getInt("idActivity") : -1;
        idSellingPoint = getArguments() != null ? getArguments().getInt("idSellingPoint") : -1;

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt64);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconClose);

        if (mRequestAdapter != null) {
            rvRequestDetail.setAdapter(mRequestAdapter);
            rvRequestDetail.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));

            loadFooter();
        } else if (getArguments().getSerializable("Request") != null) {

            fabRequest.setVisibility(View.GONE);
            loadRecycler();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mRequestPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mRequestPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
        mMainActivity.setmRequestAdapter(mRequestAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_request, menu);
        menu.getItem(0).setVisible(mShowAddRequestItemMenu && getArguments().getSerializable("Request") == null
                ? true
                : false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.onBackPressed();
                break;

            case R.id.action_add_request:
                mRequestPresenter.addRequest(idSellingPoint, idActivity,
                        Double.valueOf(textRequestSubtotal.getText().toString()),
                        Double.valueOf(textRequestDiscount.getText().toString()),
                        Double.valueOf(textRequestTotal.getText().toString()),
                        getProductIds(),
                        getQuantityByProduct());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab_request)
    public void onClick() {
        Fragment productFragment = new ProductView();
        Bundle args = new Bundle();

        args.putBoolean("Request", true);
        productFragment.setArguments(args);

        mMainActivity.loadFragmentAndAddedToStack(R.id.container, productFragment);
    }

    @Override
    public void productSelected(ProductModel productModel) {
        mRequestAdapter = mMainActivity.getmRequestAdapter() != null
                ? (RequestAdapter) mMainActivity.getmRequestAdapter()
                : new RequestAdapter(this, true);

        mRequestAdapter.addProduct(productModel);
    }

    /**
     * Escuchador disparado por {@link RequestAdapter}
     */
    @Override
    public void quantityChanged() {
        loadFooter();
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.REQUEST_ADDED_V:
                mMainActivity.showSnackBar(txt97);
                mRequestAdapter = new RequestAdapter(this, true);
                rvRequestDetail.setAdapter(mRequestAdapter);
                loadFooter();

                OnRequestAdded onRequestAdded = (OnRequestAdded) mMainActivity.getSupportFragmentManager().findFragmentByTag("ActivityView");
                onRequestAdded.requestAdded((RequestModel) action.getPayload());
                break;

            case ActionType.REQUEST_NOT_ADDED_V:
                mMainActivity.showSnackBar(txt98);
                break;
        }
    }

    /**
     * Muestra u oculta el add request item del menu en el toolbar
     * @param flag bandera para mostrar u ocultar el menu
     */
    public void showAddClientItemMenu(boolean flag) {
        mShowAddRequestItemMenu = flag;
        mMainActivity.invalidateOptionsMenu();
    }

    /**
     * Carga el footer con datos
     */
    private void loadFooter() {
        List result = mRequestPresenter.calculateTotal(mRequestAdapter.getLstProducts());
        textRequestSubtotal.setText(result.get(0).toString());
        textRequestDiscount.setText(result.get(1).toString());
        textRequestTotal.setText(result.get(2).toString());

        showAddClientItemMenu(mRequestAdapter.getLstProducts().size() > 0);
    }

    /**
     * Obtiene los ids de la lista de productos
     * @return lista de ids de productos
     */
    private List<Integer> getProductIds() {
        List<Integer> ids = new ArrayList();

        for (ProductModel product : mRequestAdapter.getLstProducts())
            ids.add(product.getProductId());

        return ids;
    }

    /**
     * Obtiene las cantidades por producto
     * @return lista de cantidad por producto
     */
    private List<Integer> getQuantityByProduct() {
        List<Integer> quantities = new ArrayList();

        for (ProductModel product : mRequestAdapter.getLstProducts())
            quantities.add(product.getQuantity());

        return quantities;
    }

    /**
     * Carga el recycler para visualizacion
     */
    private void loadRecycler() {
        RequestModel requestModel = (RequestModel) getArguments().getSerializable("Request");
        mRequestAdapter = new RequestAdapter(this, false);

        for (RequestModel.RequestDetail requestDetail : requestModel.getDetail()) {
            mRequestAdapter.addProduct(
                    new ProductModel(
                            requestDetail.getProduct().getProductId(),
                            requestDetail.getProduct().getCiaCode(),
                            requestDetail.getProduct().getProductCode(),
                            requestDetail.getProduct().getDescription(),
                            requestDetail.getProduct().getSegment(),
                            requestDetail.getProduct().getChannel(),
                            requestDetail.getProduct().getFabric(),
                            requestDetail.getProduct().getOrigin(),
                            requestDetail.getProduct().getFamily(),
                            requestDetail.getProduct().getBrand(),
                            requestDetail.getProduct().getGroup(),
                            requestDetail.getProduct().getType(),
                            requestDetail.getProduct().getPrice(),
                            requestDetail.getProduct().getDiscount(),
                            requestDetail.getQuantity(),
                            requestDetail.getSubTotal()
                    )
            );
        }

        rvRequestDetail.setAdapter(mRequestAdapter);
        rvRequestDetail.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));
    }

    /**
     * Interface para mostrar el nuevo pedido en el slider
     * del pdv correspondiente
     */
    public interface OnRequestAdded {

        void requestAdded(RequestModel requestAdded);
    }
}
