package com.imcruzparts.pdv.presentation.product.util;

import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.product.model.ProductModelLocal;
import com.imcruzparts.pdv.presentation.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

public class PmlToPm implements Mapper<List<ProductModelLocal>, List<ProductModel>> {

    @Override
    public List<ProductModel> map(List<ProductModelLocal> productModelLocals) {
        List<ProductModel> lstProductModels = new ArrayList();

        for (ProductModelLocal productModelLocal : productModelLocals)
            lstProductModels.add(new ProductModel(
                    productModelLocal.getIdProducto(),
                    productModelLocal.getCiaCod(),
                    productModelLocal.getCodProducto(),
                    productModelLocal.getDescripcion(),
                    productModelLocal.getSegmento(),
                    productModelLocal.getCanal(),
                    productModelLocal.getFabrica(),
                    productModelLocal.getOrigen(),
                    productModelLocal.getFamilia(),
                    productModelLocal.getMarca(),
                    productModelLocal.getGrupo(),
                    productModelLocal.getTipo(),
                    productModelLocal.getPrecio(),
                    productModelLocal.getDescuento()

            ));

        return lstProductModels;
    }
}
