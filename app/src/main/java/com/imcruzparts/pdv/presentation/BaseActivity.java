package com.imcruzparts.pdv.presentation;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.imcruzparts.pdv.R;

/**
 * Codigo boilerplate
 */
public class BaseActivity extends AppCompatActivity {
    /**
     * Carga el nuevo fragment
     * @param container identificador del contenedor en el cual se pondra el fragment
     * @param fragment fragment a cargar
     */
    public void loadFragment(int container, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(container, fragment)
                .commitAllowingStateLoss();
    }

    /**
     * Carga el nuevo fragment
     * @param container identificador del contenedor en el cual se pondra el fragment
     * @param fragment fragment a cargar
     * @param tag tag para identificar al fragment
     */
    public void loadFragment(int container, Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(container, fragment, tag)
                .commitAllowingStateLoss();
    }

    /**
     * Carga el nuevo fragment y lo añade a la pila para volver atras
     * @param container identificador del contenedor en el cual se pondra el fragment
     * @param fragment fragment a cargar
     */
    public void loadFragmentAndAddedToStack(int container, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(container, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    /**
     * Carga el nuevo fragment y lo añade a la pila para volver atras, y le agrega un tag
     * @param container identificador del contenedor en el cual se pondra el fragment
     * @param fragment fragment a cargar
     * @param tag tag para identificar al fragment
     */
    public void loadFragmentAndAddedToStack(int container, Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .replace(container, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    /**
     * Remueve un fragment
     * @param fragment fragment a remover
     */
    public void removeCurrentFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .remove(fragment)
                .commit();

        fragmentManager.popBackStack();
    }

    /**
     * Muestra un snackbar
     * @param message mensaje a mostrar en el snackbar
     */
    public void showSnackBar(String message) {
        View rootView = findViewById(R.id.container);
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Muestra un snackbar
     * @param message mensaje a mostrar
     * @param action nombre acción
     * @param listener escuchador
     */
    public void showSnackBar(String message, String action, View.OnClickListener listener) {
        View rootView = findViewById(R.id.container);
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).setAction(action,listener).show();
    }
}