package com.imcruzparts.pdv.presentation.home;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.home.presentation.client.ClientView;
import com.imcruzparts.pdv.presentation.home.util.ClientsAdapter;
import com.imcruzparts.pdv.presentation.util.DividerDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class HomeView extends Fragment implements ClientsAdapter.Pagination,
        SearchView.OnQueryTextListener, MenuItemCompat.OnActionExpandListener,
        ClientsAdapter.OnItemClicked {

    @BindString(R.string.txt_9)
    String toolbarTitle;
    @BindString(R.string.txt_49)
    String txt49;
    @BindString(R.string.txt_61)
    String txt61;
    @BindDrawable(R.mipmap.ic_menu)
    Drawable iconMenu;
    @BindString(R.string.txt_43)
    String txt43;
    @BindString(R.string.txt_71)
    String txt71;

    @BindView(R.id.rv_home_clients)
    RecyclerView rvHomeClients;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.empty_image)
    ImageView emptyIcon;
    @BindView(R.id.empty_title)
    TextView emptyTitle;
    @BindView(R.id.empty_description)
    TextView emptyDescription;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Unbinder mUnbinder;
    private MainActivity mMainActivity;
    private ClientsAdapter mClientsAdapter;
    private HomePresenter mHomePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(true);
        mClientsAdapter = new ClientsAdapter(this, false);
        mHomePresenter = new HomePresenter();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEmptyView();
        rvHomeClients.setAdapter(mClientsAdapter);
        rvHomeClients.setLayoutManager(new LinearLayoutManager(mMainActivity, LinearLayoutManager.VERTICAL, false));
        // Agrega los headers de decoracion al recyclerview
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(mClientsAdapter);
        rvHomeClients.addItemDecoration(headersDecor);
        // Agrega divisores entre los items del recyclerview
        rvHomeClients.addItemDecoration(new DividerDecoration(getActivity()));
        mClientsAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                headersDecor.invalidateHeaders();
            }
        });

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(toolbarTitle);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconMenu);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mHomePresenter.onStart();
        mHomePresenter.getClients();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mHomePresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @OnClick(R.id.fab_home)
    public void onViewClicked() {
        mMainActivity.loadFragmentAndAddedToStack(R.id.container, new ClientView(), "ClientView");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        MenuItem item = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint(txt43);
        searchView.setOnQueryTextListener(this);

        final AutoCompleteTextView filterAutoCompleteTextView;
        filterAutoCompleteTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        filterAutoCompleteTextView.setHint(txt71);

        MenuItemCompat.setOnActionExpandListener(item, this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.openDrawer();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Escuchador que es dispara por {@link ClientsAdapter}
     */
    @Override
    public void getMore() {
        mHomePresenter.getClients();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<HomeModel> lstFilteredClients = mHomePresenter.filter(mClientsAdapter.getLstClientsNotFiltered(), newText);
        mClientsAdapter.setFilteredClients(lstFilteredClients);
        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        mClientsAdapter.restoreAdapter();
        return true;
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.CLIENTS_OBTAINED_V:

                List<HomeModel> lstClients = (List<HomeModel>) action.getPayload();
                mClientsAdapter.setClients(lstClients);
                mClientsAdapter.notifyDataSetChanged();
                break;
        }
    }

    /**
     * Escuchador disparado por {@link ClientsAdapter} cuando un item del recyclerview
     * es presionado
     * @param homeModel datos del cliente seleccionado
     */
    @Override
    public void onItemClicked(HomeModel homeModel) {
        Bundle args = new Bundle();
        Fragment clientView = new ClientView();

        args.putSerializable("HomeModel", homeModel);
        clientView.setArguments(args);

        mMainActivity.loadFragmentAndAddedToStack(R.id.container, clientView, "ClientView");
    }

    private void setEmptyView() {
        emptyTitle.setText(txt49);
        emptyDescription.setText(txt61);
        emptyIcon.setImageDrawable(mMainActivity.getDrawable(R.drawable.ic_menu_clientes));
        mClientsAdapter.setEmptyView(emptyView);
    }
}
