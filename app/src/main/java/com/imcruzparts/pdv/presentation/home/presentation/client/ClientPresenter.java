package com.imcruzparts.pdv.presentation.home.presentation.client;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.home.AddClient;
import com.imcruzparts.pdv.domain.home.UpdateClient;

import org.greenrobot.eventbus.Subscribe;

/**
 * Clase que valida la vista {@link ClientView},
 * y dispara el escuchador {@link ClientView#onMessageEvent(Action)}
 */
public class ClientPresenter {

    private UseCase mAddClient;
    private UseCase mUpdateClient;

    public ClientPresenter() {
        this.mAddClient = new AddClient();
        this.mUpdateClient = new UpdateClient();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.ADD_CLIENT_SUCCESS_P:
                EventBusMask.post(new Action(ActionType.ADD_CLIENT_SUCCESS_V));
                break;

            case ActionType.ADD_CLIENT_ERROR_P:
                EventBusMask.post(new Action(ActionType.ADD_CLIENT_ERROR_V));
                break;

            case ActionType.UPDATE_CLIENT_SUCCESS_P:
                EventBusMask.post(new Action(ActionType.UPDATE_CLIENT_SUCCESS_V));
                break;

            case ActionType.UPDATE_CLIENT_ERROR_P:
                EventBusMask.post(new Action(ActionType.UPDATE_CLIENT_ERROR_V));
                break;
        }
    }

    /**
     * Ejecuta el metodo {@link AddClient#execute(Object)}
     * @param firstName nombre del cliente
     * @param lastName apellidos del cliente
     * @param documentNumber numero de documento del cliente
     * @param cellPhoneNumber numero de celular del cliente
     * @param phoneNumber numero de telefono fijo del cliente
     * @param email correo del cliente
     * @param socialReason razon social del cliente
     * @param nit nit del cliente
     * @param name nombre del negocio del cliente
     * @param state departamento donde esta el negocio del cliente
     * @param address dirección del negocio del cliente
     * @param latitud latitud del negocio del cliente
     * @param longitud longitud del negocio del cliente
     */
    public void addClient(String firstName, String lastName, String documentNumber,
                          String cellPhoneNumber, String phoneNumber, String email,
                          String socialReason, String nit, String name,
                          String state, String address, double latitud, double longitud) {

        mAddClient.execute(AddClient.Params.forExecute(
                name,
                state,
                address,
                latitud,
                longitud,
                firstName,
                lastName,
                documentNumber,
                cellPhoneNumber,
                phoneNumber,
                email,
                socialReason,
                nit
        ));
    }

    /**
     * Ejecuta el metodo {@link UpdateClient#execute(Object)}
     * @param clientId identificador del cliente
     * @param firstName nombre del cliente
     * @param lastName apellidos del cliente
     * @param documentNumber numero de documento del cliente
     * @param cellPhoneNumber numero de celular del cliente
     * @param phoneNumber numero de telefono fijo del cliente
     * @param email correo del cliente
     * @param socialReason razon social del cliente
     * @param nit nit del cliente
     * @param businessId identificador del punto de venta
     * @param name nombre del negocio del cliente
     * @param state departamento donde esta el negocio del cliente
     * @param address dirección del negocio del cliente
     * @param latitud latitud del negocio del cliente
     * @param longitud longitud del negocio del cliente
     */
    public void updateClient(int clientId, String firstName, String lastName, String documentNumber,
                          String cellPhoneNumber, String phoneNumber, String email,
                          String socialReason, String nit, int businessId, String name,
                          String state, String address, double latitud, double longitud) {

        mUpdateClient.execute(UpdateClient.Params.forExecute(
                businessId,
                name,
                state,
                address,
                latitud,
                longitud,
                clientId,
                firstName,
                lastName,
                documentNumber,
                cellPhoneNumber,
                phoneNumber,
                email,
                socialReason,
                nit
        ));
    }
}
