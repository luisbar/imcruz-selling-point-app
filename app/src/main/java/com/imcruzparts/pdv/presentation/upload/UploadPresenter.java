package com.imcruzparts.pdv.presentation.upload;

import android.content.Context;
import android.util.Log;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.upload.SendActivities;
import com.imcruzparts.pdv.domain.upload.SendClients;
import com.imcruzparts.pdv.presentation.download.DownloadView;
import com.imcruzparts.pdv.presentation.upload.util.FileToBase64;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.List;

/**
 * Clase que valida la vista {@link DownloadView},
 * y dispara el escuchador {@link DownloadView#onMessageEvent(Action)}
 */
public class UploadPresenter implements FileToBase64.OnPicturesTransFormed {

    private UseCase mSendClients;
    private UseCase mSendActivities;
    private Context mContext;

    public UploadPresenter(Context mContext) {
        this.mContext = mContext;
        mSendClients = new SendClients();
        mSendActivities = new SendActivities();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Esuchador que es disparado cuando {@link FileToBase64}
     * termina de transformar las imagenes a base64
     * @param lstBase64Files
     */
    @Override
    public void piscturesTransformed(HashMap<String, String> lstBase64Files) {
        mSendActivities.execute(lstBase64Files);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.CLIENTS_UPLOADED_P:
                EventBusMask.post(new Action(ActionType.CLIENTS_UPLOADED_V, action.getPayload(), action.isError()));
                break;

            case ActionType.ACTIVITIES_UPLOADED_P:
                EventBusMask.post(new Action(ActionType.ACTIVITIES_UPLOADED_V, action.getPayload(), action.isError()));
                break;
        }
    }

    /**
     * Ejecuta metodo que envia los clientes nuevos y modificados
     * a la nube
     */
    public void sendClients() {
        mSendClients.execute();
    }

    /**
     * Ejecuta metodo que envia las actividades y pedidos
     * a la nube
     */
    public void sendActivities() {
        new Thread(new FileToBase64(mContext, this)).start();
    }
}
