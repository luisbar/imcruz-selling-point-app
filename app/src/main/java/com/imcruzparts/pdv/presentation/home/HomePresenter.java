package com.imcruzparts.pdv.presentation.home;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.home.GetClients;
import com.imcruzparts.pdv.presentation.home.util.PdvToHome;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que valida la vista {@link HomeView},
 * y dispara el escuchador {@link HomeView#onMessageEvent(Action)}
 */
public class HomePresenter {

    private UseCase mGetClients;
    private Mapper mPvdToHome;

    public HomePresenter() {
        this.mGetClients = new GetClients();
        this.mPvdToHome = new PdvToHome();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.CLIENTS_OBTAINED_P:
                EventBusMask.post(
                        new Action(
                                ActionType.CLIENTS_OBTAINED_V,
                                mPvdToHome.map(action.getPayload()),
                                false
                        )
                );
                break;
        }
    }

    /**
     * Metodo que ejecuta el traer clientes de la base de datos local
     */
    public void getClients() {
        mGetClients.execute();
    }

    /**
     * Filtra la lista de clientes de acuerdo a lo que el usuario introdujo
     * en el SearchView, el dato introducido por el usuario es comparado con el nombre de
     * negocio, nombre de cliente y apellidos
     * @param clients lista completa de clientes
     * @param query dato que el usuario introdujo
     * @return lista de clientes filtrada
     */
    public List<HomeModel> filter(List<HomeModel> clients, String query) {

        query = query.toLowerCase();
        List<HomeModel> filteredClients = new ArrayList<>();

        if (clients!=null && !clients.isEmpty()) {
            for (HomeModel client : clients) {

                String businessName = client.getName().toLowerCase();
                String clientFirstName = client.getClient().getFirstName().toLowerCase();
                String clientLastName = client.getClient().getLastName().toLowerCase();

                if (businessName.contains(query) ||
                        clientFirstName.contains(query) ||
                        clientLastName.contains(query)) {
                    filteredClients.add(client);
                }
            }
        }

        return filteredClients;
    }
}
