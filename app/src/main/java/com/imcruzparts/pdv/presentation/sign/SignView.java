package com.imcruzparts.pdv.presentation.sign;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.imcruzparts.pdv.MainApplication;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class SignView extends Fragment {

    @BindView(R.id.input_sign_username)
    EditText inputSignUsername;
    @BindView(R.id.input_sign_password)
    EditText inputSignPassword;
    @BindView(R.id.progress_sign)
    ProgressBar progressSign;
    @BindView(R.id.btn_sign_in)
    Button btnSignIn;

    @BindString(R.string.txt_6)
    String passwordError;
    @BindString(R.string.txt_7)
    String usernameError;
    @BindString(R.string.txt_5)
    String unauthorizedError;
    @BindString(R.string.txt_8)
    String noInternetError;

    @BindColor(R.color.disabledColor)
    int disabledColor;
    @BindColor(R.color.colorAccent)
    int colorAccent;

    private final String TAG = this.getClass().getCanonicalName();

    private Unbinder mUnbinder;
    private SignPresenter mSignPresenter;
    private MainActivity mMainActivity;
    private MainApplication mMainApplication;

    private boolean mUsernameIsValid;
    private boolean mPasswordIsValid;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        mSignPresenter = new SignPresenter();
        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);
        mMainApplication = (MainApplication) mMainActivity.getApplicationContext();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mSignPresenter.onStart();
        // Verifica si hay una sesión activa
        mSignPresenter.getUserLogged();
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mSignPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @OnClick(R.id.btn_sign_in)
    public void onViewClicked() {
        mSignPresenter
                .signIn(inputSignUsername.getText().toString(),
                        inputSignPassword.getText().toString());

        enableComponents(false);
    }

    @OnTextChanged({R.id.input_sign_username, R.id.input_sign_password})
    public void onTextChanged() {
        if (inputSignUsername.isFocused())
            mSignPresenter.validateUsername(inputSignUsername.getText().toString());
        if (inputSignPassword.isFocused())
            mSignPresenter.validatePassword(inputSignPassword.getText().toString());
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.VALIDATED_PASSWORD:
                mPasswordIsValid = (boolean) action.getPayload();
                if (!mPasswordIsValid) inputSignPassword.setError(passwordError);

                btnSignIn.setBackgroundColor(mUsernameIsValid && mPasswordIsValid ? colorAccent : disabledColor);
                btnSignIn.setEnabled(mUsernameIsValid && mPasswordIsValid);
                break;

            case ActionType.VALIDATED_USERNAME:
                mUsernameIsValid = (boolean) action.getPayload();
                if (!mUsernameIsValid) inputSignUsername.setError(usernameError);

                btnSignIn.setBackgroundColor(mUsernameIsValid && mPasswordIsValid ? colorAccent : disabledColor);
                btnSignIn.setEnabled(mUsernameIsValid && mPasswordIsValid);
                break;

            case ActionType.THERE_IS_USER_LOGGED:
                SignModel signModel = (SignModel) action.getPayload();
                mMainActivity.setHeaderView(signModel.getUser(), signModel.getRole());
                // Falta verificar el rol y de acuerdo a eso mostrar la vista correspondiente
                break;

            case ActionType.SIGN_IN_SUCCESS_V:
                mMainActivity.setHeaderView("Usuario","rol");
                break;

            case ActionType.SIGN_IN_ERROR_V:
                String message = (int) action.getPayload() == R.string.txt_5
                        ? unauthorizedError
                        : noInternetError;
                mMainActivity.showSnackBar(message);
                enableComponents(true);
                break;
        }
    }

    /**
     * Habilita o desabilita los componentes de la vista de acuerdo a una bandera
     * @param flag bandera para habilitar o desabilitar los componentes de la vista: si
     *             flag = true habilita los inputs, el boton y oculta el progressbar,
     *             si flag = false hace lo contrario
     */
    private void enableComponents(boolean flag) {
        inputSignUsername.setEnabled(flag);
        inputSignPassword.setEnabled(flag);
        btnSignIn.setEnabled(flag);
        progressSign.setVisibility(flag ? View.GONE : View.VISIBLE);
    }
}
