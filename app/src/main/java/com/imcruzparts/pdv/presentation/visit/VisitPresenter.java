package com.imcruzparts.pdv.presentation.visit;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.visit.GetRoutes;
import com.imcruzparts.pdv.domain.visit.GetRoutesFromCloud;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.util.AmlToMm;
import com.imcruzparts.pdv.presentation.visit.util.RmlToRm;
import com.imcruzparts.pdv.presentation.visit.util.SrmlToVm;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Clase que valida la vista {@link VisitView},
 * y dispara el escuchador {@link VisitView#onMessageEvent(Action)}
 */
public class VisitPresenter {

    private UseCase mGetRoutesFromCloud;
    private UseCase mGetRoutes;

    public VisitPresenter() {
        this.mGetRoutesFromCloud = new GetRoutesFromCloud();
        this.mGetRoutes = new GetRoutes();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.PENDING_ROUTES_OBTAINED_P:
                EventBusMask.post(new Action(ActionType.PENDING_ROUTES_OBTAINED_V,
                        new SrmlToVm().map((List<SellerRouteModelLocal>) action.getPayload())));
                break;

            case ActionType.PENDING_ROUTES_NOT_OBTAINED_P:
                EventBusMask.post(new Action(ActionType.PENDING_ROUTES_NOT_OBTAINED_V, action.getPayload()));
                break;

            case ActionType.INITIALIZED_ROUTES_OBTAINED_P:
                EventBusMask.post(new Action(ActionType.INITIALIZED_ROUTES_OBTAINED_V,
                        new AmlToMm().map((List<ActivityModelLocal>) action.getPayload())));
                break;

            case ActionType.PENDING_REQUEST_OBTAINED_P:
                EventBusMask.post(new Action(ActionType.PENDING_REQUEST_OBTAINED_V,
                        new RmlToRm().map((List<RequestModelLocal>) action.getPayload())));
                break;
        }
    }

    /**
     * Ejecuta metodo para traer las rutas correspondientes al usuario logueado
     * desde la nube
     */
    public void getRoutesFromloud() {
        mGetRoutesFromCloud.execute();
    }

    /**
     * Ejecuta metodo para traer las rutas correspondientes al usuario logueado
     * de la base de datos local
     */
    public void getRoutes() {
        mGetRoutes.execute();
    }
}
