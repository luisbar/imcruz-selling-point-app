package com.imcruzparts.pdv.presentation.visit.presentation.activity.util;


import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.ActivityModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Convierte objeto {@link List<ActivityModelLocal>} a {@link List< ActivityModel >}
 */
public class AmlToMm implements Mapper<List<ActivityModelLocal>, List<ActivityModel>> {

    @Override
    public List<ActivityModel> map(List<ActivityModelLocal> activityModelLocals) {
        List<ActivityModel> lstActivities = new ArrayList();

        for (ActivityModelLocal activityModelLocal : activityModelLocals) {
            lstActivities.add(
              new ActivityModel(
                      activityModelLocal.getIdActividad(),
                      activityModelLocal.getValor(),
                      activityModelLocal.getDescripcion(),
                      activityModelLocal.getVisible(),
                      activityModelLocal.getEstado(),
                      activityModelLocal.getFecha(),
                      activityModelLocal.getObservaciones(),
                      activityModelLocal.getFechaAlta(),
                      activityModelLocal.getFechaBaja(),
                      activityModelLocal.getRutaVendedor().getIdRutaVendedor(),
                      activityModelLocal.getRutaVendedor().getDistribuidor(),
                      activityModelLocal.getRutaVendedor().getSupervisor(),
                      activityModelLocal.getRutaVendedor().getDescripcion(),
                      activityModelLocal.getRutaVendedor().getFechaProgramacion(),
                      activityModelLocal.getRutaVendedor().getEstado(),
                      activityModelLocal.getRutaVendedor().getFechaAlta(),
                      activityModelLocal.getRutaVendedor().getFechaBaja(),
                      activityModelLocal.getRuta().getIdRuta(),
                      activityModelLocal.getRuta().getNombre(),
                      activityModelLocal.getRuta().getDescripcion(),
                      activityModelLocal.getRuta().getDepartamento(),
                      activityModelLocal.getRuta().getZona(),
                      activityModelLocal.getRuta().getFechaAlta(),
                      activityModelLocal.getRuta().getFechaBaja(),
                      activityModelLocal.getVendedor().getIdEmpleado(),
                      activityModelLocal.getVendedor().getApellido(),
                      activityModelLocal.getVendedor().getDireccion(),
                      activityModelLocal.getVendedor().getEmail(),
                      activityModelLocal.getVendedor().getFechaAlta(),
                      activityModelLocal.getVendedor().getFechaBaja(),
                      activityModelLocal.getVendedor().getNombre(),
                      activityModelLocal.getVendedor().getRol(),
                      activityModelLocal.getVendedor().getTelefono(),
                      activityModelLocal.getPdv().getIdPDV(),
                      activityModelLocal.getPdv().getNombre(),
                      activityModelLocal.getPdv().getDepartamento(),
                      activityModelLocal.getPdv().getDireccion(),
                      activityModelLocal.getPdv().getLatitud(),
                      activityModelLocal.getPdv().getLongitud(),
                      activityModelLocal.getPdv().getEstado(),
                      activityModelLocal.getPdv().getFechaAlta(),
                      activityModelLocal.getPdv().getFechaBaja(),
                      activityModelLocal.getPdv().getIdCliente().getIdCliente(),
                      activityModelLocal.getPdv().getIdCliente().getNombre(),
                      activityModelLocal.getPdv().getIdCliente().getApellido(),
                      activityModelLocal.getPdv().getIdCliente().getNroDocumento(),
                      activityModelLocal.getPdv().getIdCliente().getTelefono(),
                      activityModelLocal.getPdv().getIdCliente().getCelular(),
                      activityModelLocal.getPdv().getIdCliente().getEmail(),
                      activityModelLocal.getPdv().getIdCliente().getEstado(),
                      activityModelLocal.getPdv().getIdCliente().getRazonSocial(),
                      activityModelLocal.getPdv().getIdCliente().getNit()
              )
            );
        }

        return lstActivities;
    }
}
