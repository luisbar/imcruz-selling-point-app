package com.imcruzparts.pdv.presentation.route.presentation.sellingpointroute;

import java.io.Serializable;

public class SellingPointRouteModel implements Serializable {

    private String Estado;
    private String FechaAlta;
    private Object FechaBaja;
    private Integer IdRuta;
    private Integer IdRutaPDV;
    private SellingPoint PDV;
    private Supervisor Supervisor;

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public Object getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(Object fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public Integer getIdRuta() {
        return IdRuta;
    }

    public void setIdRuta(Integer idRuta) {
        IdRuta = idRuta;
    }

    public Integer getIdRutaPDV() {
        return IdRutaPDV;
    }

    public void setIdRutaPDV(Integer idRutaPDV) {
        IdRutaPDV = idRutaPDV;
    }

    public SellingPoint getPDV() {
        return PDV;
    }

    public void setPDV(SellingPoint PDV) {
        this.PDV = PDV;
    }

    public SellingPointRouteModel.Supervisor getSupervisor() {
        return Supervisor;
    }

    public void setSupervisor(SellingPointRouteModel.Supervisor supervisor) {
        Supervisor = supervisor;
    }

    public class Supervisor implements Serializable {

        private String Apellido;
        private String Direccion;
        private Distributor Distribuidor;
        private String Email;
        private String FechaAlta;
        private String FechaBaja;
        private Integer IdEmpleado;
        private String Nombre;
        private Object Sesion;
        private String Telefono;

        public String getApellido() {
            return Apellido;
        }

        public void setApellido(String apellido) {
            Apellido = apellido;
        }

        public String getDireccion() {
            return Direccion;
        }

        public void setDireccion(String direccion) {
            Direccion = direccion;
        }

        public Distributor getDistribuidor() {
            return Distribuidor;
        }

        public void setDistribuidor(Distributor distribuidor) {
            Distribuidor = distribuidor;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdEmpleado() {
            return IdEmpleado;
        }

        public void setIdEmpleado(Integer idEmpleado) {
            IdEmpleado = idEmpleado;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public Object getSesion() {
            return Sesion;
        }

        public void setSesion(Object sesion) {
            Sesion = sesion;
        }

        public String getTelefono() {
            return Telefono;
        }

        public void setTelefono(String telefono) {
            Telefono = telefono;
        }
    }

    public class Distributor implements Serializable {

        private String Direccion;
        private String Estado;
        private String FechaAlta;
        private Object FechaBaja;
        private Integer IdDistribuidor;
        private String Nit;
        private String RazonSocial;
        private String Responsable;

        public String getDireccion() {
            return Direccion;
        }

        public void setDireccion(String direccion) {
            Direccion = direccion;
        }

        public String getEstado() {
            return Estado;
        }

        public void setEstado(String estado) {
            Estado = estado;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public Object getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(Object fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdDistribuidor() {
            return IdDistribuidor;
        }

        public void setIdDistribuidor(Integer idDistribuidor) {
            IdDistribuidor = idDistribuidor;
        }

        public String getNit() {
            return Nit;
        }

        public void setNit(String nit) {
            Nit = nit;
        }

        public String getRazonSocial() {
            return RazonSocial;
        }

        public void setRazonSocial(String razonSocial) {
            RazonSocial = razonSocial;
        }

        public String getResponsable() {
            return Responsable;
        }

        public void setResponsable(String responsable) {
            Responsable = responsable;
        }
    }

    public class SellingPoint implements Serializable {

        private String Departamento;
        private String Direccion;
        private String Estado;
        private String FechaAlta;
        private String FechaBaja;
        private Client IdCliente;
        private Integer IdPDV;
        private Double Latitud;
        private Double Longitud;
        private String Nombre;

        public String getDepartamento() {
            return Departamento;
        }

        public void setDepartamento(String departamento) {
            Departamento = departamento;
        }

        public String getDireccion() {
            return Direccion;
        }

        public void setDireccion(String direccion) {
            Direccion = direccion;
        }

        public String getEstado() {
            return Estado;
        }

        public void setEstado(String estado) {
            Estado = estado;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Client getIdCliente() {
            return IdCliente;
        }

        public void setIdCliente(Client idCliente) {
            IdCliente = idCliente;
        }

        public Integer getIdPDV() {
            return IdPDV;
        }

        public void setIdPDV(Integer idPDV) {
            IdPDV = idPDV;
        }

        public Double getLatitud() {
            return Latitud;
        }

        public void setLatitud(Double latitud) {
            Latitud = latitud;
        }

        public Double getLongitud() {
            return Longitud;
        }

        public void setLongitud(Double longitud) {
            Longitud = longitud;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }
    }

    public class Client implements Serializable {

        private String Apellido;
        private String Celular;
        private String Email;
        private String Estado;
        private String FechaAlta;
        private String FechaBaja;
        private Integer IdCliente;
        private String Nit;
        private String Nombre;
        private String NroDocumento;
        private String RazonSocial;
        private String Telefono;

        public String getApellido() {
            return Apellido;
        }

        public void setApellido(String apellido) {
            Apellido = apellido;
        }

        public String getCelular() {
            return Celular;
        }

        public void setCelular(String celular) {
            Celular = celular;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getEstado() {
            return Estado;
        }

        public void setEstado(String estado) {
            Estado = estado;
        }

        public String getFechaAlta() {
            return FechaAlta;
        }

        public void setFechaAlta(String fechaAlta) {
            FechaAlta = fechaAlta;
        }

        public String getFechaBaja() {
            return FechaBaja;
        }

        public void setFechaBaja(String fechaBaja) {
            FechaBaja = fechaBaja;
        }

        public Integer getIdCliente() {
            return IdCliente;
        }

        public void setIdCliente(Integer idCliente) {
            IdCliente = idCliente;
        }

        public String getNit() {
            return Nit;
        }

        public void setNit(String nit) {
            Nit = nit;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public String getNroDocumento() {
            return NroDocumento;
        }

        public void setNroDocumento(String nroDocumento) {
            NroDocumento = nroDocumento;
        }

        public String getRazonSocial() {
            return RazonSocial;
        }

        public void setRazonSocial(String razonSocial) {
            RazonSocial = razonSocial;
        }

        public String getTelefono() {
            return Telefono;
        }

        public void setTelefono(String telefono) {
            Telefono = telefono;
        }
    }
}
