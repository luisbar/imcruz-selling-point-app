package com.imcruzparts.pdv.presentation.sign.util;

import com.imcruzparts.pdv.common.Mapper;
import com.imcruzparts.pdv.data.sign.model.SessionModelLocal;
import com.imcruzparts.pdv.presentation.sign.SignModel;

/**
 * Mapea objeto {@link com.imcruzparts.pdv.data.sign.model.SessionModelLocal}
 * a {@link com.imcruzparts.pdv.presentation.sign.SignModel}
 */
public class SmlToSm implements Mapper<SessionModelLocal, SignModel> {

    @Override
    public SignModel map(SessionModelLocal sessionModelLocal) {
        return new SignModel(sessionModelLocal.getUsuario(), sessionModelLocal.getRol());
    }
}
