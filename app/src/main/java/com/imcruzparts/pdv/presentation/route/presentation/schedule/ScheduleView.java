package com.imcruzparts.pdv.presentation.route.presentation.schedule;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.route.RouteModel;
import com.imcruzparts.pdv.presentation.route.presentation.schedule.util.SellerModel;

import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ScheduleView extends Fragment implements CompactCalendarView.CompactCalendarViewListener
        , TextWatcher {

    @BindString(R.string.txt_132)
    String txt132;
    @BindString(R.string.txt_8)
    String txt8;
    @BindString(R.string.txt_73)
    String txt73;
    @BindString(R.string.txt_137)
    String txt137;
    @BindString(R.string.txt_138)
    String txt138;

    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable iconClose;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Unbinder unbinder;
    @BindView(R.id.input_schedule_route)
    EditText inputScheduleRoute;
    @BindView(R.id.input_schedule_distributor)
    EditText inputScheduleDistributor;
    @BindView(R.id.input_schedule_seller)
    AutoCompleteTextView inputScheduleSeller;
    @BindView(R.id.input_schedule_note)
    EditText inputScheduleNote;
    @BindView(R.id.text_schedule_month)
    TextView textScheduleMonth;
    @BindView(R.id.calendar)
    CompactCalendarView calendar;
    @BindView(R.id.layout_progress)
    LinearLayout layoutProgress;

    private MainActivity mMainActivity;
    private List<ScheduleModel> lstSchedule;
    private SchedulePresenter mSchedulePresenter;
    private List<SellerModel> lstSeller;

    private String route;
    private String distributor;
    private int idRoute;
    private int idDistributor;
    private int idSeller = -1;
    private Date dateSelected;
    private RouteModel routeModel;

    private int posSellerRouteSelected;
    private boolean dateSelectIsEmpty;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);
        mSchedulePresenter = new SchedulePresenter();

        receiveArgs();

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt132);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconClose);

        inputScheduleRoute.setText(route);
        inputScheduleDistributor.setText(distributor);

        loadSellerRoutes();
        configCalendar();

        ArrayAdapter<SellerModel> adapter = new ArrayAdapter(
                mMainActivity,
                android.R.layout.simple_list_item_1,
                lstSeller
        );
        inputScheduleSeller.setAdapter(adapter);
        inputScheduleSeller.addTextChangedListener(this);
        inputScheduleSeller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SellerModel sellerModel = (SellerModel) parent.getItemAtPosition(position);
                idSeller = sellerModel.getIdEmpleado();
                mMainActivity.invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mSchedulePresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mSchedulePresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_schedule, menu);
        menu.getItem(0).setVisible(dateSelectIsEmpty && idSeller != -1);
        menu.getItem(1).setVisible(!dateSelectIsEmpty ? true : false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.onBackPressed();
                break;

            case R.id.action_add_seller_route:
                layoutProgress.setVisibility(View.VISIBLE);
                mSchedulePresenter.addSellerRoute(
                        idDistributor,
                        idSeller,
                        idRoute,
                        dateSelected,
                        inputScheduleNote.getText().toString(),
                        routeModel
                );
                break;

            case R.id.action_remove_seller_route:
                layoutProgress.setVisibility(View.VISIBLE);
                mSchedulePresenter.removeSellerRoute(lstSchedule.get(posSellerRouteSelected));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Escuchador del calendario cuando un dia es presionado
     * @param dateClicked fecha presionada
     */
    @Override
    public void onDayClick(Date dateClicked) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        int i = 0;

        for (ScheduleModel scheduleModel : lstSchedule) {
            if (dateFormat.format(scheduleModel.getFechaProgramacion()).equals(dateFormat.format(dateClicked))) {
                dateSelectIsEmpty = false;
                mMainActivity.invalidateOptionsMenu();

                inputScheduleSeller.setText(scheduleModel.getVendedor().getNombre());
                inputScheduleNote.setText(scheduleModel.getDescripcion());

                inputScheduleSeller.setFocusableInTouchMode(false);
                inputScheduleNote.setFocusableInTouchMode(false);
                inputScheduleSeller.clearFocus();
                inputScheduleNote.clearFocus();

                posSellerRouteSelected = i;
                return;
            }
            i++;
        }

        idSeller = -1;
        dateSelectIsEmpty = true;
        mMainActivity.invalidateOptionsMenu();

        inputScheduleSeller.setText("");
        inputScheduleNote.getText().clear();
        inputScheduleSeller.setFocusableInTouchMode(true);
        inputScheduleNote.setFocusableInTouchMode(true);

        dateSelected = dateClicked;
    }

    /**
     * Escuchador del calendario cuando se cambia de mes
     * @param firstDayOfNewMonth fecha del primer dia del mes reciente
     */
    @Override
    public void onMonthScroll(Date firstDayOfNewMonth) {
        onDayClick(calendar.getFirstDayOfCurrentMonth());
        switch (firstDayOfNewMonth.getMonth()) {

            case 0:
                textScheduleMonth.setText("Enero");
                break;

            case 1:
                textScheduleMonth.setText("Febrero");
                break;


            case 2:
                textScheduleMonth.setText("Marzo");
                break;


            case 3:
                textScheduleMonth.setText("Abril");
                break;

            case 4:
                textScheduleMonth.setText("Mayo");
                break;

            case 5:
                textScheduleMonth.setText("Junio");
                break;

            case 6:
                textScheduleMonth.setText("Julio");
                break;

            case 7:
                textScheduleMonth.setText("Agosto");
                break;

            case 8:
                textScheduleMonth.setText("Septiembre");
                break;

            case 9:
                textScheduleMonth.setText("Octubre");
                break;

            case 10:
                textScheduleMonth.setText("Noviembre");
                break;

            case 11:
                textScheduleMonth.setText("Diciembre");
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    /**
     * Borra el texto del inputScheduleSeller si se borro un carater de este,
     * para asi asegurar de que se seleccione nuevamente otro vendedor
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (before > count && dateSelectIsEmpty) {
            inputScheduleSeller.removeTextChangedListener(this);

            inputScheduleSeller.getText().clear();
            idSeller = -1;
            mMainActivity.invalidateOptionsMenu();

            inputScheduleSeller.addTextChangedListener(this);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {

        switch (action.getType()) {

            case ActionType.SELLER_ROUTE_ADDED_V:
                if (action.isError())
                    showError(action.getPayload());
                else {
                    ScheduleModel scheduleModel = (ScheduleModel) action.getPayload();
                    lstSchedule.add(scheduleModel);

                    calendar.addEvent(
                            new Event(
                                    Color.GREEN,
                                    scheduleModel.getFechaProgramacion().getTime()
                            )
                    );

                    onDayClick(dateSelected);
                    mMainActivity.showSnackBar(txt137);
                }
                layoutProgress.setVisibility(View.GONE);
                break;

            case ActionType.SELLER_ROUTE_REMOVED_V:
                if (action.isError())
                    showError(action.getPayload());
                else {
                    ScheduleModel routeRemoved = lstSchedule.remove(posSellerRouteSelected);
                    calendar.removeEvents(routeRemoved.getFechaProgramacion());

                    onDayClick(routeRemoved.getFechaProgramacion());
                    mMainActivity.showSnackBar(txt138);
                }

                layoutProgress.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * Recibe los parametros enviados por RouteView
     */
    private void receiveArgs() {
        lstSchedule = getArguments() != null
                ? (List<ScheduleModel>) getArguments().getSerializable("schedules")
                : null;
        lstSeller = getArguments() != null
                ? (List<SellerModel>) getArguments().getSerializable("sellers")
                : null;
        idRoute = getArguments() != null
                ? getArguments().getInt("idRoute")
                : null;
        route = getArguments() != null
                ? getArguments().getString("route")
                : null;
        idDistributor = getArguments() != null
                ? getArguments().getInt("idDistributor")
                : null;
        distributor = getArguments() != null
                ? getArguments().getString("distributor")
                : null;
        routeModel = getArguments() != null
                ? (RouteModel) getArguments().getSerializable("routeObject")
                : null;
    }

    /**
     * Configura el calendario
     */
    private void configCalendar() {
        calendar.setListener(this);
        calendar.setEventIndicatorStyle(CompactCalendarView.FILL_LARGE_INDICATOR);
        calendar.setCurrentDayBackgroundColor(
                calendar.getEvents(new Date()).isEmpty()
                        ? R.color.colorPrimary
                        : Color.GREEN
        );
        calendar.setCurrentDate(calendar.getFirstDayOfCurrentMonth());// Para que aparezca seleccionado el primer dia del mes
        onDayClick(calendar.getFirstDayOfCurrentMonth());// Para que cargue datos a la vista is es que hay un evento guardado el primero del mes actual
        onMonthScroll(new Date());// Para que muestre el nombre del mes actual
    }

    /**
     * Carga las rutas vendedor, si es que hay
     */
    private void loadSellerRoutes() {
        for (ScheduleModel schedule : lstSchedule) {
            calendar.addEvent(new Event(
                    Color.GREEN,
                    schedule.getFechaProgramacion().getTime()
            ));
        }
    }

    private void showError(Object payload) {
        String error = payload instanceof String
                ? String.valueOf(payload)
                : (int) payload == R.string.txt_8
                ? txt8
                : (int) payload == R.string.txt_73
                ? txt73
                : null;

        mMainActivity.showSnackBar(error);
    }
}
