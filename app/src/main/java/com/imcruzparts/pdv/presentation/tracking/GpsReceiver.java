package com.imcruzparts.pdv.presentation.tracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Receptor que escucha cuando el gps es habilitado
 * o deshabilitado
 */
public class GpsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent mIntent = new Intent(context, TrackingService.class);
        mIntent.setAction("GPS");
        context.startService(mIntent);
    }
}

