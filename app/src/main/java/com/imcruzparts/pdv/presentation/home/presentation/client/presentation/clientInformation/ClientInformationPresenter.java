package com.imcruzparts.pdv.presentation.home.presentation.client.presentation.clientInformation;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;

/**
 * Clase que valida la vista {@link ClientInformationView},
 */
public class ClientInformationPresenter {

    /**
     * Valida el nombre del cliente
     * @param firstName nombre del cliente
     */
    public void validateFirstName(String firstName) {
        boolean payload = firstName.length() > 2;

        EventBusMask.post(new Action(ActionType.VALIDATED_FIRST_NAME,
                payload, false));
    }

    /**
     * Valida los apellidos del cliente
     * @param lastName apellidos del cliente
     */
    public void validateLastName(String lastName) {
        boolean payload = lastName.length() > 2;

        EventBusMask.post(new Action(ActionType.VALIDATED_LAST_NAME,
                payload, false));
    }

    /**
     * Valida el numero de documento
     * @param documentNumber numero de documento del cliente
     */
    public void validateDocumentNumber(String documentNumber) {
        boolean payload = documentNumber.length() > 5;

        EventBusMask.post(new Action(ActionType.VALIDATED_DOCUMENT_NUMBER,
                payload, false));
    }

    /**
     * Valida el numero de celular del cliente
     * @param cellPhoneNumber numero de celular del cliente
     */
    public void validateCellPhoneNumber(String cellPhoneNumber) {
        boolean payload = cellPhoneNumber.length() > 7;

        EventBusMask.post(new Action(ActionType.VALIDATED_CELL_PHONE_NUMBER,
                payload, false));
    }

    /**
     * Valida el numero de telefono fijo del cliente
     * @param phoneNumber numero de telefono fijo del cliente
     */
    public void validatePhoneNumber(String phoneNumber) {
        boolean payload = phoneNumber.length() > 6;

        EventBusMask.post(new Action(ActionType.VALIDATED_PHONE_NUMBER,
                payload, false));
    }

    /**
     * Valida el correo del cliente
     * @param email correo del cliente
     */
    public void validateEmail(String email) {
        boolean payload = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

        EventBusMask.post(new Action(ActionType.VALIDATED_EMAIL,
                payload, false));
    }

    /**
     * Valida la razon social del cliente
     * @param socialReason razon social del cliente
     */
    public void validateSocialReason(String socialReason) {
        boolean payload = socialReason.length() > 3;

        EventBusMask.post(new Action(ActionType.VALIDATED_SOCIAL_REASON,
                payload, false));
    }

    /**
     * Valida el nit del cliente
     * @param socialNit nit del cliente
     */
    public void validateNit(String socialNit) {
        boolean payload = socialNit.length() > 5;

        EventBusMask.post(new Action(ActionType.VALIDATED_NIT,
                payload, false));
    }
}
