package com.imcruzparts.pdv.presentation.upload.util;

import android.content.Context;
import android.util.Base64;
import android.util.Base64OutputStream;

import com.imcruzparts.pdv.presentation.upload.UploadPresenter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class FileToBase64 implements Runnable {

    private String APP_PATH;
    private OnPicturesTransFormed mOnPicturesTransFormed;

    public FileToBase64(Context context, UploadPresenter uploadPresenter) {
        this.APP_PATH = context.getFilesDir() + "/activities/";
        this.mOnPicturesTransFormed = uploadPresenter;
    }

    @Override
    public void run() {
        File picturesDirectory = new File(APP_PATH);
        HashMap<String, String> lstBase64Files = new HashMap();

        if (picturesDirectory.listFiles() !=null){
            for (File picture : picturesDirectory.listFiles()) {
                try {
                    InputStream inputStream = new FileInputStream(picture.getPath());
                    byte[] buffer = new byte[1024];
                    int bytesRead;

                    ByteArrayOutputStream output = new ByteArrayOutputStream();
                    Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

                    while ((bytesRead = inputStream.read(buffer)) != -1)
                        output64.write(buffer, 0, bytesRead);

                    output64.close();

                    lstBase64Files.put(picture.getName(), output.toString());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        mOnPicturesTransFormed.piscturesTransformed(lstBase64Files);
    }

    /**
     * Interface para disparar evento en {@link com.imcruzparts.pdv.presentation.upload.UploadPresenter}
     * cuando se ha terminado de transformar todas las imagenes a base64
     */
    public interface OnPicturesTransFormed {

        void piscturesTransformed(HashMap<String, String> lstBase64Files);
    }
}
