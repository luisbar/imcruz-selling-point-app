package com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.request;

import java.io.Serializable;
import java.util.List;

public class RequestModel implements Serializable {

    private Integer idRequest;
    private Integer idSellingPoint;
    private String date;
    private String state;
    private Double subTotal;
    private Double discount;
    private Double total;
    private List<RequestDetail> detail;

    public RequestModel(Integer idRequest, Integer idSellingPoint, String date, String state, Double subTotal, Double discount, Double total, List<RequestDetail> detail) {
        this.idRequest = idRequest;
        this.idSellingPoint = idSellingPoint;
        this.date = date;
        this.state = state;
        this.subTotal = subTotal;
        this.discount = discount;
        this.total = total;
        this.detail = detail;
    }

    public Integer getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(Integer idRequest) {
        this.idRequest = idRequest;
    }

    public Integer getIdSellingPoint() {
        return idSellingPoint;
    }

    public void setIdSellingPoint(Integer idSellingPoint) {
        this.idSellingPoint = idSellingPoint;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<RequestDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<RequestDetail> detail) {
        this.detail = detail;
    }

    public static class RequestDetail implements Serializable {

        private Integer idDetail;
        private Integer quantity;
        private Double price;
        private Double discount;
        private Double subTotal;
        private String created;
        private String deleted;
        private Product product;

        public RequestDetail(Integer idDetail, Integer quantity, Double price,
                             Double discount, Double subTotal, String created,
                             String deleted,
                             Integer productId, String ciaCode, String productCode,
                             String description, String segment, String channel,
                             String fabric, String origin, String family, String brand,
                             String group, String type, Double productPrice,
                             Double productDiscount) {
            this.idDetail = idDetail;
            this.quantity = quantity;
            this.price = price;
            this.discount = discount;
            this.subTotal = subTotal;
            this.created = created;
            this.deleted = deleted;
            this.product = new Product(productId, ciaCode, productCode, description, segment,
                    channel, fabric, origin, family, brand, group, type, productPrice,
                    productDiscount);
        }

        public Integer getIdDetail() {
            return idDetail;
        }

        public void setIdDetail(Integer idDetail) {
            this.idDetail = idDetail;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public Double getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Double subTotal) {
            this.subTotal = subTotal;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }
    }

    public static class Product implements Serializable {

        private Integer productId;
        private String ciaCode;
        private String productCode;
        private String description;
        private String segment;
        private String channel;
        private String fabric;
        private String origin;
        private String family;
        private String brand;
        private String group;
        private String type;
        private Double price;
        private Double discount;

        public Product(Integer productId, String ciaCode, String productCode,
                       String description, String segment, String channel,
                       String fabric, String origin, String family, String brand,
                       String group, String type, Double price, Double discount) {
            this.productId = productId;
            this.ciaCode = ciaCode;
            this.productCode = productCode;
            this.description = description;
            this.segment = segment;
            this.channel = channel;
            this.fabric = fabric;
            this.origin = origin;
            this.family = family;
            this.brand = brand;
            this.group = group;
            this.type = type;
            this.price = price;
            this.discount = discount;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public String getCiaCode() {
            return ciaCode;
        }

        public void setCiaCode(String ciaCode) {
            this.ciaCode = ciaCode;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSegment() {
            return segment;
        }

        public void setSegment(String segment) {
            this.segment = segment;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getFabric() {
            return fabric;
        }

        public void setFabric(String fabric) {
            this.fabric = fabric;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String getFamily() {
            return family;
        }

        public void setFamily(String family) {
            this.family = family;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }
    }
}
