package com.imcruzparts.pdv.presentation.visit.presentation.activity;

import java.io.Serializable;
import java.util.Date;

public class ActivityModel implements Serializable {

    private Integer id;
    private SellerRoute sellerRoute;
    private Route route;
    private Seller seller;
    private SellingPoint sellingPoint;
    private Client client;
    private String value;
    private String description;
    private Boolean visible;
    private String state;
    private String date;
    private String note;
    private String created;
    private String deleted;

    public ActivityModel(Integer id, String value, String description, Boolean visible,
                         String state, String date, String note, String created, String deleted,
                         Integer idSellerRoute, String distributor, String supervisor,
                         String sellerRoutedescription, Date orderDate, String routeState,
                         String createdSellerRoute, String deletedSellerRoute,
                         Integer idRoute, String name, String routeDescription, String country,
                         String zone, String createdRoute, String deletedRoute,
                         Integer idSeller, String lastName, String address, String email,
                         String createdSeller, String deletedSeller, String firstName,
                         String role, String phoneNumber,
                         Integer idSellingPoint, String sellingPointName, String sellingPointCountry,
                         String sellingPointAddress, Double latitude, Double longitude,
                         String sellingPointState, String createdSellingPoint, String deletedSellingPoint,
                         Integer idClient, String firstNameClient, String lastNameClient, String document,
                         String phoneNumberClient, String cellPhoneNumber, String emailClient,
                         String stateClient, String socialReason, String nit) {
        this.id = id;
        this.value = value;
        this.description = description;
        this.visible = visible;
        this.state = state;
        this.date = date;
        this.note = note;
        this.created = created;
        this.deleted = deleted;
        this.sellerRoute = new SellerRoute(idSellerRoute, distributor, supervisor,
                sellerRoutedescription, orderDate, routeState, createdSellerRoute,
                deletedSellerRoute);
        this.route = new Route(idRoute, name, routeDescription, country, zone, createdRoute,
                deletedRoute);
        this.seller = new Seller(idSeller, lastName, address, email, createdSeller,
                deletedSeller, firstName, role, phoneNumber);
        this.sellingPoint = new SellingPoint(idSellingPoint, sellingPointName, sellingPointCountry,
                sellingPointAddress, latitude, longitude, sellingPointState, createdSellingPoint,
                deletedSellingPoint);
        this.client = new Client(idClient, firstNameClient, lastNameClient, document,
                phoneNumberClient, cellPhoneNumber, emailClient, stateClient, socialReason, nit);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SellerRoute getSellerRoute() {
        return sellerRoute;
    }

    public void setSellerRoute(SellerRoute sellerRoute) {
        this.sellerRoute = sellerRoute;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public SellingPoint getSellingPoint() {
        return sellingPoint;
    }

    public void setSellingPoint(SellingPoint sellingPoint) {
        this.sellingPoint = sellingPoint;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public class SellerRoute implements Serializable {

        private Integer id;
        private String distributor;
        private String supervisor;
        private String description;
        private Date orderDate;
        private String state;
        private String created;
        private String deleted;

        public SellerRoute(Integer id, String distributor, String supervisor, String description, Date orderDate, String state, String created, String deleted) {
            this.id = id;
            this.distributor = distributor;
            this.supervisor = supervisor;
            this.description = description;
            this.orderDate = orderDate;
            this.state = state;
            this.created = created;
            this.deleted = deleted;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDistributor() {
            return distributor;
        }

        public void setDistributor(String distributor) {
            this.distributor = distributor;
        }

        public String getSupervisor() {
            return supervisor;
        }

        public void setSupervisor(String supervisor) {
            this.supervisor = supervisor;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Date getOrderDate() {
            return orderDate;
        }

        public void setOrderDate(Date orderDate) {
            this.orderDate = orderDate;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }
    }

    public class Route implements Serializable {

        private Integer id;
        private String name;
        private String description;
        private String country;
        private String zone;
        private String created;
        private String deleted;

        public Route(Integer id, String name, String description, String country, String zone, String created, String deleted) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.country = country;
            this.zone = zone;
            this.created = created;
            this.deleted = deleted;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }
    }

    public class Seller implements Serializable {

        private Integer id;
        private String lastName;
        private String address;
        private String email;
        private String created;
        private String deleted;
        private String firstName;
        private String role;
        private String phoneNumber;

        public Seller(Integer id, String lastName, String address, String email, String created, String deleted, String firstName, String role, String phoneNumber) {
            this.id = id;
            this.lastName = lastName;
            this.address = address;
            this.email = email;
            this.created = created;
            this.deleted = deleted;
            this.firstName = firstName;
            this.role = role;
            this.phoneNumber = phoneNumber;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }

    public class SellingPoint implements Serializable {

        private Integer id;
        private String name;
        private String country;
        private String address;
        private Double latitude;
        private Double longitude;
        private String state;
        private String created;
        private String deleted;

        public SellingPoint(Integer id, String name, String country, String address, Double latitude, Double longitude, String state, String created, String deleted) {
            this.id = id;
            this.name = name;
            this.country = country;
            this.address = address;
            this.latitude = latitude;
            this.longitude = longitude;
            this.state = state;
            this.created = created;
            this.deleted = deleted;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }
    }

    public class Client implements Serializable {

        private Integer id;
        private String firstNameClient;
        private String lastNameClient;
        private String document;
        private String phoneNumberClient;
        private String cellPhoneNumber;
        private String emailClient;
        private String stateClient;
        private String socialReason;
        private String nit;

        public Client(Integer id, String firstNameClient, String lastNameClient, String document, String phoneNumberClient, String cellPhoneNumber, String emailClient, String stateClient, String socialReason, String nit) {
            this.id = id;
            this.firstNameClient = firstNameClient;
            this.lastNameClient = lastNameClient;
            this.document = document;
            this.phoneNumberClient = phoneNumberClient;
            this.cellPhoneNumber = cellPhoneNumber;
            this.emailClient = emailClient;
            this.stateClient = stateClient;
            this.socialReason = socialReason;
            this.nit = nit;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstNameClient() {
            return firstNameClient;
        }

        public void setFirstNameClient(String firstNameClient) {
            this.firstNameClient = firstNameClient;
        }

        public String getLastNameClient() {
            return lastNameClient;
        }

        public void setLastNameClient(String lastNameClient) {
            this.lastNameClient = lastNameClient;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getPhoneNumberClient() {
            return phoneNumberClient;
        }

        public void setPhoneNumberClient(String phoneNumberClient) {
            this.phoneNumberClient = phoneNumberClient;
        }

        public String getCellPhoneNumber() {
            return cellPhoneNumber;
        }

        public void setCellPhoneNumber(String cellPhoneNumber) {
            this.cellPhoneNumber = cellPhoneNumber;
        }

        public String getEmailClient() {
            return emailClient;
        }

        public void setEmailClient(String emailClient) {
            this.emailClient = emailClient;
        }

        public String getStateClient() {
            return stateClient;
        }

        public void setStateClient(String stateClient) {
            this.stateClient = stateClient;
        }

        public String getSocialReason() {
            return socialReason;
        }

        public void setSocialReason(String socialReason) {
            this.socialReason = socialReason;
        }

        public String getNit() {
            return nit;
        }

        public void setNit(String nit) {
            this.nit = nit;
        }
    }
}
