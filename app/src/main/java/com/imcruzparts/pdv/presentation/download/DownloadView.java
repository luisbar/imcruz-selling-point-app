package com.imcruzparts.pdv.presentation.download;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.presentation.MainActivity;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Vista pasiva que solo actualiza su estado de acuerdo a ciertas acciones
 * de {@link ActionType} que son escuchadas por {@link #onMessageEvent(Action)}
 */
public class DownloadView extends Fragment {

    @BindString(R.string.txt_112)
    String txt112;
    @BindString(R.string.txt_69)
    String txt69;
    @BindString(R.string.txt_8)
    String txt8;
    @BindString(R.string.txt_73)
    String txt73;
    @BindString(R.string.txt_116)
    String txt116;
    @BindString(R.string.txt_117)
    String txt117;

    @BindDrawable(R.mipmap.ic_menu)
    Drawable menu;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Unbinder unbinder;
    @BindView(R.id.check_download_clients)
    CheckBox checkDownloadClients;
    @BindView(R.id.check_download_products)
    CheckBox checkDownloadProducts;
    @BindView(R.id.layout_progress)
    LinearLayout layoutProgress;

    private MainActivity mMainActivity;
    private DownloadPresenter mDownloadPresenter;

    private boolean showMenu = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_download, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(true);
        mDownloadPresenter = new DownloadPresenter();
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt112);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusMask.register(this);
        mDownloadPresenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusMask.unregister(this);
        mDownloadPresenter.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_download, menu);
        menu.getItem(0).setVisible(
                showMenu
                ? true
                : false
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.openDrawer();
                break;

            case R.id.action_download_clients_products:
                layoutProgress.setVisibility(View.VISIBLE);
                showMenu(false);

                if (checkDownloadClients.isChecked())
                    mDownloadPresenter.getClients();
                if (checkDownloadProducts.isChecked())
                    mDownloadPresenter.getProducts();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.CLIENTS_DOWNLOADED_V:
                if (action.isError())
                    showError((Integer) action.getPayload());
                else
                    mMainActivity.showSnackBar(txt116);

                checkDownloadClients.setChecked(false);
                if (!checkDownloadProducts.isChecked() && !checkDownloadClients.isChecked())
                    layoutProgress.setVisibility(View.GONE);

                break;

            case ActionType.PRODUCTS_DOWNLOADED_V:
                if (action.isError())
                    showError((Integer) action.getPayload());
                else
                    mMainActivity.showSnackBar(txt117);

                checkDownloadProducts.setChecked(false);
                if (!checkDownloadProducts.isChecked() && !checkDownloadClients.isChecked())
                    layoutProgress.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick({R.id.check_download_clients, R.id.check_download_products})
    public void onViewClicked(View view) {
        showMenu(checkDownloadClients.isChecked() || checkDownloadProducts.isChecked());
    }

    private void showError(int idError) {
        String error = idError == R.string.txt_8
                ? txt8
                : idError == R.string.txt_69
                ? txt69
                : idError == R.string.txt_73
                ? txt73
                : null;

        mMainActivity.showSnackBar(error);
    }

    private void showMenu(boolean flag) {
        showMenu = flag;
        mMainActivity.invalidateOptionsMenu();
    }
}
