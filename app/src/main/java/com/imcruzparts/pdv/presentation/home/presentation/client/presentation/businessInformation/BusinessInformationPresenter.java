package com.imcruzparts.pdv.presentation.home.presentation.client.presentation.businessInformation;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;

/**
 * Clase que valida la vista {@link BusinessInformationView},
 */
public class BusinessInformationPresenter {

    /**
     * Valida el nombre del negocio
     * @param name nombre del negocio
     */
    public void validateName(String name) {
        boolean payload = name.length() > 2;

        EventBusMask.post(new Action(ActionType.VALIDATED_NAME,
                payload, false));
    }

    /**
     * Valida el departamento
     * @param state departamento donde esta el negocio
     */
    public void validateState(String state) {
        boolean payload = state.length() > 1;

        EventBusMask.post(new Action(ActionType.VALIDATED_STATE,
                payload, false));
    }

    /**
     * Valida la dirección del negocio
     * @param address dirección del negocio
     */
    public void validateAddress(String address) {
        boolean payload = address.length() > 3;

        EventBusMask.post(new Action(ActionType.VALIDATED_ADDRESS,
                payload, false));
    }
}
