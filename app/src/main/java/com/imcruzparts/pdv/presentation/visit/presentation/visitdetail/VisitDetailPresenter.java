package com.imcruzparts.pdv.presentation.visit.presentation.visitdetail;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.domain.UseCase;
import com.imcruzparts.pdv.domain.visitdetail.CreateActivities;
import com.imcruzparts.pdv.presentation.visit.presentation.activity.util.AmlToMm;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Clase que valida la vista {@link VisitDetailView},
 * y dispara el escuchador {@link VisitDetailView#onMessageEvent(Action)}
 */
public class VisitDetailPresenter {

    private UseCase mCreateActivities;

    public VisitDetailPresenter() {
        this.mCreateActivities = new CreateActivities();
    }

    /**
     * Registra el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStart() {
        EventBusMask.register(this);
    }

    /**
     * Remueve el escuchador {@link #onMessageEvent(Action)}
     */
    public void onStop() {
        EventBusMask.unregister(this);
    }

    /**
     * Escucha ciertas acciones que estan en {@link ActionType}
     * @param action objecto de tipo {@link Action}
     */
    @Subscribe
    public void onMessageEvent(Action action) {
        switch (action.getType()) {

            case ActionType.ACTIVITIES_CREATED_P:
                EventBusMask.post(new Action(ActionType.ACTIVITIES_CREATED_V,
                        new AmlToMm().map((List<ActivityModelLocal>) action.getPayload())));
                break;

            case ActionType.ACTIVITIES_NOT_CREATED_P:
                EventBusMask.post(new Action(ActionType.ACTIVITIES_NOT_CREATED_V));
                break;
        }
    }

    /**
     * Ejecuta metodo para crear las actividades con el id de ruta especificado
     * @param idSellerRoute identificador de la ruta de vendedor
     */
    public void createActivities(int idSellerRoute) {
        mCreateActivities.execute(idSellerRoute);
    }
}
