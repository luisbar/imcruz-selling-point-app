package com.imcruzparts.pdv.presentation.product.presentation.detail;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.MainActivity;
import com.imcruzparts.pdv.presentation.product.ProductModel;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Vista pasiva que muestra el detalle de un producto
 */
public class DetailView extends Fragment implements AppBarLayout.OnOffsetChangedListener {

    @BindView(R.id.input_product_detail_description)
    EditText inputProductDetailDescription;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.appbar)
    AppBarLayout appbar;

    @BindDrawable(R.mipmap.ic_window_close)
    Drawable iconClose;

    @BindString(R.string.txt_48)
    String txt48;
    @BindView(R.id.input_product_detail_product_code)
    EditText inputProductDetailProductCode;

    @BindView(R.id.input_product_detail_cia_code)
    EditText inputProductDetailCiaCode;
    @BindView(R.id.input_product_detail_segment)
    EditText inputProductDetailSegment;
    @BindView(R.id.input_product_detail_chanel)
    EditText inputProductDetailChanel;
    @BindView(R.id.input_product_detail_fabric)
    EditText inputProductDetailFabric;
    @BindView(R.id.input_product_detail_origin)
    EditText inputProductDetailOrigin;
    @BindView(R.id.input_product_detail_family)
    EditText inputProductDetailFamily;
    @BindView(R.id.input_product_detail_brand)
    EditText inputProductDetailBrand;
    @BindView(R.id.input_product_detail_group)
    EditText inputProductDetailGroup;
    @BindView(R.id.input_product_detail_type)
    EditText inputProductDetailType;

    private MainActivity mMainActivity;
    private Unbinder mUnbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt48);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(iconClose);

        appbar.addOnOffsetChangedListener(this);

        if (getArguments() != null) {
            ProductModel productModel = (ProductModel) getArguments().getSerializable("Product");

            inputProductDetailDescription.setText(productModel.getDescription());
            inputProductDetailProductCode.setText(productModel.getProductCode());
            inputProductDetailCiaCode.setText(productModel.getCiaCode());
            inputProductDetailSegment.setText(productModel.getSegment());
            inputProductDetailChanel.setText(productModel.getChannel());
            inputProductDetailFabric.setText(productModel.getFabric());
            inputProductDetailOrigin.setText(productModel.getOrigin());
            inputProductDetailFamily.setText(productModel.getFamily());
            inputProductDetailBrand.setText(productModel.getBrand());
            inputProductDetailGroup.setText(productModel.getGroup());
            inputProductDetailType.setText(productModel.getType());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private static final int PERCENTAGE_TO_SHOW_IMAGE = 20;
    private int mMaxScrollSize;

    private boolean mIsImageHidden;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(verticalOffset)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true;
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false;
            }
        }
    }
}
