package com.imcruzparts.pdv.presentation.home;

import java.io.Serializable;

public class HomeModel implements Serializable {

    private Integer _id;
    private String name;
    private String country;
    private String address;
    private Double latitude;
    private Double longitude;
    private String state;
    private Long _created;
    private Long _deleted;
    private ClientModel client;

    public HomeModel() {
    }

    public HomeModel(Integer _id, String name, String country, String address, Double latitude, Double longitude, String state, Long _created, Long _deleted, ClientModel client) {
        this._id = _id;
        this.name = name;
        this.country = country;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.state = state;
        this._created = _created;
        this._deleted = _deleted;
        this.client = client;
    }

    public HomeModel(Integer _id, String name, String country, String address, Double latitude, Double longitude, ClientModel client) {
        this._id = _id;
        this.name = name;
        this.country = country;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.client = client;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long get_created() {
        return _created;
    }

    public void set_created(Long _created) {
        this._created = _created;
    }

    public Long get_deleted() {
        return _deleted;
    }

    public void set_deleted(Long _deleted) {
        this._deleted = _deleted;
    }

    public ClientModel getClient() {
        return client;
    }

    public void setClient(ClientModel client) {
        this.client = client;
    }

    public static class ClientModel implements Serializable {

        private Integer _id;
        private String firstName;
        private String lastName;
        private String documentNumber;
        private String cellPhoneNumber;
        private String phoneNumber;
        private String email;
        private String state;
        private String socialReason;
        private String nit;
        private Long _created;

        public ClientModel() {
        }

        public ClientModel(Integer _id, String firstName, String lastName, String documentNumber, String cellPhoneNumber, String phoneNumber, String email, String state, String socialReason, String nit, Long _created) {
            this._id = _id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.documentNumber = documentNumber;
            this.cellPhoneNumber = cellPhoneNumber;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.state = state;
            this.socialReason = socialReason;
            this.nit = nit;
            this._created = _created;
        }

        public ClientModel(Integer _id, String firstName, String lastName, String documentNumber, String cellPhoneNumber, String phoneNumber, String email, String socialReason, String nit) {
            this._id = _id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.documentNumber = documentNumber;
            this.cellPhoneNumber = cellPhoneNumber;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.socialReason = socialReason;
            this.nit = nit;
        }

        public Integer get_id() {
            return _id;
        }

        public void set_id(Integer _id) {
            this._id = _id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getDocumentNumber() {
            return documentNumber;
        }

        public void setDocumentNumber(String documentNumber) {
            this.documentNumber = documentNumber;
        }

        public String getCellPhoneNumber() {
            return cellPhoneNumber;
        }

        public void setCellPhoneNumber(String cellPhoneNumber) {
            this.cellPhoneNumber = cellPhoneNumber;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getSocialReason() {
            return socialReason;
        }

        public void setSocialReason(String socialReason) {
            this.socialReason = socialReason;
        }

        public String getNit() {
            return nit;
        }

        public void setNit(String nit) {
            this.nit = nit;
        }

        public Long get_created() {
            return _created;
        }

        public void set_created(Long _created) {
            this._created = _created;
        }

        public static ClientModel constructor(Integer _id, String firstName, String lastName, String documentNumber, String cellPhoneNumber, String phoneNumber, String email, String socialReason, String nit) {
            return new ClientModel(_id,
                            firstName,
                            lastName,
                            documentNumber,
                            cellPhoneNumber,
                            phoneNumber,
                            email,
                            socialReason,
                            nit
                    );
        }
    }
}
