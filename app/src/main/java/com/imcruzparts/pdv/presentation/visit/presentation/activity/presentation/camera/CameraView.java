package com.imcruzparts.pdv.presentation.visit.presentation.activity.presentation.camera;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.presentation.MainActivity;

import java.io.File;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.result.PendingResult;
import io.fotoapparat.result.PhotoResult;

/**
 * Vista que sirve para tomar fotos, y almacena una foto por actividad
 */
public class CameraView extends Fragment {

    @BindString(R.string.txt_96)
    String txt96;
    @BindString(R.string.txt_122)
    String txt122;

    @BindDrawable(R.mipmap.ic_window_close_gray)
    Drawable closeIcon;

    @BindView(R.id.camera_view)
    io.fotoapparat.view.CameraView cameraView;
    Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Fotoapparat mFotoapparat;
    private MainActivity mMainActivity;

    private String APP_PATH;
    private int idActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);

        mMainActivity = (MainActivity) this.getActivity();
        mMainActivity.enableDrawer(false);
        unbinder = ButterKnife.bind(this, view);
        idActivity = getArguments() != null ? getArguments().getInt("idActivity") : idActivity;

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mMainActivity.setSupportActionBar(toolbar);
        mMainActivity.getSupportActionBar().setTitle(txt122);

        mMainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mMainActivity.getSupportActionBar().setHomeAsUpIndicator(closeIcon);

        mFotoapparat = Fotoapparat
                .with(mMainActivity)
                .into(cameraView)
                .build();

        createDirectoryForPictures();
    }

    @Override
    public void onStart() {
        super.onStart();
        mFotoapparat.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        mFotoapparat.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                mMainActivity.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.take_piture)
    public void onViewClicked() {
        PhotoResult photoResult = mFotoapparat.takePicture();
        File photo = new File(APP_PATH, idActivity + ".jpg");

        photoResult.saveToFile(photo).whenAvailable(new PendingResult.Callback<Void>() {
            @Override
            public void onResult(Void aVoid) {
                mMainActivity.showSnackBar(txt96);
            }
        });
    }

    /**
     * Crea directorio donde seran almacenadas las fotos de las actividades
     */
    private void createDirectoryForPictures() {
        File file = new File(mMainActivity.getFilesDir(), "activities");

        if (!file.exists())
            file.mkdir();

        APP_PATH = file.getPath();
    }
}
