package com.imcruzparts.pdv.data.seller.specification;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetSellers {

    @GET("Empleado/List/100")
    Call<Object> get();
}
