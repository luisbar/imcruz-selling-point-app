package com.imcruzparts.pdv.data.sellerroute.model;

import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.data.sign.model.EmployeeModelLocal;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SellerRouteModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdRutaVendedor;
    private RouteModelLocal RutaVenta;
    private String Distribuidor;
    private String Supervisor;
    private EmployeeModelLocal Vendedor;
    private RealmList<PdvModelLocal> PDVs;
    private String Descripcion;
    private Date FechaProgramacion;
    private String Estado;
    private String FechaAlta;
    private String FechaBaja;

    public SellerRouteModelLocal() {
    }

    public SellerRouteModelLocal(Integer idRutaVendedor, RouteModelLocal rutaVenta, String distribuidor, String supervisor, EmployeeModelLocal vendedor, RealmList<PdvModelLocal> PDVs, String descripcion, Date fechaProgramacion, String estado, String fechaAlta, String fechaBaja) {
        IdRutaVendedor = idRutaVendedor;
        RutaVenta = rutaVenta;
        Distribuidor = distribuidor;
        Supervisor = supervisor;
        Vendedor = vendedor;
        this.PDVs = PDVs;
        Descripcion = descripcion;
        FechaProgramacion = fechaProgramacion;
        Estado = estado;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
    }

    public Integer getIdRutaVendedor() {
        return IdRutaVendedor;
    }

    public void setIdRutaVendedor(Integer idRutaVendedor) {
        IdRutaVendedor = idRutaVendedor;
    }

    public RouteModelLocal getRutaVenta() {
        return RutaVenta;
    }

    public void setRutaVenta(RouteModelLocal rutaVenta) {
        RutaVenta = rutaVenta;
    }

    public String getDistribuidor() {
        return Distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        Distribuidor = distribuidor;
    }

    public String getSupervisor() {
        return Supervisor;
    }

    public void setSupervisor(String supervisor) {
        Supervisor = supervisor;
    }

    public EmployeeModelLocal getVendedor() {
        return Vendedor;
    }

    public void setVendedor(EmployeeModelLocal vendedor) {
        Vendedor = vendedor;
    }

    public RealmList<PdvModelLocal> getPDVs() {
        return PDVs;
    }

    public void setPDVs(RealmList<PdvModelLocal> PDVs) {
        this.PDVs = PDVs;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Date getFechaProgramacion() {
        return FechaProgramacion;
    }

    public void setFechaProgramacion(Date fechaProgramacion) {
        FechaProgramacion = fechaProgramacion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }
}
