package com.imcruzparts.pdv.data.request.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RequestModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdPedido;
    private Integer IdPdv;
    private Integer IdActividad;
    private String Fecha;
    private String Estado;
    private Double SubTotal;
    private Double Descuento;
    private Double MontoTotal;
    private RealmList<RequestDetailModelLocal> Detalle;
    private String Observacion;

    public RequestModelLocal() {
    }

    public RequestModelLocal(Integer idPedido, Integer idPdv, Integer idActividad, String fecha, String estado, Double subTotal, Double descuento, Double montoTotal, RealmList<RequestDetailModelLocal> detalle, String observacion) {
        IdPedido = idPedido;
        IdPdv = idPdv;
        IdActividad = idActividad;
        Fecha = fecha;
        Estado = estado;
        SubTotal = subTotal;
        Descuento = descuento;
        MontoTotal = montoTotal;
        Detalle = detalle;
        Observacion = observacion;
    }

    public Integer getIdPedido() {
        return IdPedido;
    }

    public void setIdPedido(Integer idPedido) {
        IdPedido = idPedido;
    }

    public Integer getIdPdv() {
        return IdPdv;
    }

    public void setIdPdv(Integer idPdv) {
        IdPdv = idPdv;
    }

    public Integer getIdActividad() {
        return IdActividad;
    }

    public void setIdActividad(Integer idActividad) {
        IdActividad = idActividad;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public Double getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(Double subTotal) {
        SubTotal = subTotal;
    }

    public Double getDescuento() {
        return Descuento;
    }

    public void setDescuento(Double descuento) {
        Descuento = descuento;
    }

    public Double getMontoTotal() {
        return MontoTotal;
    }

    public void setMontoTotal(Double montoTotal) {
        MontoTotal = montoTotal;
    }

    public RealmList<RequestDetailModelLocal> getDetalle() {
        return Detalle;
    }

    public void setDetalle(RealmList<RequestDetailModelLocal> detalle) {
        Detalle = detalle;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }
}
