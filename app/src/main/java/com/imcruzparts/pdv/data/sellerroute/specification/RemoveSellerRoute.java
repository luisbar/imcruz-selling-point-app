package com.imcruzparts.pdv.data.sellerroute.specification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RemoveSellerRoute {

    @POST("RutaVenta/Programacion/Remove")
    Call<Void> post(@Body Object sellerRoute);
}
