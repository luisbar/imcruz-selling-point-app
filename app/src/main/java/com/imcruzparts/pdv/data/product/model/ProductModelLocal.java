package com.imcruzparts.pdv.data.product.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProductModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdProducto;
    private String CiaCod;
    private String CodProducto;
    private String Descripcion;
    private String Segmento;
    private String Canal;
    private String Fabrica;
    private String Origen;
    private String Familia;
    private String Marca;
    private String Grupo;
    private String Tipo;
    private Double Precio;
    private Double Descuento;

    public ProductModelLocal() {
    }

    public ProductModelLocal(Integer idProducto, String ciaCod, String codProducto, String descripcion, String segmento, String canal, String fabrica, String origen, String familia, String marca, String grupo, String tipo, Double precio, Double descuento) {
        IdProducto = idProducto;
        CiaCod = ciaCod;
        CodProducto = codProducto;
        Descripcion = descripcion;
        Segmento = segmento;
        Canal = canal;
        Fabrica = fabrica;
        Origen = origen;
        Familia = familia;
        Marca = marca;
        Grupo = grupo;
        Tipo = tipo;
        Precio = precio;
        Descuento = descuento;
    }

    public Integer getIdProducto() {
        return IdProducto;
    }

    public void setIdProducto(Integer idProducto) {
        IdProducto = idProducto;
    }

    public String getCiaCod() {
        return CiaCod;
    }

    public void setCiaCod(String ciaCod) {
        CiaCod = ciaCod;
    }

    public String getCodProducto() {
        return CodProducto;
    }

    public void setCodProducto(String codProducto) {
        CodProducto = codProducto;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getSegmento() {
        return Segmento;
    }

    public void setSegmento(String segmento) {
        Segmento = segmento;
    }

    public String getCanal() {
        return Canal;
    }

    public void setCanal(String canal) {
        Canal = canal;
    }

    public String getFabrica() {
        return Fabrica;
    }

    public void setFabrica(String fabrica) {
        Fabrica = fabrica;
    }

    public String getOrigen() {
        return Origen;
    }

    public void setOrigen(String origen) {
        Origen = origen;
    }

    public String getFamilia() {
        return Familia;
    }

    public void setFamilia(String familia) {
        Familia = familia;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String grupo) {
        Grupo = grupo;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double precio) {
        Precio = precio;
    }

    public Double getDescuento() {
        return Descuento;
    }

    public void setDescuento(Double descuento) {
        Descuento = descuento;
    }
}
