package com.imcruzparts.pdv.data.sellerroute.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene las actividades de la ruta pendiente
 */
public class GetActivitiesAboutPendingRoute implements RealmSpecification {

    private Realm mRealm;

    public GetActivitiesAboutPendingRoute(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(ActivityModelLocal.class)
                .equalTo("RutaVendedor.Estado", "INICIADA")
                .findAll();
    }
}
