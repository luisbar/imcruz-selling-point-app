package com.imcruzparts.pdv.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imcruzparts.pdv.common.DateSerializer;

import java.io.IOException;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    /**
     * Metodo para crear el servicio rest con token
     * @param serviceClass clase del rest api
     * @param baseUrl      url del servicio
     * @param token        token de autentificacion
     * @param <S>          Servicio rest api
     * @return retornal servicio de rest api
     */
    public static <S> S createService(Class<S> serviceClass, String baseUrl, final String token) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // Configura el nivel de log deseado
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        OkHttpClient client = httpClient.build();

        if (token != null) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Authorization", token)
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit adapter = builder.client(httpClient.build()).build();

        return adapter.create(serviceClass);
    }

    /**
     * Metodo para crear el servicio rest sin token
     * @param serviceClass clase del rest api
     * @param baseUrl      url del servicio
     * @param <S>          Servicio rest api
     * @return retornal servicio de rest api
     */
    public static <S> S createService(Class<S> serviceClass, String baseUrl) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        OkHttpClient client = httpClient.build();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // Configura el nivel de log deseado
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Accept", "application/json")
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit adapter = builder.client(httpClient.build()).build();
        return adapter.create(serviceClass);
    }

    /**
     * Metodo para crear el servicio rest con token
     * @param serviceClass clase del rest api
     * @param baseUrl      url del servicio
     * @param token        token de autentificacion
     * @param <S>          Servicio rest api
     * @return retornal servicio de rest api
     */
    public static <S> S createServiceWithDateFormatter(Class<S> serviceClass, String baseUrl, final String token) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer());

        Gson gson = gsonBuilder.create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // Configura el nivel de log deseado
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        OkHttpClient client = httpClient.build();

        if (token != null) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Authorization", token)
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

        }

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit adapter = builder.client(httpClient.build()).build();

        return adapter.create(serviceClass);
    }
}
