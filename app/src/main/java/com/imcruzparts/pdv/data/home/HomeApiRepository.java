package com.imcruzparts.pdv.data.home;

import com.google.gson.Gson;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.data.home.specification.GetClientsFromCloud;
import com.imcruzparts.pdv.data.home.specification.GetNewAndUpdatedClients;
import com.imcruzparts.pdv.data.home.specification.SendClients;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeApiRepository implements ApiRepository {

    private final int OK = 200;
    private final int BAD_REQUEST = 400;
    private final int SESSION_EXPIRED = 401;

    @Override
    public void patch(Object o) {
        // TODO: nada
    }

    @Override
    public void getOne(Object o) {
        // TODO: nada
    }

    /**
     * Obtiene todos los clientes de la nube, luego
     * invoca a la clase encargada de guardarla localmente
     */
    @Override
    public void getMany() {
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        GetClientsFromCloud getClientsFromCloud = ServiceGenerator.createService(GetClientsFromCloud.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = getClientsFromCloud.get();

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()){

                    case OK:
                        HomeDatabaseRepository mHomeDatabaseRepository;
                        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
                        Gson gson = new Gson();

                        mHomeDatabaseRepository.addMany(gson.toJson(response.body()));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.CLIENTS_DOWNLOADED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.CLIENTS_DOWNLOADED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.CLIENTS_DOWNLOADED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }


    /**
     * Sube los clientes nuevos o midificados a la nube
     */
    @Override
    public void post(Object o) {
        // Obtiene el token
        final DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Obtiene la lista de clientes a enviar (esto no deberia estar aqui)
        final Realm realm = Realm.getDefaultInstance();
        RealmSpecification clientsNewOrUpdated = new GetNewAndUpdatedClients(realm);
        final RealmResults<PdvModelLocal> resultPdvs = clientsNewOrUpdated.toRealmResult();
        List<PdvModelLocal> lstPdvs = realm.copyFromRealm(resultPdvs);
        // Crea el servicio
        SendClients sendClients = ServiceGenerator.createService(SendClients.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = sendClients.post(lstPdvs);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {
                    case OK:
                        realm.close();
                        for (PdvModelLocal pdv : resultPdvs)
                            mHomeDatabaseRepository.update(HomeDatabaseRepository.Params.forUpdate(
                                    pdv.getIdPDV(),
                                    pdv.getNombre(),
                                    pdv.getDepartamento(),
                                    pdv.getDireccion(),
                                    pdv.getLatitud(),
                                    pdv.getLongitud(),
                                    "ACTIVO",
                                    pdv.getFechaAlta(),
                                    pdv.getFechaBaja(),
                                    pdv.getIdCliente().getIdCliente(),
                                    pdv.getIdCliente().getNombre(),
                                    pdv.getIdCliente().getApellido(),
                                    pdv.getIdCliente().getNroDocumento(),
                                    pdv.getIdCliente().getCelular(),
                                    pdv.getIdCliente().getTelefono(),
                                    pdv.getIdCliente().getEmail(),
                                    "ACTIVO",
                                    pdv.getIdCliente().getRazonSocial(),
                                    pdv.getIdCliente().getNit(),
                                    pdv.getIdCliente().getFechaAlta(),
                                    pdv.getIdCliente().getFechaBaja()
                            ));
                        EventBusMask.post(new Action(ActionType.CLIENTS_UPLOADED_P));
                        break;

                    case BAD_REQUEST:
                        realm.close();
                        EventBusMask.post(new Action(ActionType.CLIENTS_UPLOADED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        realm.close();
                        EventBusMask.post(new Action(ActionType.CLIENTS_UPLOADED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.CLIENTS_UPLOADED_P, R.string.txt_8, true));
                realm.close();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void deleteOne(Object o) {
        // TODO: nada
    }
}
