package com.imcruzparts.pdv.data.main.specification;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Cierra sesión, el cierre de sesión consiste en setear la fechaHoraFin devuelta
 * por el servidor en la nube, en la base de datos local
 */
public interface SignOut {

    @POST("Close")
    Call<Object> post();
}
