package com.imcruzparts.pdv.data.product;


import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.product.model.ProductModelLocal;
import com.imcruzparts.pdv.data.product.specification.GetProducts;

import io.realm.Realm;
import io.realm.RealmResults;

public class ProductDatabaseRepository implements DatabaseRepository<ProductModelLocal, String> {

    private Realm mRealm;

    @Override
    public void add(ProductModelLocal item) {
        // TODO: nada
    }

    /**
     * Guarda muchos productos en la base de datos local
     * @param jsonArray objtetos a guardar
     */
    @Override
    public void addMany(final String jsonArray) {
        mRealm = Realm.getDefaultInstance();

        mRealm.executeTransactionAsync(new Realm.Transaction() {
           @Override
           public void execute(Realm realm) {
               realm.createOrUpdateAllFromJson(ProductModelLocal.class, jsonArray);
           }
       },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mRealm.close();
                EventBusMask.post(new Action(ActionType.PRODUCTS_DOWNLOADED_P));
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                EventBusMask.post(new Action(ActionType.PRODUCTS_DOWNLOADED_P, R.string.txt_69, true));
                error.printStackTrace();
            }
        });
    }

    @Override
    public void update(ProductModelLocal item) {
        // TODO: nada
    }

    @Override
    public void remove(String specification) {
        // TODO: nada
    }

    @Override
    public void queryOne(String specification) {
        // TODO: nada
    }

    @Override
    public String queryOne() {
        return null;
    }

    /**
     * Obtiene todos los productos de la base de datos local
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de la consulta a realizar
     */
    @Override
    public void queryMany(String specification) {
        mRealm = Realm.getDefaultInstance();

        RealmSpecification mGetProducts = new GetProducts(mRealm);
        RealmResults results = mGetProducts.toRealmResult();
        EventBusMask.post(new Action(ActionType.PRODUCTS_OBTAINED_P,
                results.subList(0, results.size()), false));

        mRealm.close();
    }
}
