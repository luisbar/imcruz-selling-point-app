package com.imcruzparts.pdv.data.home.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.sign.model.SessionModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene token activo, si es que hubiese
 */
public class GetToken implements RealmSpecification {

    private Realm mRealm;

    public GetToken(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(SessionModelLocal.class)
                .isNull("FechaHoraFin").findAll();
    }
}
