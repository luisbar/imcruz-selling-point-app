package com.imcruzparts.pdv.data.sign.specification;


import com.imcruzparts.pdv.data.sign.specification.util.ParamForSignIn;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SignIn {

    @POST("Sesion")
    Call<Object> post(@Body ParamForSignIn paramForSignIn);
}
