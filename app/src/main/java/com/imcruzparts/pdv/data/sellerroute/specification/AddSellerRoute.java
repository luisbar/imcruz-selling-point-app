package com.imcruzparts.pdv.data.sellerroute.specification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AddSellerRoute {

    @POST("RutaVenta/Programacion/Add")
    Call<Object> post(@Body Object sellerRoute);
}
