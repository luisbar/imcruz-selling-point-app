package com.imcruzparts.pdv.data.home.specification;

import com.imcruzparts.pdv.data.home.model.PdvModelLocal;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SendClients {

    @POST("PuntoDeVenta/UpList")
    Call<Void> post(@Body List<PdvModelLocal> pPDVList);
}
