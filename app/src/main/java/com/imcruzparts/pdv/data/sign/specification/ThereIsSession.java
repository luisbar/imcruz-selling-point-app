package com.imcruzparts.pdv.data.sign.specification;


import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.sign.model.SessionModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene la tupla con fechaHoraFin == null, esto significa que hay
 * una sesión activa por que no se vencio el tiempo de validez
 * del token
 */
public class ThereIsSession implements RealmSpecification {

    private Realm mRealm;

    public ThereIsSession(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {

        return mRealm.where(SessionModelLocal.class)
                .isNull("FechaHoraFin").findAll();
    }
}
