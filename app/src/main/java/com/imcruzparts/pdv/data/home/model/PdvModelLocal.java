package com.imcruzparts.pdv.data.home.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PdvModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdPDV;
    private ClientModelLocal IdCliente;
    private String Nombre;
    private String Departamento;
    private String Direccion;
    private Double Latitud;
    private Double Longitud;
    private String Estado;
    private String FechaAlta;
    private String FechaBaja;

    public PdvModelLocal() {
    }

    public PdvModelLocal(Integer idPDV, ClientModelLocal idCliente, String nombre, String departamento, String direccion, Double latitud, Double longitud, String estado, String fechaAlta, String fechaBaja) {
        IdPDV = idPDV;
        IdCliente = idCliente;
        Nombre = nombre;
        Departamento = departamento;
        Direccion = direccion;
        Latitud = latitud;
        Longitud = longitud;
        Estado = estado;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
    }

    public Integer getIdPDV() {
        return IdPDV;
    }

    public void setIdPDV(Integer idPDV) {
        IdPDV = idPDV;
    }

    public ClientModelLocal getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(ClientModelLocal idCliente) {
        IdCliente = idCliente;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String departamento) {
        Departamento = departamento;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public Double getLatitud() {
        return Latitud;
    }

    public void setLatitud(Double latitud) {
        Latitud = latitud;
    }

    public Double getLongitud() {
        return Longitud;
    }

    public void setLongitud(Double longitud) {
        Longitud = longitud;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }
}
