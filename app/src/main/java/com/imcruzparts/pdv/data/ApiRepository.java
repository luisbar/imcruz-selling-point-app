package com.imcruzparts.pdv.data;

/**
 * Iterface a ser implementada por las clases que accedan a la api
 * @param <Item> pojo
 * @param <Id> identificador
 * @param <Ids> identificadores
 */
public interface ApiRepository<Item, Id, Ids> {

    /**
     * Modifica los datos si el item existe, caso contrario inserta uno nuevo
     * @param item objeto a ser guardado en la nube mediante la api
     */
    void patch(Item item);

    /**
     * Obtiene el item especificado
     * @param id id del objeto a ser obtenido de la nube mediante la api
     */
    void getOne(Id id);

    /**
     * Obtiene los items especificados
     */
    void getMany();

    /**
     * Inserta uno nuevo
     * @param item objeto a ser guardado en la nube mediante la api
     */
    void post(Item item);

    /**
     * Elimina el item especificado
     * @param id id del objeto a ser eliminado de la nube mediante la api
     */
    void deleteOne(Id id);
}
