package com.imcruzparts.pdv.data.request.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Obtiene los pedidos de la ruta actualmente iniciada
 */
public class GetPendingRequest  implements RealmSpecification {

    private Realm mRealm;

    public GetPendingRequest(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {

        RealmResults<ActivityModelLocal> lsActivities = mRealm.where(ActivityModelLocal.class)
                .equalTo("RutaVendedor.Estado", "INICIADA").findAll();
        // Si lsActivities esa vacio crea un array con longitud = 1 por que
        // la consulta in() debe recibir un array con al menos un dato
        Integer[] lstIdActivities = new Integer[lsActivities.isEmpty() ? 1 : lsActivities.size()];
        lstIdActivities[0] = new Integer(0);

        int i = 0;
        for (ActivityModelLocal activity : lsActivities)
            lstIdActivities[i++] = activity.getIdActividad();

        return mRealm.where(RequestModelLocal.class)
                .in("IdActividad", lstIdActivities).findAll();
    }
}
