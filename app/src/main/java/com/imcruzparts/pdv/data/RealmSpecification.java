package com.imcruzparts.pdv.data;

import com.imcruzparts.pdv.data.sign.specification.ThereIsSession;

import io.realm.RealmResults;

/**
 * Interfaz a ser implementada por todas las clases que tengan
 * un criterio o especiicacion diferente de consultas a la base de datos
 * local, ejemplo {@link ThereIsSession}
 */
public interface RealmSpecification {

    RealmResults toRealmResult();
}
