package com.imcruzparts.pdv.data.home.specification;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetClientsFromCloud {

    @GET("PuntoDeVenta/Page/1")
    Call<Object> get();
}
