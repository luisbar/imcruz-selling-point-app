package com.imcruzparts.pdv.data.request;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.data.product.model.ProductModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestDetailModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.data.request.specification.GetPendingRequest;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class RequestDatabaseRepository implements DatabaseRepository<RequestDatabaseRepository.Params, Object> {

    private Realm mRealm;

    /**
     * Agrega un nuevo pedido a la base de datos local
     * @param item objeto a guardar
     */
    @Override
    public void add(final Params item) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                int id = realm.where(RequestModelLocal.class).max("IdPedido") != null
                        ? realm.where(RequestModelLocal.class).max("IdPedido").intValue() + 1
                        : 1;
                RequestModelLocal requestHeader = realm.createObject(RequestModelLocal.class, id);
                ActivityModelLocal activity = realm.where(ActivityModelLocal.class)
                        .equalTo("IdActividad", item.idActivity).findFirst();

                requestHeader.setIdPdv(item.idSellingPoint);
                requestHeader.setIdActividad(item.idActivity);
                requestHeader.setFecha(new Date().toString());
                requestHeader.setEstado("PENDIENTE");
                requestHeader.setSubTotal(item.subTotal);
                requestHeader.setDescuento(item.discount);
                requestHeader.setMontoTotal(item.totalAmount);

                RealmList<RequestDetailModelLocal> detail = new RealmList();
                for (int i = 0; i < item.productsId.size() ; i++) {
                    id = realm.where(RequestDetailModelLocal.class).max("IdDetalle") != null
                            ? realm.where(RequestDetailModelLocal.class).max("IdDetalle").intValue() + 1
                            : 1;
                    RequestDetailModelLocal requestDetail = realm.createObject(RequestDetailModelLocal.class, id);
                    ProductModelLocal product = realm.where(ProductModelLocal.class)
                            .equalTo("IdProducto", item.productsId.get(i)).findFirst();

                    requestDetail.setIdPedido(requestHeader.getIdPedido());
                    requestDetail.setProducto(product);
                    requestDetail.setCantidad(item.quantityByProducts.get(i));
                    requestDetail.setPrecio(product.getPrecio());
                    requestDetail.setDescuento(product.getDescuento());
                    requestDetail.setSubTotal(product.getPrecio() * item.quantityByProducts.get(i));
                    requestDetail.setFechaAlta(new Date().toString());

                    detail.add(requestDetail);
                }

                requestHeader.setDetalle(detail);
                activity.getPedidos().add(requestHeader);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mRealm.close();
                queryOne();
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                EventBusMask.post(new Action(ActionType.REQUEST_NOT_ADDED_P));
                error.printStackTrace();
            }
        });

    }

    @Override
    public void addMany(String jsonArray) {

    }

    /**
     * Metodo que actualiza el estado de un pedido y agrega observación a un pedido
     * @param item campo a actualizar
     */
    @Override
    public void update(final Params item) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
               RequestModelLocal request = realm.where(RequestModelLocal.class)
                       .equalTo("IdPedido", item.idRequest)
                       .findFirst();
               String date = new Date().toString();

               switch (item.fieldToUpdate) {

                   case "state":
                       request.setEstado(item.value);
                       request.setFecha(date);
                       break;

                   case "note":
                       request.setObservacion(item.value);
                       break;
               }
            }
       },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mRealm.close();
                switch (item.fieldToUpdate) {

                    case "state":
                        EventBusMask.post(new Action(ActionType.REQUEST_DELIVERED_P));
                        break;

                    case "note":
                        EventBusMask.post(new Action(ActionType.NOTE_ADDED_TO_REQUEST_P));
                        break;
                }
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                switch (item.fieldToUpdate) {

                    case "state":
                        EventBusMask.post(new Action(ActionType.REQUEST_NOT_DELIVERED_P));
                        break;

                    case "note":
                        EventBusMask.post(new Action(ActionType.NOTE_NOT_ADDED_TO_REQUEST_P));
                        break;
                }
                error.printStackTrace();
            }
        });
    }

    @Override
    public void remove(String specification) {

    }

    @Override
    public void queryOne(String specification) {

    }

    /**
     * Obtiene el ultimo pedido guardado
     */
    @Override
    public Object queryOne() {
        mRealm = Realm.getDefaultInstance();

        EventBusMask.post(new Action(ActionType.REQUEST_ADDED_P, mRealm.where(RequestModelLocal.class)
                .findAllSorted("IdPedido", Sort.DESCENDING)
                .get(0)));

        mRealm.close();
        return null;
    }

    /**
     * Obtiene los pedidos de la ruta actualmente iniciada
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de la consulta a realizar
     */
    @Override
    public void queryMany(String specification) {
        mRealm = Realm.getDefaultInstance();

        GetPendingRequest getPendingRequest = new GetPendingRequest(mRealm);
        RealmResults<RequestModelLocal> pendingRequest = getPendingRequest.toRealmResult();

        if (!pendingRequest.isEmpty())
            EventBusMask.post(new Action(ActionType.PENDING_REQUEST_OBTAINED_P, pendingRequest.subList(0, pendingRequest.size())));

        mRealm.close();
    }

    public static class Params {

        private int idSellingPoint;
        private int idActivity;
        private double subTotal;
        private double discount;
        private double totalAmount;
        private List<Integer> productsId;
        private List<Integer> quantityByProducts;
        // Para actualizacion de un pedido
        private int idRequest;
        private String fieldToUpdate;
        private String value;

        public Params(int idSellingPoint, int idActivity, double subTotal, double discount, double totalAmount, List<Integer> productsId, List<Integer> quantityByProducts) {
            this.idSellingPoint = idSellingPoint;
            this.idActivity = idActivity;
            this.subTotal = subTotal;
            this.discount = discount;
            this.totalAmount = totalAmount;
            this.productsId = productsId;
            this.quantityByProducts = quantityByProducts;
        }

        public Params(int idRequest, String fieldToUpdate, String value) {
            this.idRequest = idRequest;
            this.fieldToUpdate = fieldToUpdate;
            this.value = value;
        }

        public int getIdSellingPoint() {
            return idSellingPoint;
        }

        public void setIdSellingPoint(int idSellingPoint) {
            this.idSellingPoint = idSellingPoint;
        }

        public int getIdActivity() {
            return idActivity;
        }

        public void setIdActivity(int idActivity) {
            this.idActivity = idActivity;
        }

        public double getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(double subTotal) {
            this.subTotal = subTotal;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public double getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(double totalAmount) {
            this.totalAmount = totalAmount;
        }

        public List<Integer> getProductsId() {
            return productsId;
        }

        public void setProductsId(List<Integer> productsId) {
            this.productsId = productsId;
        }

        public static Params paramsForAdd(int idSellingPoint, int idActivity, double subTotal, double discount, double totalAmount, List<Integer> productsId, List<Integer> quantityByProducts) {
            return new Params(idSellingPoint, idActivity, subTotal, discount, totalAmount,
                    productsId, quantityByProducts);
        }

        public static Params paramsForUpdate(int idRequest, String fieldToUpdate, String value) {
            return new Params(idRequest, fieldToUpdate, value);
        }
    }
}
