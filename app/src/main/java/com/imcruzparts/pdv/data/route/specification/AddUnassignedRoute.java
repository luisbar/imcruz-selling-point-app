package com.imcruzparts.pdv.data.route.specification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AddUnassignedRoute {

    @POST("RutaPDV/Add")
    Call<Object> post(@Body Object routeUnassigned);
}
