package com.imcruzparts.pdv.data.sellerroute.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Obtiene las rutas pendientes de la base de datos local
 */
public class GetPendingRoutes implements RealmSpecification {

    private Realm mRealm;

    public GetPendingRoutes(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(SellerRouteModelLocal.class)
                .equalTo("Estado", "PENDIENTE").findAll()
                .sort("FechaProgramacion", Sort.DESCENDING);
    }
}
