package com.imcruzparts.pdv.data.request.model;

import com.imcruzparts.pdv.data.product.model.ProductModelLocal;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RequestDetailModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdDetalle;
    private Integer IdPedido;
    private ProductModelLocal Producto;
    private Integer Cantidad;
    private Double Precio;
    private Double Descuento;
    private Double SubTotal;
    private String FechaAlta;
    private String FechaBaja;

    public RequestDetailModelLocal() {
    }

    public RequestDetailModelLocal(Integer idDetalle, Integer idPedido, ProductModelLocal producto, Integer cantidad, Double precio, Double descuento, Double subTotal, String fechaAlta, String fechaBaja) {
        IdDetalle = idDetalle;
        IdPedido = idPedido;
        Producto = producto;
        Cantidad = cantidad;
        Precio = precio;
        Descuento = descuento;
        SubTotal = subTotal;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
    }

    public Integer getIdDetalle() {
        return IdDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        IdDetalle = idDetalle;
    }

    public Integer getIdPedido() {
        return IdPedido;
    }

    public void setIdPedido(Integer idPedido) {
        IdPedido = idPedido;
    }

    public ProductModelLocal getProducto() {
        return Producto;
    }

    public void setProducto(ProductModelLocal producto) {
        Producto = producto;
    }

    public Integer getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Integer cantidad) {
        Cantidad = cantidad;
    }

    public Double getPrecio() {
        return Precio;
    }

    public void setPrecio(Double precio) {
        Precio = precio;
    }

    public Double getDescuento() {
        return Descuento;
    }

    public void setDescuento(Double descuento) {
        Descuento = descuento;
    }

    public Double getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(Double subTotal) {
        SubTotal = subTotal;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }
}
