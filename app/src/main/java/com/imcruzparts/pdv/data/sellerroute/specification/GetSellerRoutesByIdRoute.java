package com.imcruzparts.pdv.data.sellerroute.specification;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetSellerRoutesByIdRoute {

    @GET("RutaVenta/Programacion/{idRoute}")
    Call<Object> get(@Path("idRoute") int idRoute);
}
