package com.imcruzparts.pdv.data.param.specification;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Para traer los parametros de la nube
 */
public interface GetParam {

    @GET("Parametros")
    Call<Object> get();
}
