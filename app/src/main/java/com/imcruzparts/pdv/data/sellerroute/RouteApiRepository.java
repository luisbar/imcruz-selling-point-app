package com.imcruzparts.pdv.data.sellerroute;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.common.NetDateTimeAdapter;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.sellerroute.specification.AddSellerRoute;
import com.imcruzparts.pdv.data.sellerroute.specification.GetRoutesFromCloud;
import com.imcruzparts.pdv.data.sellerroute.specification.GetSellerRoutesByIdRoute;
import com.imcruzparts.pdv.data.sellerroute.specification.RemoveSellerRoute;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteApiRepository implements ApiRepository {

    private final int OK = 200;
    private final int BAD_REQUEST = 400;
    private final int SESSION_EXPIRED = 401;
    private final int FORBIDDEN = 403;

    private int idRoute;
    private Object lstSellers;
    private boolean getAllRoutes;

    public RouteApiRepository() {
    }

    public RouteApiRepository(int idRoute, Object lstSellers, boolean getAllRoutes) {
        this.idRoute = idRoute;
        this.lstSellers = lstSellers;
        this.getAllRoutes = getAllRoutes;
    }

    @Override
    public void patch(Object o) {

    }

    @Override
    public void getOne(Object o) {

    }

    /**
     * Trae todas las rutas correspondientes al usuario logueado desde la nube
     */
    @Override
    public void getMany() {
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        if (getAllRoutes)
            getRoutesByDistributor(token);
        else
            getRoutesBySeller(token);

    }

    /**
     * Obtiene todas las rutas vendedor por id ruta
     */
    private void getRoutesByDistributor(String token) {
        GetSellerRoutesByIdRoute getSellerRoutesByIdRoute = ServiceGenerator.createService(GetSellerRoutesByIdRoute.class,
                "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/",
                token);
        Call call = getSellerRoutesByIdRoute.get(idRoute);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(
                                ActionType.ROUTES_PROGRAMMED_P,
                                Arrays.asList(
                                        lstSellers,
                                        response.body()
                                ),
                                false
                        ));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    /**
     * Obtiene todas las rutas de un vendedor
     */
    private void getRoutesBySeller(String token) {
        GetRoutesFromCloud getRoutesFromCloud = ServiceGenerator.createService(GetRoutesFromCloud.class,
                "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/",
                token);
        Call call = getRoutesFromCloud.get();

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        DatabaseRepository mRouteDatabaseRepository;
                        mRouteDatabaseRepository = new RouteDatabaseRepository();

                        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new NetDateTimeAdapter()).create();

                        mRouteDatabaseRepository.addMany(gson.toJson(response.body()));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.PENDING_ROUTES_NOT_OBTAINED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.PENDING_ROUTES_NOT_OBTAINED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.PENDING_ROUTES_NOT_OBTAINED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void post(Object o) {
        // Se obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Se hace la petición
        AddSellerRoute addSellerRoute = ServiceGenerator.createServiceWithDateFormatter(AddSellerRoute.class,
                "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/",
                token);
        Call call = addSellerRoute.post(o);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(ActionType.SELLER_ROUTE_ADDED_P, response.body(), false));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.SELLER_ROUTE_ADDED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.SELLER_ROUTE_ADDED_P, R.string.txt_73, true));
                        break;

                    case FORBIDDEN:
                        try {
                            String json = response.errorBody().string();
                            Gson gson = new Gson();
                            Type type = new TypeToken<Forbidden>() {}.getType();
                            Forbidden forbidden = gson.fromJson(json, type);

                            EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, forbidden.getMensaje(), true));
                        } catch (IOException e) {
                            EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, R.string.txt_8, true));
                            e.printStackTrace();
                        }
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SELLER_ROUTE_ADDED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void deleteOne(Object o) {
        // Se obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Se hace la petición
        RemoveSellerRoute removeSellerRoute = ServiceGenerator.createServiceWithDateFormatter(
                RemoveSellerRoute.class,
                "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/",
                token
        );
        Call call = removeSellerRoute.post(o);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, null, false));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, R.string.txt_73, true));
                        break;

                    case FORBIDDEN:
                        try {
                            String json = response.errorBody().string();
                            Gson gson = new Gson();
                            Type type = new TypeToken<Forbidden>() {}.getType();
                            Forbidden forbidden = gson.fromJson(json, type);

                            EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, forbidden.getMensaje(), true));
                        } catch (IOException e) {
                            EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, R.string.txt_8, true));
                            e.printStackTrace();
                        }
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SELLER_ROUTE_REMOVED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    private class Forbidden {

        private String Mensaje;

        public String getMensaje() {
            return Mensaje;
        }
    }
}
