package com.imcruzparts.pdv.data.activity.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.param.model.ParamModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene todos los parametros que pertenecen al grupo actividad
 */
public class GetActivitiesFromParam implements RealmSpecification {

    private Realm mRealm;

    public GetActivitiesFromParam(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(ParamModelLocal.class)
                .equalTo("Grupo", "ACTIVIDADES").findAll();
    }
}
