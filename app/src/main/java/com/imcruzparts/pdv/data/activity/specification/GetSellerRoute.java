package com.imcruzparts.pdv.data.activity.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene la ruta de vendedor con el id especificado
 */
public class GetSellerRoute implements RealmSpecification {

    private Realm mRealm;
    private int idSellerRoute;

    public GetSellerRoute(Realm mRealm, int idSellerRoute) {
        this.mRealm = mRealm;
        this.idSellerRoute = idSellerRoute;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(SellerRouteModelLocal.class)
                .equalTo("IdRutaVendedor", idSellerRoute).findAll();
    }
}
