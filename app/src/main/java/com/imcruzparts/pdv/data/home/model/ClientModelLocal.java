package com.imcruzparts.pdv.data.home.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ClientModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdCliente;
    private String Nombre;
    private String Apellido;
    private String NroDocumento;
    private String Celular;
    private String Telefono;
    private String Email;
    private String Estado;
    private String RazonSocial;
    private String Nit;
    private String FechaAlta;
    private String FechaBaja;

    public ClientModelLocal() {
    }

    public ClientModelLocal(Integer idCliente, String nombre, String apellido, String nroDocumento, String celular, String telefono, String email, String estado, String razonSocial, String nit, String fechaAlta, String fechaBaja) {
        IdCliente = idCliente;
        Nombre = nombre;
        Apellido = apellido;
        NroDocumento = nroDocumento;
        Celular = celular;
        Telefono = telefono;
        Email = email;
        Estado = estado;
        RazonSocial = razonSocial;
        Nit = nit;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
    }

    public Integer getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(Integer idCliente) {
        IdCliente = idCliente;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getNroDocumento() {
        return NroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        NroDocumento = nroDocumento;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        RazonSocial = razonSocial;
    }

    public String getNit() {
        return Nit;
    }

    public void setNit(String nit) {
        Nit = nit;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }
}
