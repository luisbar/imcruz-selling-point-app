package com.imcruzparts.pdv.data.main;

import com.google.gson.Gson;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.main.specification.SignOut;
import com.imcruzparts.pdv.data.sign.SignDatabaseRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainApiRepository implements ApiRepository {
    @Override
    public void patch(Object o) {

    }

    @Override
    public void getOne(Object o) {

    }

    @Override
    public void getMany() {

    }

    /**
     * Metodo que cierra la sesión
     * @param o objecto a enviar al servidor
     */
    @Override
    public void post(Object o) {
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        SignOut signOut = ServiceGenerator.createService(SignOut.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = signOut.post();

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200) {
                    DatabaseRepository mSignDatabaseRepository;
                    mSignDatabaseRepository = new SignDatabaseRepository();
                    Gson gson = new Gson();

                    mSignDatabaseRepository.update(gson.toJson(response.body()));
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SIGN_OUT_ERROR_P));
            }
        });
    }

    @Override
    public void deleteOne(Object o) {

    }
}
