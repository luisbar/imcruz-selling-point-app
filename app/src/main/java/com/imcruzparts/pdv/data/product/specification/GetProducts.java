package com.imcruzparts.pdv.data.product.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.product.model.ProductModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Clase que obtiene todos los productos de la base de datos
 */
public class GetProducts implements RealmSpecification {

    private Realm mRealm;

    public GetProducts(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(ProductModelLocal.class).findAll();
    }
}
