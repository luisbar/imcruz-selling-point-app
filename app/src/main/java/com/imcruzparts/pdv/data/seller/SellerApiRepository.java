package com.imcruzparts.pdv.data.seller;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.seller.specification.GetSellers;
import com.imcruzparts.pdv.data.sellerroute.RouteApiRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerApiRepository implements ApiRepository {

    private final int OK = 200;
    private final int BAD_REQUEST = 400;
    private final int SESSION_EXPIRED = 401;

    private int idRoute;

    public SellerApiRepository(int idRoute) {
        this.idRoute = idRoute;
    }

    @Override
    public void patch(Object o) {

    }

    @Override
    public void getOne(Object o) {

    }

    @Override
    public void getMany() {
        // Obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Hace la petición
        GetSellers getSellers = ServiceGenerator.createService(GetSellers.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = getSellers.get();

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        new RouteApiRepository(idRoute, response.body(), true).getMany();
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.ROUTES_PROGRAMMED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void post(Object o) {

    }

    @Override
    public void deleteOne(Object o) {

    }
}
