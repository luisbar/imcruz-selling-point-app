package com.imcruzparts.pdv.data.home;

import android.util.Log;

import com.google.gson.Gson;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.Pagination;
import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.home.model.ClientModelLocal;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.data.home.specification.GetClients;
import com.imcruzparts.pdv.data.home.specification.GetToken;
import com.imcruzparts.pdv.data.sign.model.SessionModelLocal;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Clase que se encarga de gestionar la tabla {@link ClientModelLocal} y
 * {@link PdvModelLocal}
 */
public class HomeDatabaseRepository extends Pagination implements DatabaseRepository<HomeDatabaseRepository.Params, String> {

    private Realm mRealm;
    private final String TAG = getClass().getCanonicalName();
    private boolean paginated;// Para paginar los datos

    public HomeDatabaseRepository(boolean paginated) {
        super(4);
        this.paginated = paginated;
    }

    /**
     * Guarda un cliente en la base de datos local
     * @param item objeto a guardar
     */
    @Override
    public void add(final Params item) {

        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                int id = realm.where(ClientModelLocal.class).max("IdCliente") != null
                        ? realm.where(ClientModelLocal.class).max("IdCliente").intValue() + 1
                        : 1;
                ClientModelLocal cliente = realm.createObject(ClientModelLocal.class, id);

                cliente.setNombre(item.cliente.Nombre);
                cliente.setApellido(item.cliente.Apellido);
                cliente.setNroDocumento(item.cliente.NroDocumento);
                cliente.setCelular(item.cliente.Celular);
                cliente.setTelefono(item.cliente.Telefono);
                cliente.setEmail(item.cliente.Email);
                cliente.setEstado(item.cliente.Estado);
                cliente.setRazonSocial(item.cliente.RazonSocial);
                cliente.setNit(item.cliente.Nit);
                cliente.setFechaAlta(item.cliente.FechaAlta);

                id = realm.where(PdvModelLocal.class).max("IdPDV") != null
                        ? realm.where(PdvModelLocal.class).max("IdPDV").intValue() + 1
                        : 1;
                PdvModelLocal pdv = realm.createObject(PdvModelLocal.class, id);

                pdv.setIdCliente(cliente);
                pdv.setNombre(item.Nombre);
                pdv.setDepartamento(item.Departamento);
                pdv.setDireccion(item.Direccion);
                pdv.setLatitud(item.Latitud);
                pdv.setLongitud(item.Longitud);
                pdv.setEstado(item.Estado);
                pdv.setFechaAlta(item.FechaAlta);
                pdv.setFechaBaja(item.FechaBaja);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e(TAG, "onSuccess");
                EventBusMask.post(new Action(ActionType.ADD_CLIENT_SUCCESS_P));
                mRealm.close();
                new HomeApiRepository().post(null);
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "onError", error);
                EventBusMask.post(new Action(ActionType.ADD_CLIENT_ERROR_P));
                mRealm.close();
            }
        });
    }

    /**
     * Guarda muchos clientes en la base de datos local
     * @param jsonArray objtetos a guardar
     */
    @Override
    public void addMany(final String jsonArray) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.createOrUpdateAllFromJson(PdvModelLocal.class, jsonArray);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBusMask.post(new Action(ActionType.CLIENTS_DOWNLOADED_P));
                mRealm.close();
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                EventBusMask.post(new Action(ActionType.CLIENTS_DOWNLOADED_P, R.string.txt_69, true));
                error.printStackTrace();
            }
        });
    }

    /**
     * Actualiza un cliente en la base de datos local
     * @param item objeto a actualizar
     */
    @Override
    public void update(final Params item) {
        mRealm = Realm.getDefaultInstance();
        Gson gson = new Gson();
        final String pdvUpdated = gson.toJson(item);
        final String clientUpdated = gson.toJson(item.cliente);

        mRealm.executeTransactionAsync(new Realm.Transaction() {
           @Override
           public void execute(Realm realm) {
               realm.createOrUpdateObjectFromJson(PdvModelLocal.class, pdvUpdated);
               realm.createOrUpdateObjectFromJson(ClientModelLocal.class, clientUpdated);
           }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBusMask.post(new Action(ActionType.UPDATE_CLIENT_SUCCESS_P));
                mRealm.close();
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                EventBusMask.post(new Action(ActionType.UPDATE_CLIENT_ERROR_P));
                mRealm.close();
            }
        });
    }

    @Override
    public void remove(String specification) {

    }

    @Override
    public void queryOne(String specification) {

    }

    /**
     * Verifica si hay token activo
     * @return token activo si es que hubiese, caso contrario devuelve null
     */
    @Override
    public String queryOne() {
        mRealm = Realm.getDefaultInstance();
        RealmSpecification mGetToken = new GetToken(mRealm);
        RealmResults<SessionModelLocal> results = mGetToken.toRealmResult();

        return !results.isEmpty() ? results.get(0).getIdSesion() : null;
    }

    /**
     * Obtiene todos los clientes de la base de datos local
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de la consulta a realizar
     */
    @Override
    public void queryMany(String specification) {
        mRealm = Realm.getDefaultInstance();
        GetClients getClients = new GetClients(mRealm);
        List<PdvModelLocal> lstClients;

        if (paginated)
            lstClients = paginate(getClients.toRealmResult());
        else
            lstClients = getClients.toRealmResult().subList(0, getClients.toRealmResult().size());

        if (!lstClients.isEmpty())
            EventBusMask.post(new Action(ActionType.CLIENTS_OBTAINED_P,
                lstClients, false));

        mRealm.close();
    }

    public static class Params {

        private Integer IdPDV;
        private String Nombre;
        private String Departamento;
        private String Direccion;
        private Double Latitud;
        private Double Longitud;
        private String Estado;
        private String FechaAlta;
        private String FechaBaja;
        private Params.Cliente cliente;

        public Params(String nombre, String departamento, String direccion,
                      Double latitud, Double longitud, String estado,
                      String fechaAlta, String fechaBaja,
                      String nombreCliente, String apellido, String nroDocumento, String celular,
                      String telefono, String email, String estadoCliente, String razonSocial,
                      String nit, String fechaAltaCliente, String fechaBajaCliente) {
            Nombre = nombre;
            Departamento = departamento;
            Direccion = direccion;
            Latitud = latitud;
            Longitud = longitud;
            Estado = estado;
            FechaAlta = fechaAlta;
            FechaBaja = fechaBaja;
            cliente = new Cliente(nombreCliente, apellido, nroDocumento, celular,
                    telefono, email, estadoCliente, razonSocial, nit, fechaAltaCliente,
                    fechaBajaCliente);
        }

        public Params(Integer idPDV, String nombre, String departamento, String direccion,
                      Double latitud, Double longitud, String estado, String fechaAlta,
                      String fechaBaja, Integer idCliente, String nombreCliente, String apellido,
                      String nroDocumento, String celular, String telefono, String email,
                      String estadoCliente, String razonSocial, String nit, String fechaAltaCliente,
                      String fechaBajaCliente) {
            IdPDV = idPDV;
            Nombre = nombre;
            Departamento = departamento;
            Direccion = direccion;
            Latitud = latitud;
            Longitud = longitud;
            Estado = estado;
            FechaAlta = fechaAlta;
            FechaBaja = fechaBaja;
            this.cliente = new Cliente(idCliente, nombreCliente, apellido, nroDocumento,
                    celular, telefono, email, estadoCliente, razonSocial, nit,
                    fechaAltaCliente, fechaBajaCliente);
        }

        public static Params forAdd(String nombre, String departamento, String direccion,
                                    Double latitud, Double longitud, String estado,
                                    String fechaAlta, String fechaBaja,
                                    String nombreCliente, String apellido, String nroDocumento, String celular,
                                    String telefono, String email, String estadoCliente, String razonSocial,
                                    String nit, String fechaAltaCliente, String fechaBajaCliente) {
            return new Params(
                    nombre,
                    departamento,
                    direccion,
                    latitud,
                    longitud,
                    estado,
                    fechaAlta,
                    fechaBaja,
                    nombreCliente,
                    apellido,
                    nroDocumento,
                    celular,
                    telefono,
                    email,
                    estadoCliente,
                    razonSocial,
                    nit,
                    fechaAltaCliente,
                    fechaBajaCliente

            );
        }

        public static Params forUpdate(Integer idPDV, String nombre, String departamento, String direccion,
                                       Double latitud, Double longitud, String estado, String fechaAlta,
                                       String fechaBaja, Integer idCliente, String nombreCliente, String apellido,
                                       String nroDocumento, String celular, String telefono, String email,
                                       String estadoCliente, String razonSocial, String nit, String fechaAltaCliente,
                                       String fechaBajaCliente) {
            return new Params(
                    idPDV,
                    nombre,
                    departamento,
                    direccion,
                    latitud,
                    longitud,
                    estado,
                    fechaAlta,
                    fechaBaja,
                    idCliente,
                    nombreCliente,
                    apellido,
                    nroDocumento,
                    celular,
                    telefono,
                    email,
                    estadoCliente,
                    razonSocial,
                    nit,
                    fechaAltaCliente,
                    fechaBajaCliente
            );
        }

        public class Cliente {

            private Integer IdCliente;
            private String Nombre;
            private String Apellido;
            private String NroDocumento;
            private String Celular;
            private String Telefono;
            private String Email;
            private String Estado;
            private String RazonSocial;
            private String Nit;
            private String FechaAlta;
            private String FechaBaja;

            public Cliente(Integer idCliente, String nombre, String apellido, String nroDocumento, String celular, String telefono, String email, String estado, String razonSocial, String nit, String fechaAlta, String fechaBaja) {
                IdCliente = idCliente;
                Nombre = nombre;
                Apellido = apellido;
                NroDocumento = nroDocumento;
                Celular = celular;
                Telefono = telefono;
                Email = email;
                Estado = estado;
                RazonSocial = razonSocial;
                Nit = nit;
                FechaAlta = fechaAlta;
                FechaBaja = fechaBaja;
            }

            public Cliente(String nombre, String apellido, String nroDocumento, String celular, String telefono, String email, String estado, String razonSocial, String nit, String fechaAlta, String fechaBaja) {
                Nombre = nombre;
                Apellido = apellido;
                NroDocumento = nroDocumento;
                Celular = celular;
                Telefono = telefono;
                Email = email;
                Estado = estado;
                RazonSocial = razonSocial;
                Nit = nit;
                FechaAlta = fechaAlta;
                FechaBaja = fechaBaja;
            }
        }
    }
}
