package com.imcruzparts.pdv.data.param;

import com.google.gson.Gson;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.param.specification.GetParam;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParamApiRepository implements ApiRepository {

    private DatabaseRepository mParamDatabaseRepository;

    public ParamApiRepository(String employeeLogged) {
        this.mParamDatabaseRepository = new ParamDatabaseRepository(employeeLogged);
    }

    @Override
    public void patch(Object o) {

    }

    @Override
    public void getOne(Object o) {

    }

    /**
     * Trae todos los parametros de la nube, e invoca a clase pa guardarlos
     * en la base de datos local
     */
    @Override
    public void getMany() {
        GetParam getParam = ServiceGenerator.createService(GetParam.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/");
        Call call = getParam.get();

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200) {
                    Gson gson = new Gson();
                    String item = gson.toJson(response.body());

                    mParamDatabaseRepository.addMany(item);
                } else
                    EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_P, R.string.txt_8));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_P, R.string.txt_8));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void post(Object o) {

    }

    @Override
    public void deleteOne(Object o) {

    }
}
