package com.imcruzparts.pdv.data;

/**
 * Metodos genericos para acceso a la base de datos local
 * @param <ToSave> objeto a guardar
 */
public interface DatabaseRepository<ToSave, ToReturn> {

    /**
     * Guarda un objeto en la base de datos local
     * @param item objeto a guardar
     */
    void add(ToSave item);

    /**
     * Guarda muchos objetos en la base de datos local
     * @param jsonArray objtetos a guardar
     */
    void addMany(String jsonArray);

    /**
     * Actualiza un objeto en la base de datos
     * @param item objeto a actualizar
     */
    void update(ToSave item);

    /**
     * Remueve un objeto de la base de datos
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de eliminación que se hara
     */
    void remove(String specification);

    /**
     * Obtiene un objeto de la base de datos local
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de la consulta a realizar
     */
    void queryOne(String specification);

    /**
     * Obtiene un objeto de la base de datos local
     */
     ToReturn queryOne();

    /**
     * Obtiene una lista de objetos de la base de datos local
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de la consulta a realizar
     * @return lista de objetos
     */
    void queryMany(String specification);
}
