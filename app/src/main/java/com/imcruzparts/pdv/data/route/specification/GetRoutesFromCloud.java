package com.imcruzparts.pdv.data.route.specification;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetRoutesFromCloud {

    @GET("RutaVenta")
    Call<Object> get();
}
