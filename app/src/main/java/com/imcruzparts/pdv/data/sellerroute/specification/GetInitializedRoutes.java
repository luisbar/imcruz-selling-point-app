package com.imcruzparts.pdv.data.sellerroute.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene la ruta con estado iniciada de la base de datos local
 */
public class GetInitializedRoutes implements RealmSpecification {

    private Realm mRealm;

    public GetInitializedRoutes(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(SellerRouteModelLocal.class)
                .equalTo("Estado", "INICIADA")
                .findAll();
    }
}
