package com.imcruzparts.pdv.data.sign.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SessionModelLocal extends RealmObject {

    @PrimaryKey
    private String IdSesion;
    private String FechaHoraFin;
    private String FechaHoraInicio;
    private Integer IdUsuario;
    private String Rol;
    private Integer Ttl;
    private String Usuario;

    public SessionModelLocal() {
    }

    public SessionModelLocal(String idSesion, String fechaHoraFin, String fechaHoraInicio, Integer idUsuario, String rol, Integer ttl, String usuario) {
        IdSesion = idSesion;
        FechaHoraFin = fechaHoraFin;
        FechaHoraInicio = fechaHoraInicio;
        IdUsuario = idUsuario;
        Rol = rol;
        Ttl = ttl;
        Usuario = usuario;
    }

    public String getIdSesion() {
        return IdSesion;
    }

    public void setIdSesion(String idSesion) {
        IdSesion = idSesion;
    }

    public String getFechaHoraFin() {
        return FechaHoraFin;
    }

    public void setFechaHoraFin(String fechaHoraFin) {
        FechaHoraFin = fechaHoraFin;
    }

    public String getFechaHoraInicio() {
        return FechaHoraInicio;
    }

    public void setFechaHoraInicio(String fechaHoraInicio) {
        FechaHoraInicio = fechaHoraInicio;
    }

    public Integer getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getRol() {
        return Rol;
    }

    public void setRol(String rol) {
        Rol = rol;
    }

    public Integer getTtl() {
        return Ttl;
    }

    public void setTtl(Integer ttl) {
        Ttl = ttl;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }
}
