package com.imcruzparts.pdv.data.sellerroute.specification;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Obtiene todas las rutas correspondientes al usuario logueado desde la nube
 */
public interface GetRoutesFromCloud {

    @GET("RutaVenta/Vendedor")
    Call<Object> get();
}
