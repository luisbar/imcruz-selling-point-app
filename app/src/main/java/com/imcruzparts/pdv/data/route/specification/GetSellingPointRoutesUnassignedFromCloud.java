package com.imcruzparts.pdv.data.route.specification;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetSellingPointRoutesUnassignedFromCloud {

    @GET("RutaPDV/Unassigned/{idRoute}")
    Call<Object> get(@Path("idRoute") int idRoute);
}
