package com.imcruzparts.pdv.data.sellerroute.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RouteModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdRuta;
    private String Nombre;
    private String Descripcion;
    private String Departamento;
    private String Zona;
    private String FechaAlta;
    private String FechaBaja;

    public RouteModelLocal() {
    }

    public RouteModelLocal(Integer idRuta, String nombre, String descripcion, String departamento, String zona, String fechaAlta, String fechaBaja) {
        IdRuta = idRuta;
        Nombre = nombre;
        Descripcion = descripcion;
        Departamento = departamento;
        Zona = zona;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
    }

    public Integer getIdRuta() {
        return IdRuta;
    }

    public void setIdRuta(Integer idRuta) {
        IdRuta = idRuta;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String departamento) {
        Departamento = departamento;
    }

    public String getZona() {
        return Zona;
    }

    public void setZona(String zona) {
        Zona = zona;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }
}
