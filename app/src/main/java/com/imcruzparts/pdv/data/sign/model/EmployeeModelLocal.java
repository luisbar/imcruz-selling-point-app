package com.imcruzparts.pdv.data.sign.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class EmployeeModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdEmpleado;
    private String Apellido;
    private String Direccion;
    private String Email;
    private String FechaAlta;
    private String FechaBaja;
    private String Nombre;
    private String Rol;
    private String Telefono;
    private DistributorModelLocal Distribuidor;
    private SessionModelLocal Sesion;


    public EmployeeModelLocal() {
    }

    public EmployeeModelLocal(Integer idEmpleado, String apellido, String direccion, String email, String fechaAlta, String fechaBaja, String nombre, String rol, String telefono, DistributorModelLocal distribuidor, SessionModelLocal sesion) {
        IdEmpleado = idEmpleado;
        Apellido = apellido;
        Direccion = direccion;
        Email = email;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
        Nombre = nombre;
        Rol = rol;
        Telefono = telefono;
        Distribuidor = distribuidor;
        Sesion = sesion;
    }

    public Integer getIdEmpleado() {
        return IdEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        IdEmpleado = idEmpleado;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getRol() {
        return Rol;
    }

    public void setRol(String rol) {
        Rol = rol;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public DistributorModelLocal getDistribuidor() {
        return Distribuidor;
    }

    public void setDistribuidor(DistributorModelLocal distribuidor) {
        Distribuidor = distribuidor;
    }

    public SessionModelLocal getSesion() {
        return Sesion;
    }

    public void setSesion(SessionModelLocal sesion) {
        Sesion = sesion;
    }
}
