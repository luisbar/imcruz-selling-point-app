package com.imcruzparts.pdv.data.param;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.param.model.ParamModelLocal;
import com.imcruzparts.pdv.data.sign.SignDatabaseRepository;

import io.realm.Realm;

public class ParamDatabaseRepository implements DatabaseRepository<String, Object> {

    private Realm mRealm;
    private String employeeLogged;
    private DatabaseRepository mSignDatabaseRepository;

    public ParamDatabaseRepository(String employeeLogged) {
        this.employeeLogged = employeeLogged;
        this.mSignDatabaseRepository = new SignDatabaseRepository();
    }

    @Override
    public void add(final String item) {
    }

    /**
     * Guarda los parametros en la base de datos local
     * @param jsonArray objtetos a guardar
     */
    @Override
    public void addMany(final String jsonArray) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
               realm.createOrUpdateAllFromJson(ParamModelLocal.class, jsonArray);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mSignDatabaseRepository.add(employeeLogged);
                mRealm.close();
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_P, R.string.txt_8));
                error.printStackTrace();
            }
        });
    }

    @Override
    public void update(String item) {

    }

    @Override
    public void remove(String specification) {

    }

    @Override
    public void queryOne(String specification) {

    }

    @Override
    public Object queryOne() {
        return null;
    }

    @Override
    public void queryMany(String specification) {

    }
}
