package com.imcruzparts.pdv.data.sign;

import com.google.gson.Gson;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.param.ParamApiRepository;
import com.imcruzparts.pdv.data.sign.specification.SignIn;
import com.imcruzparts.pdv.data.sign.specification.util.ParamForSignIn;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignApiRepository implements ApiRepository<SignApiRepository.Params, Integer, List<Integer>> {

    private final String TAG = SignApiRepository.class.toString();

    @Override
    public void patch(Params params) {
        // TODO: nada
    }

    @Override
    public void getOne(Integer integer) {
        // TODO: nada
    }

    @Override
    public void getMany() {
        // TODO: nada
    }

    /**
     * Metodo que inicia sesión
     * @param params objeto que contiene el username y la clave
     */
    @Override
    public void post(Params params) {
        SignIn signIn = ServiceGenerator.createService(SignIn.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/");
        Call call = signIn.post(new ParamForSignIn(params.username, params.password));

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() == 200) {
                    Gson gson = new Gson();
                    String item = gson.toJson(response.body());

                    ApiRepository mParamApiRepository = new ParamApiRepository(item);
                    mParamApiRepository.getMany();

                } else
                    EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_P, R.string.txt_5));
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_P, R.string.txt_8));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void deleteOne(Integer integer) {
        // TODO: nada
    }

    public static final class Params {

        private final String username;
        private final String password;

        public Params(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public static Params forPost(String username, String password) {
            return new Params(username, password);
        }
    }
}