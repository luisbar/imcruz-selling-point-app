package com.imcruzparts.pdv.data.activity;

import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.data.activity.specification.GetActivitiesAboutPendingRoute;
import com.imcruzparts.pdv.data.activity.specification.GetActivitiesFromParam;
import com.imcruzparts.pdv.data.activity.specification.GetSellerRoute;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.data.param.model.ParamModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.data.sellerroute.RouteDatabaseRepository;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class ActivityDatabaseRepository implements DatabaseRepository<ActivityDatabaseRepository.Params, Object> {

    private Realm mRealm;

    @Override
    public void add(Params item) {

    }

    /**
     * Crea las atividades para cada pdv de la ruta seleccionada
     * @param idSellerRoute identificador de ruta seleccionada
     */
    @Override
    public void addMany(final String idSellerRoute) {
        mRealm = Realm.getDefaultInstance();

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                final SellerRouteModelLocal sellerRouteModelLocal = (SellerRouteModelLocal)
                        new GetSellerRoute(realm, Integer.valueOf(idSellerRoute))
                        .toRealmResult().get(0);

                RealmResults<ParamModelLocal> results =
                        new GetActivitiesFromParam(realm).toRealmResult();

                for (PdvModelLocal pdv : sellerRouteModelLocal.getPDVs()) {
                    for (ParamModelLocal paramModelLocal : results) {

                        ActivityModelLocal activityModelLocal;
                        int id = realm.where(ActivityModelLocal.class).max("IdActividad") != null
                                ? realm.where(ActivityModelLocal.class).max("IdActividad").intValue() + 1
                                : 1;

                        activityModelLocal = realm.createObject(ActivityModelLocal.class, id);

                        activityModelLocal.setRutaVendedor(sellerRouteModelLocal);
                        activityModelLocal.setRuta(sellerRouteModelLocal.getRutaVenta());
                        activityModelLocal.setVendedor(sellerRouteModelLocal.getVendedor());
                        activityModelLocal.setPdv(pdv);
                        activityModelLocal.setValor(paramModelLocal.getValor());
                        activityModelLocal.setDescripcion(paramModelLocal.getDescripcion());
                        activityModelLocal.setVisible(paramModelLocal.getVisible());
                        activityModelLocal.setEstado("PENDIENTE");
                        activityModelLocal.setFechaAlta(new Date().toString());
                        activityModelLocal.setPedidos(new RealmList<RequestModelLocal>());
                    }
                }
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mRealm.close();
                new RouteDatabaseRepository("INICIADA").update(idSellerRoute);
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                EventBusMask.post(new Action(ActionType.ACTIVITIES_NOT_CREATED_P));
                mRealm.close();
                error.printStackTrace();
            }
        });
    }

    /**
     * Metodo que actualiza el estado de una actividad y agrega observación a una actividad
     * @param item campo a actualizar
     */
    @Override
    public void update(final Params item) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                ActivityModelLocal activity = realm.where(ActivityModelLocal.class)
                        .equalTo("IdActividad", item.idActivity)
                        .findFirst();

                String date = (new Date().toString());

                switch (item.fieldToUpade) {

                    case "state":
                        activity.setEstado(item.value);
                        activity.setFecha(date);
                        break;

                    case "note":
                        activity.setObservaciones(item.value);
                        break;
                    // Para que no dispare evento con EventBus
                    case "stateNotVisibleActivity":
                        activity.setEstado(item.value);
                        activity.setFecha(date);
                        break;
                }
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mRealm.close();
                switch (item.fieldToUpade) {

                    case "state":
                        EventBusMask.post(new Action(ActionType.ACTIVITY_FINALIZED_P));
                        break;

                    case "note":
                        EventBusMask.post(new Action(ActionType.NOTE_ADDED_P));
                        break;
                }
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                switch (item.fieldToUpade) {

                    case "state":
                        EventBusMask.post(new Action(ActionType.ACTIVITY_NOT_FINALIZED_P));
                        break;

                    case "note":
                        EventBusMask.post(new Action(ActionType.NOTE_NOT_ADDED_P));
                        break;
                }
                error.printStackTrace();
            }
        });
    }

    @Override
    public void remove(String specification) {

    }

    @Override
    public void queryOne(String specification) {

    }

    @Override
    public Object queryOne() {
        return null;
    }

    /**
     * Obtiene las actividades de la ruta especificada
     */
    @Override
    public void queryMany(String specification) {
        mRealm = Realm.getDefaultInstance();

        RealmResults<ActivityModelLocal> results = new GetActivitiesAboutPendingRoute(mRealm).toRealmResult();

        EventBusMask.post(new Action(ActionType.ACTIVITIES_CREATED_P, results.subList(0, results.size())));
        // Filtro las actividades con valor VISITAR
        results = mRealm.where(ActivityModelLocal.class).createQueryFromResult(results)
                    .equalTo("Valor", "VISITAR")
                    .equalTo("Estado", "PENDIENTE")
                    .findAll();

        EventBusMask.post(new Action(ActionType.GET_PENDING_VISITS, mRealm.copyFromRealm(results)));

        mRealm.close();
    }

    public static class Params {

        private int idActivity;
        private String fieldToUpade;
        private String value;

        public Params(int idActivity, String fieldToUpade, String value) {
            this.idActivity = idActivity;
            this.fieldToUpade = fieldToUpade;
            this.value = value;
        }

        public static  Params paramsForUpdate(int idActivity, String fieldToUpade, String value) {
            return new Params(idActivity, fieldToUpade, value);
        }
    }
}
