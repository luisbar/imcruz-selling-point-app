package com.imcruzparts.pdv.data.product;

import com.google.gson.Gson;
import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.product.specification.GetProductsFromCloud;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductApiRepository implements ApiRepository {

    private final String TAG = ProductApiRepository.class.toString();
    private final int OK = 200;
    private final int BAD_REQUEST = 400;
    private final int SESSION_EXPIRED = 401;

    @Override
    public void patch(Object o) {

    }

    @Override
    public void getOne(Object o) {

    }

    /**
     * Obtiene todos los productos desde la nube, luego invoca a otra clase
     * para guardarlo en la base de datos local
     */
    @Override
    public void getMany() {
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        GetProductsFromCloud productsFromCloud = ServiceGenerator.createService(GetProductsFromCloud.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);

        Call call = productsFromCloud.get();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        ProductDatabaseRepository productDatabaseRepository;
                        productDatabaseRepository = new ProductDatabaseRepository();
                        Gson gson = new Gson();
                        String products = gson.toJson(response.body());

                        productDatabaseRepository.addMany(products);
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.PRODUCTS_DOWNLOADED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.PRODUCTS_DOWNLOADED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.PRODUCTS_DOWNLOADED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void post(Object o) {

    }

    @Override
    public void deleteOne(Object o) {

    }
}
