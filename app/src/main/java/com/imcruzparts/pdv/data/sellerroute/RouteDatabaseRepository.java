package com.imcruzparts.pdv.data.sellerroute;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.activity.ActivityDatabaseRepository;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.data.request.RequestDatabaseRepository;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;
import com.imcruzparts.pdv.data.sellerroute.specification.GetActivitiesAboutPendingRoute;
import com.imcruzparts.pdv.data.sellerroute.specification.GetInitializedRoutes;
import com.imcruzparts.pdv.data.sellerroute.specification.GetPendingRoutes;

import io.realm.Realm;
import io.realm.RealmResults;

public class RouteDatabaseRepository implements DatabaseRepository<String, Object> {

    private Realm mRealm;
    private String finalize;

    public RouteDatabaseRepository(String finalize) {
        this.finalize = finalize;
    }

    public RouteDatabaseRepository() {
    }

    @Override
    public void add(String item) {

    }

    /**
     * Guarda las rutas traidas desde la nube en la base de datos local
     * @param jsonArray objtetos a guardar
     */
    @Override
    public void addMany(final String jsonArray) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.createOrUpdateAllFromJson(SellerRouteModelLocal.class, jsonArray);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                queryMany(null);
                mRealm.close();
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                EventBusMask.post(new Action(ActionType.PENDING_ROUTES_NOT_OBTAINED_P, R.string.txt_69));
                error.printStackTrace();
            }
        });
    }

    @Override
    public void update(final String idSellerRoute) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(SellerRouteModelLocal.class)
                        .equalTo("IdRutaVendedor", Integer.valueOf(idSellerRoute))
                        .findFirst()
                        .setEstado(finalize);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mRealm.close();
                if (finalize.equals("FINALIZADA"))
                    EventBusMask.post(new Action(ActionType.SELLER_ROUTE_FINALIZE_P, false));
                else if (finalize.equals("INICIADA"))
                    new ActivityDatabaseRepository().queryMany(null);
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                mRealm.close();
                if (finalize.equals("FINALIZADA"))
                    EventBusMask.post(new Action(ActionType.SELLER_ROUTE_FINALIZE_P, true));
                error.printStackTrace();
            }
        });
    }

    @Override
    public void remove(String specification) {

    }

    @Override
    public void queryOne(String specification) {

    }

    @Override
    public Object queryOne() {
        return null;
    }

    /**
     * Obtiene la ruta iniciada, si es que hubiese, caso contrario obtiene las pendientes
     * ordenadas por fechaProgramación
     * @param specification nombre de la clase que tiene la especificacion o criterio
     *                      de la consulta a realizar
     */
    @Override
    public void queryMany(String specification) {
        mRealm = Realm.getDefaultInstance();

        RealmResults<SellerRouteModelLocal> routes = new GetInitializedRoutes(mRealm).toRealmResult();

        if (!routes.isEmpty()) {
            new RequestDatabaseRepository().queryMany(null);
            RealmResults<ActivityModelLocal> activities =
                    new GetActivitiesAboutPendingRoute(mRealm).toRealmResult();
            EventBusMask.post(new Action(ActionType.INITIALIZED_ROUTES_OBTAINED_P, activities.subList(0, activities.size())));
        } else {
            routes = new GetPendingRoutes(mRealm).toRealmResult();;
            EventBusMask.post(new Action(ActionType.PENDING_ROUTES_OBTAINED_P, routes.subList(0, routes.size())));
        }

        mRealm.close();
    }
}
