package com.imcruzparts.pdv.data.route;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.route.specification.AddUnassignedRoute;
import com.imcruzparts.pdv.data.route.specification.GetRoutesFromCloud;
import com.imcruzparts.pdv.data.route.specification.GetSellingPointRoutesAssignedFromCloud;
import com.imcruzparts.pdv.data.route.specification.GetSellingPointRoutesUnassignedFromCloud;
import com.imcruzparts.pdv.data.route.specification.RemoveAssignedRoute;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteApiRepository implements ApiRepository {

    private final int OK = 200;
    private final int BAD_REQUEST = 400;
    private final int SESSION_EXPIRED = 401;

    @Override
    public void patch(Object o) {

    }

    /**
     * Obtiene las rutasPdvs asignadas a una rutaVenta
     * @param o id ruta venta
     */
    @Override
    public void getOne(final Object o) {
        // Obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        final String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Hace la petición
        GetSellingPointRoutesAssignedFromCloud sellingPointRoutesFromCloud;
        sellingPointRoutesFromCloud = ServiceGenerator.createService(GetSellingPointRoutesAssignedFromCloud.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = sellingPointRoutesFromCloud.get((int) o);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        getUnassigned(Integer.valueOf(o.toString()), token, response.body());
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    /**
     * Obtiene las rutasPdv que no estan asignadas a una rutaVenta
     * pero que estan en el perimetro correspondiente a esa
     */
    private void getUnassigned(int idRoute, String token, final Object assignedRoutes) {

        GetSellingPointRoutesUnassignedFromCloud getSellingPointRoutesUnassignedFromCloud;
        getSellingPointRoutesUnassignedFromCloud = ServiceGenerator.createService(GetSellingPointRoutesUnassignedFromCloud.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = getSellingPointRoutesUnassignedFromCloud.get(idRoute);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P,
                                Arrays.asList(
                                        assignedRoutes,
                                        response.body()
                                ),
                                false));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.SELLING_POINT_ROUTES_GOTTEN_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    /**
     * Obtiene todas las rutasVenta
     */
    @Override
    public void getMany() {
        // Obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Hace la petición
        GetRoutesFromCloud getRoutesFromCloud = ServiceGenerator.createService(GetRoutesFromCloud.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = getRoutesFromCloud.get();

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(ActionType.ROUTES_GOTTEN_P, response.body(), false));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.ROUTES_GOTTEN_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.ROUTES_GOTTEN_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.ROUTES_GOTTEN_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    /**
     * Agrega una rutaPdv a una rutaVenta
     * @param o identificador rutaPdv
     */
    @Override
    public void post(Object o) {
        // Obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Hace la petición
        AddUnassignedRoute addUnassignedRoute = ServiceGenerator.createService(AddUnassignedRoute.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = addUnassignedRoute.post(o);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(ActionType.ROUTE_ADDED_P, response.body(), false));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.ROUTE_ADDED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.ROUTE_ADDED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.ROUTE_ADDED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    /**
     * Elimina una rutaPdv de una rutaVenta
     * @param o identificador rutaPdv
     */
    @Override
    public void deleteOne(Object o) {
        // Obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Hace la petición
        RemoveAssignedRoute removeAssignedRoute = ServiceGenerator.createService(RemoveAssignedRoute.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = removeAssignedRoute.delete(o);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                switch (response.code()) {

                    case OK:
                        EventBusMask.post(new Action(ActionType.ROUTE_REMOVED_P, null, false));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.ROUTE_REMOVED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.ROUTE_REMOVED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.ROUTE_REMOVED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }
}
