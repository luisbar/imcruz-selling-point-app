package com.imcruzparts.pdv.data;

import java.util.List;

import io.realm.RealmResults;

/**
 * Clase que pagina los datos de una lista, de acuerdo
 * a la variable sizeOfPagination
 */
public class Pagination {

    private int until;
    private int from;
    private int sizeOfPagination;

    public Pagination(int sizeOfPagination) {
        this.sizeOfPagination = sizeOfPagination;
        this.from = 0;
        this.until = sizeOfPagination;
    }

    protected List paginate(RealmResults results) {
        int size = results.size();
        List paginatedList = getPaginatedList(results);
        updateFromAndUntil(size);

        return paginatedList;
    }

    /**
     * Verifica que el valor de until sea menor o igual al tamaño
     * de la lista, por que si no es asi, el valor de until pasa al tamaño de la lista
     * o la lista esta vacia
     * @param results lista a paginar
     * @return resultado de la paginacion
     */
    private List getPaginatedList(RealmResults results) {

        if (until <= results.size())// Esta condición es por si la tabla no tiene datos, entonces en result.size = 0
            return results.subList(from, until);
        else
            return results.subList(from, results.size());
    }

    /**
     * Incrementa los valores de from y until
     * @param size tamaño de la lista a paginar
     */
    private void updateFromAndUntil(int size) {

        if (until != size && until != size + 1) {

            from = until;
            until = (until + sizeOfPagination) > size
                    ? size
                    : until + sizeOfPagination;
        } else if (until + 1 == size + 1){// Para que no continue enviando los ultimos datos de acuerdo al sizeOfPagination
            from = until;
            until++;
        }
    }
}
