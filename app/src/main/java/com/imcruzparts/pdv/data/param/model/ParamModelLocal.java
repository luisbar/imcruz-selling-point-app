package com.imcruzparts.pdv.data.param.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ParamModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdParametro;
    private String Codigo;
    private String Descripcion;
    private String Grupo;
    private String Valor;
    private Boolean Visible;

    public ParamModelLocal() {
    }

    public ParamModelLocal(Integer idParametro, String codigo, String descripcion, String grupo, String valor, Boolean visible) {
        IdParametro = idParametro;
        Codigo = codigo;
        Descripcion = descripcion;
        Grupo = grupo;
        Valor = valor;
        Visible = visible;
    }

    public Integer getIdParametro() {
        return IdParametro;
    }

    public void setIdParametro(Integer idParametro) {
        IdParametro = idParametro;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String grupo) {
        Grupo = grupo;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }

    public Boolean getVisible() {
        return Visible;
    }

    public void setVisible(Boolean visible) {
        Visible = visible;
    }
}
