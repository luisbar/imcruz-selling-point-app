package com.imcruzparts.pdv.data.home.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene todos los clientes
 */
public class GetClients implements RealmSpecification {

    private Realm mRealm;

    public GetClients(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(PdvModelLocal.class)
                .findAll()
                .sort("IdCliente.Nombre");
    }
}
