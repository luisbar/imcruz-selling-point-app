package com.imcruzparts.pdv.data.route.specification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RemoveAssignedRoute {

    @POST("RutaPDV/Remove")
    Call<Void> delete(@Body Object routeUnassigned);
}
