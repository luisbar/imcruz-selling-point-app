package com.imcruzparts.pdv.data.home.specification;

import com.imcruzparts.pdv.data.RealmSpecification;
import com.imcruzparts.pdv.data.home.model.PdvModelLocal;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Obtiene los pdvs con estado nuevo o actualizado
 */
public class GetNewAndUpdatedClients implements RealmSpecification {

    private Realm mRealm;

    public GetNewAndUpdatedClients(Realm mRealm) {
        this.mRealm = mRealm;
    }

    @Override
    public RealmResults toRealmResult() {
        return mRealm.where(PdvModelLocal.class)
                .equalTo("Estado", "NUEVO")
                .or()
                .equalTo("Estado", "MODIFICADO")
                .findAll();
    }
}
