package com.imcruzparts.pdv.data.activity;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.ApiRepository;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.ServiceGenerator;
import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;
import com.imcruzparts.pdv.data.activity.specification.SendActivities;
import com.imcruzparts.pdv.data.home.HomeDatabaseRepository;
import com.imcruzparts.pdv.data.sellerroute.RouteDatabaseRepository;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityApiRepository implements ApiRepository {

    private final int OK = 200;
    private final int BAD_REQUEST = 400;
    private final int SESSION_EXPIRED = 401;

    @Override
    public void patch(Object o) {

    }

    @Override
    public void getOne(Object o) {

    }

    @Override
    public void getMany() {

    }

    /**
     * Sube todas las actividades y pedidos a la nube
     * @param lstBase64Files
     */
    @Override
    public void post(Object lstBase64Files) {
        // Obtiene el token
        DatabaseRepository mHomeDatabaseRepository;
        mHomeDatabaseRepository = new HomeDatabaseRepository(false);
        String token = mHomeDatabaseRepository.queryOne() != null
                ? mHomeDatabaseRepository.queryOne().toString()
                : null;
        // Obtiene las ActivityModelLocals con SellerRouteModelLocal
        // con estado = FINALIZADA (esto no deberia estar aqui)
        Realm realm = Realm.getDefaultInstance();

        final RealmResults<ActivityModelLocal> results = realm.where(ActivityModelLocal.class)
                .equalTo("RutaVendedor.Estado", "FINALIZADA")
                .findAll();
        final List<ActivityModelLocal> lstActivitites = realm.copyFromRealm(results);

        // Crea el servicio
        SendActivities sendActivities = ServiceGenerator.createServiceWithDateFormatter(SendActivities.class, "http://amirozlav-001-site1.etempurl.com/ServicePDV.svc/", token);
        Call call = sendActivities.post(lstActivitites);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                 switch (response.code()) {

                    case OK:
                        for (ActivityModelLocal activity : lstActivitites)
                            new RouteDatabaseRepository("CERRADA").update(activity.getRutaVendedor().getIdRutaVendedor().toString());
                        EventBusMask.post(new Action(ActionType.ACTIVITIES_UPLOADED_P));
                        break;

                    case BAD_REQUEST:
                        EventBusMask.post(new Action(ActionType.ACTIVITIES_UPLOADED_P, R.string.txt_8, true));
                        break;

                    case SESSION_EXPIRED:
                        EventBusMask.post(new Action(ActionType.ACTIVITIES_UPLOADED_P, R.string.txt_73, true));
                        break;
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                EventBusMask.post(new Action(ActionType.ACTIVITIES_UPLOADED_P, R.string.txt_8, true));
                t.printStackTrace();
            }
        });
    }

    @Override
    public void deleteOne(Object o) {

    }
}
