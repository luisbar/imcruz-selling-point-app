package com.imcruzparts.pdv.data.sign;

import com.imcruzparts.pdv.R;
import com.imcruzparts.pdv.common.Action;
import com.imcruzparts.pdv.common.ActionType;
import com.imcruzparts.pdv.common.EventBusMask;
import com.imcruzparts.pdv.data.DatabaseRepository;
import com.imcruzparts.pdv.data.sign.model.EmployeeModelLocal;
import com.imcruzparts.pdv.data.sign.model.SessionModelLocal;
import com.imcruzparts.pdv.data.sign.specification.ThereIsSession;

import io.realm.Realm;
import io.realm.RealmResults;

public class SignDatabaseRepository implements DatabaseRepository<String, String> {

    private Realm mRealm;

    private final String TAG = SignDatabaseRepository.class.toString();

    /**
     * Guarda los datos del empleado que inicio sesión
     * @param item objeto a guardar
     */
    @Override
    public void add(final String item) {

        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.createOrUpdateObjectFromJson(EmployeeModelLocal.class, item);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBusMask.post(new Action(ActionType.SIGN_IN_SUCCESS_P));
                mRealm.close();
                queryOne(null);
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                EventBusMask.post(new Action(ActionType.SIGN_IN_ERROR_P, R.string.txt_69));
                mRealm.close();
                error.printStackTrace();
            }
        });

    }

    @Override
    public void addMany(String jsonArray) {
        // TODO: nada
    }

    /**
     * Metodo que cierra sesión(el cierre de sesión consiste en guardar FechaHoraFin),
     * la cual es devuelta por el servidor
     * @param item objeto a actualizar
     */
    @Override
    public void update(final String item) {
        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.createOrUpdateObjectFromJson(SessionModelLocal.class, item);
            }
        },
        new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBusMask.post(new Action(ActionType.SIGN_OUT_SUCCESS_P));
                mRealm.close();
            }
        },
        new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                EventBusMask.post(new Action(ActionType.SIGN_OUT_ERROR_P));
                mRealm.close();
                error.printStackTrace();
            }
        });
    }


    @Override
    public void remove(String specification) {
        // TODO: nada
    }

    /**
     * Metodo que verifica si hay una sesión activa
     * @param specification nombre de la clase que tiene la especificacion o criterio
     */
    @Override
    public void queryOne(String specification) {

        mRealm = Realm.getDefaultInstance();
        ThereIsSession thereIsSession = new ThereIsSession(mRealm);

        RealmResults<SessionModelLocal>  results = thereIsSession.toRealmResult();
        SessionModelLocal sessionModelLocal = !results.isEmpty() ? results.first() : null;

        EventBusMask.post(new Action(ActionType.CHECK_USER_LOGGED, sessionModelLocal));
        mRealm.close();
    }

    @Override
    public String queryOne() {
        return null;
    }

    @Override
    public void queryMany(String specification) {
        // TODO: nada
    }
}
