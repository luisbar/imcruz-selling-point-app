package com.imcruzparts.pdv.data.sign.specification.util;

public class ParamForSignIn {

    private String Username;
    private String Password;

    public ParamForSignIn(String username, String password) {
        Username = username;
        Password = password;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
