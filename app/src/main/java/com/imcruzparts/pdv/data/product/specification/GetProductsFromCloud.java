package com.imcruzparts.pdv.data.product.specification;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetProductsFromCloud  {

    @GET("Productos/Page/1")
    Call<Object> get();
}
