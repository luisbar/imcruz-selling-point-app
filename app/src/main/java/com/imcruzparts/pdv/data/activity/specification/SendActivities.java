package com.imcruzparts.pdv.data.activity.specification;


import com.imcruzparts.pdv.data.activity.model.ActivityModelLocal;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SendActivities {

    @POST("Actividades/UpList")
    Call<Void> post(@Body List<ActivityModelLocal> activities);
}
