package com.imcruzparts.pdv.data.activity.model;

import com.imcruzparts.pdv.data.home.model.PdvModelLocal;
import com.imcruzparts.pdv.data.request.model.RequestModelLocal;
import com.imcruzparts.pdv.data.sellerroute.model.RouteModelLocal;
import com.imcruzparts.pdv.data.sellerroute.model.SellerRouteModelLocal;
import com.imcruzparts.pdv.data.sign.model.EmployeeModelLocal;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ActivityModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdActividad;
    private SellerRouteModelLocal RutaVendedor;
    private RouteModelLocal Ruta;
    private EmployeeModelLocal Vendedor;
    private PdvModelLocal Pdv;
    private String Valor;
    private String Descripcion;
    private Boolean Visible;
    private String Estado;
    private String Fecha;
    private String Observaciones;
    private String FechaAlta;
    private String FechaBaja;
    private RealmList<RequestModelLocal> Pedidos;

    public ActivityModelLocal() {
    }

    public ActivityModelLocal(Integer idActividad, SellerRouteModelLocal rutaVendedor, RouteModelLocal ruta, EmployeeModelLocal vendedor, PdvModelLocal pdv, String valor, String descripcion, Boolean visible, String estado, String fecha, String observaciones, String fechaAlta, String fechaBaja, RealmList<RequestModelLocal> pedidos) {
        IdActividad = idActividad;
        RutaVendedor = rutaVendedor;
        Ruta = ruta;
        Vendedor = vendedor;
        Pdv = pdv;
        Valor = valor;
        Descripcion = descripcion;
        Visible = visible;
        Estado = estado;
        Fecha = fecha;
        Observaciones = observaciones;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
        Pedidos = pedidos;
    }

    public Integer getIdActividad() {
        return IdActividad;
    }

    public void setIdActividad(Integer idActividad) {
        IdActividad = idActividad;
    }

    public SellerRouteModelLocal getRutaVendedor() {
        return RutaVendedor;
    }

    public void setRutaVendedor(SellerRouteModelLocal rutaVendedor) {
        RutaVendedor = rutaVendedor;
    }

    public RouteModelLocal getRuta() {
        return Ruta;
    }

    public void setRuta(RouteModelLocal ruta) {
        Ruta = ruta;
    }

    public EmployeeModelLocal getVendedor() {
        return Vendedor;
    }

    public void setVendedor(EmployeeModelLocal vendedor) {
        Vendedor = vendedor;
    }

    public PdvModelLocal getPdv() {
        return Pdv;
    }

    public void setPdv(PdvModelLocal pdv) {
        Pdv = pdv;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Boolean getVisible() {
        return Visible;
    }

    public void setVisible(Boolean visible) {
        Visible = visible;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public RealmList<RequestModelLocal> getPedidos() {
        return Pedidos;
    }

    public void setPedidos(RealmList<RequestModelLocal> pedidos) {
        Pedidos = pedidos;
    }
}
