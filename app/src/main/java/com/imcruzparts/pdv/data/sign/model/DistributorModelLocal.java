package com.imcruzparts.pdv.data.sign.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DistributorModelLocal extends RealmObject {

    @PrimaryKey
    private Integer IdDistribuidor;
    private String Direccion;
    private String Estado;
    private String FechaAlta;
    private String FechaBaja;
    private String Nit;
    private String RazonSocial;
    private String Responsable;

    public DistributorModelLocal() {
    }

    public DistributorModelLocal(Integer idDistribuidor, String direccion, String estado, String fechaAlta, String fechaBaja, String nit, String razonSocial, String responsable) {
        IdDistribuidor = idDistribuidor;
        Direccion = direccion;
        Estado = estado;
        FechaAlta = fechaAlta;
        FechaBaja = fechaBaja;
        Nit = nit;
        RazonSocial = razonSocial;
        Responsable = responsable;
    }

    public Integer getIdDistribuidor() {
        return IdDistribuidor;
    }

    public void setIdDistribuidor(Integer idDistribuidor) {
        IdDistribuidor = idDistribuidor;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        FechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        FechaBaja = fechaBaja;
    }

    public String getNit() {
        return Nit;
    }

    public void setNit(String nit) {
        Nit = nit;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        RazonSocial = razonSocial;
    }

    public String getResponsable() {
        return Responsable;
    }

    public void setResponsable(String responsable) {
        Responsable = responsable;
    }
}