### Para ver la base de datos en chrome

* escribir chrome://inspect
* dar click en inspect debajo del nombre de la app imcruzsellingpoint
* click en websql y luego en default.realm, ahi estan todas las tablas

### Por hacer

- [x] Mejorar el codigo de ProductView, mas que todo el metodo updateRecyclerAndAutocomplete
- [ ] Eliminar el codigo de inserción a la base de datos en ProductDatabaseRepository
- [ ] Da un error en la linea 298 de mainActivity: navigation.getMenu().findItem(mNavItemId).setChecked(true);
- [ ] Eliminar los clientes con Estado NUEVO despues de subirlos a la nube
- [ ] Que solo obtenga una vez las rutas vendedor
- [ ] Subir las fotos de las actividades
